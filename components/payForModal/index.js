/**
 * @author GuoNanLin
 * @date 2020-11-06
 */

import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../config';
import {
	API as $API
} from '../../utils/request';

Component({
	options: {
		pureDataPattern: /^_/ // 指定所有 _ 开头的数据字段为纯数据字段
	},
	/**
	 * 组件的属性列表
	 */
	properties: {
		'modal_name': {
			type: String,
			observer: function (newVal, oldVal) {
				if (!newVal) {
					return;
				}
				console.log()
				this.setData({ modalName: newVal });
			}
		}
	},
	/**
	 * 组件的初始数据
	 */
	data: {
		modalName: '',
		payAble: 0,
		balanceFee: 0,
		closeSrc: imgHosts + 'modal_close.png',
		visible: false,
		Length: 6,
		Value: '',
		isFocus: true,
	},
	lifetimes: {
		attached: function () {
			// 在组件实例进入页面节点树时执行
		},
		detached: function () {
			// 在组件实例被从页面节点树移除时执行
		},
	},
	pageLifetimes: {
		show: function () { },
		hide: function () { },
		resize: function () { },
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		// 判断是否设置支付密码
		checkAccount(callback) {
			const token = wx.getStorageSync('sysToken');
			const params = {
				sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
				token: token,
				...publicParams
			};
			$API.user.getAccountDetail(params).then(res => {
				if (res.data.is_set_pay_password) {
					// 创建余额转账订单
					callback();
				} else {
					// 未设置支付密码
					wx.showToast({
						title: '未设置支付密码',
						success() {
							wx.navigateTo({
								url: '/pages/myWallet/payPassword/index',
							});
						}
					});
				}
			});
		},
		showModal(payAble, balanceFee) {
			this.setData({ visible: true, payAble, balanceFee });
		},
		// 关闭打赏弹窗
		_closeModal() {
			this.setData({
				visible: false,
				Value: ''
			});
		},
		// 跳转充值页面
		_jumpToRecharge() {
			wx.navigateTo({
				url: '/pages/myWallet/recharge/index',
			});
		},
		_Tap() {
			this.setData({
				isFocus: true
			});
		},
		// 输入支付密码
		_Focus(e) {
			const inputValue = e.detail.value,
				ilen = inputValue.length,
				token = wx.getStorageSync('sysToken');
			this.setData({
				Value: inputValue
			});
			if (ilen == 6) {
				const params = {
					sign: md5(`dbkey|${publicParams.dbkey}|pay_password|${inputValue}|privateKey|${publicParams.privateKey}|token|${token}`),
					token: token,
					pay_password: inputValue,
					...publicParams
				};
				$API.pay.payPasswordVerify(params).then(res => {
					if (res.data.isok == 1) {
						// 支付密码验证通过
						this.triggerEvent('paySuccess', {});
					}
				}).catch(err => {
					// 密码错误清空密码
					this.setData({
						Value: ''
					});
				});
			}
		},
	}
})
