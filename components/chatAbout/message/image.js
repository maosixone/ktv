// pages/conversation/components/message/image.js
let { screenWidth } = wx.getSystemInfoSync();
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		message: {
			type: Object,
			observer: function (newVal, oldVal) {
				if (!newVal) {
					return
				}
				let image = '';
				if (newVal.messageType == 'RC:ReferenceMsg') {
					image = newVal.content.objName == "RC:ImgMsg" ? newVal.content.referMsg.imageUri : newVal.content.objName == "RC:GIFMsg" ? newVal.content.referMsg.remoteUrl : '';
				}
				else {
					image = newVal.messageType == 'RC:ImgMsg' ? newVal.content.imageUri : newVal.messageType == 'RC:GIFMsg' ? newVal.content.remoteUrl : '';
				}
				this.setData({
					imgSrc: image,
				});
			}
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		width: screenWidth / 2,
		imgSrc: '',
	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		onPreviewImage: function () {
			let { imgSrc } = this.data;
			this.triggerEvent('onpreviewimage', imgSrc);
		}
	}
})
