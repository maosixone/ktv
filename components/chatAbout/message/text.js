// pages/conversation/components/message/text.js
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		message: {
			type: Object,
			observer: function (newVal, oldVal) {
				if (!newVal) {
					return
				}
				let text = '';
				if (newVal.messageType == 'RC:ReferenceMsg') {
					text = newVal.content.objName == "RC:TxtMsg" ? newVal.content.referMsg.content : '';
				}
				else {
					text = newVal.content.content;
				}
				this.setData({
					text,
				});
			},
		},
		reference: Boolean
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		text: '',
	},

	methods: {
		bindlongpress: () => {
			this.triggerEvent('onlongpress');
		}
	}
})
