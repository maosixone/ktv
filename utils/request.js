import {
    hosts,
    publicParams
} from '../config';

const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const FORM = 'FORM';
const DELETE = 'DELETE';

const baseURL = hosts;

function request(method, url, data, remindTip) {
    return new Promise(function (resolve, reject) {
        let header = {
            'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
        };
        wx.request({
            url: baseURL + url,
            method: method,
            data: data,
            header: header,
            success(res) {
                //请求成功
                //判断状态码---errCode状态根据后端定义来判断

                const cookies = res.cookies;
                if (cookies.length > 0) {
                    const cookie = cookies.find(item => item.indexOf('sysToken') > -1);
                    if (cookie) {
                        const sysTokenbZQIE4m9 = cookie.split(';')[0].split('=')[1];
                        wx.setStorageSync("sysToken", sysTokenbZQIE4m9);
                    }
                }
                if (res.data.code == 10000) {
                    resolve(res.data);
                } else if (res.data.code == 10004 || res.data.code == 10005) {
                    wx.clearStorageSync();
                    wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        success() {
                            wx.reLaunch({
                                url: '/pages/login/index',
                            });
                        }
                    });
                } else {
                    //其他异常
                    let msg = res.data.msg
                    if (remindTip) {
                        msg = remindTip
                    }
                    if (url != '/api/room/bindingRoom' || url != '/api/booth/bindingBooth') {
                        wx.showToast({
                            title: msg,
                            icon: 'none',
                            duration: 2000
                        });
                    }
                    reject(res.data.msg);
                }
            },
            fail(err) {
                //请求失败
                reject(err)
            }
        })
    })
}

const API = {
    user: {
        // 获取微信支付所需的openId
        getOpenId: (data) => request(POST, '/api/pay/code2Session', data,),
        // 微信登陆
        weChatLogin: (data) => request(POST, '/api/user/weChatLogin', data),
        // 微信绑定手机号
        bindingWeChat: (data) => request(POST, '/api/user/bindingWeChat', data),
        login: (data) => request(POST, '/api/user/login', data, '手机号或密码错误'),
        logout: (data) => request(GET, '/api/user/logout', data),
        // 更新用户签名
        updPersonalSignature: (data) => request(POST, '/api/user/updPersonalSignature', data),
        getVerifyCode: (data) => request(GET, '/api/user/getSmsVerify', data),
        // 验证码登录
        codeLogin: (data) => request(POST, '/api/user/smsLogin', data),
        // 修改用户资料
        updateUserInfo: (data) => request(POST, '/api/user/updUser', data),
        // 获取用户融云信息
        getRongInfo: (data) => request(GET, '/api/user/getRongInfo', data),
        // 注册接口
        register: (data) => request(POST, '/api/user/register', data),
        // 注册、登录时设置密码
        setPassWord: (data) => request(POST, '/api/user/setPassWord', data),
        // 忘记密码
        forgetPassWord: (data) => request(POST, '/api/user/forget', data),
        // 修改用户密码+验证码版本
        setPassWordAndVerify: (data) => request(POST, '/api/user/setPassWordAndVerify', data),
        // 获取用户个人信息
        getUser: (data) => request(GET, '/api/user/getUser', data),
        // 获取粉丝、点赞、关注
        getFollowInfo: (data) => request(GET, '/api/follow/getFollowInfo', data),
        // 获取用户个人账户设置详情
        getAccountDetail: (data) => request(GET, '/api/personal/getAccountDetail', data),
        // 获取钱包信息
        getWalletInfo: (data) => request(GET, '/api/wallet/getWalletInfo', data),
        // 获取收入详情
        getIncomeDetail: (data) => request(GET, '/api/wallet/getIncomeDetail', data),
        // 获取红包池总明细
        getPacketPoolDetail: (data) => request(GET, '/api/packet/getPacketPoolDetail', data),
        // 获取省份列表
        getProvinceList: (data) => request(GET, '/api/personal/getProvinceList', data),
        // 获取城市列表
        getCityList: (data) => request(GET, '/api/personal/getCityList', data),
        // 扫二维码加好友（关注）
        queryQRCode: (data) => request(POST, '/api/follow/queryQRCode', data),
        // 获取钱包账单总明细
        getWalletDetail: (data) => request(GET, '/api/wallet/getWalletDetail', data),
        // 获取钱包账单明细列表
        getWalletDetailList: (data) => request(GET, '/api/wallet/getWalletDetailList', data),
        // 根据手机号，用户ID，获取用户信息
        getUserInfoByPhone: (data) => request(GET, '/api/user/getUserInfoByPhone', data),
        // 根据手机号获取用户信息(不需要登录)
        getUserIsExist: (data) => request(POST, '/api/user/getUserIsExist', data),
        // 获取关注列表
        getFollowList: (data) => request(GET, '/api/follow/getFollowList', data),
        // 获取粉丝列表
        getFansList: (data) => request(GET, '/api/follow/getFansList', data),
        // 获取聊天人信息
        getChatInfoById: (data) => request(GET, '/api/follow/getChatInfoById', data),
    },
    works: {
        getWorksListPage: (data) => request(GET, '/api/works/getWorksListPage', data),
        getPersonWorksList: (data) => request(GET, '/api/personal/getPersonWorksList', data),
        getLiveListPage: (data) => request(GET, '/api/live/getLiveListPage', data),
        addFollow: (data) => request(POST, '/api/follow/addFollow', data),
        cancelFollow: (data) => request(POST, '/api/follow/cancelFollow', data),
        updateStar: (data) => request(POST, '/api/works/star', data),
        // 获取收藏列表
        getWorksStarList: (data) => request(GET, '/api/personal/getWorksStarList', data),
        // 通过work_id获取作品信息
        getWorksInfoById: (data) => request(GET, '/api/works/getWorksInfoById', data),
        // 通过live_broadcast_id获取作品信息
        getLiveInfoById: (data) => request(GET, '/api/live/getLiveInfoById', data),
        // 通过ktv_user_id是否关注该用户
        isFollow: (data) => request(GET, '/api/follow/isFollow', data),
        // 作品打赏回调
        worksReward: (data) => request(POST, '/api/works/worksReward', data),
        // 作品收费规则
        getWorksRule: (data) => request(GET, '/api/works/getWorksRule', data),
        // 修改作品下载状态
        downLoadWorks: (data) => request(POST, '/api/works/downLoadWorks', data),
        // 删除作品
        deleteWorks: (data) => request(POST, '/api/works/deleteWorks', data),
    },
    pay: {
        // 验证支付密码
        payPasswordVerify: (data) => request(POST, '/api/pay/payPasswordVerify', data),
        // 余额转账
        balanceTransfer: (data) => request(POST, '/api/pay/balanceTransfer', data),
        // 余额转账
        balancePayment: (data) => request(POST, '/api/pay/balancePayment', data),
        // 充值第三方支付
        gotoPay: (data) => request(POST, '/api/pay/gotoPay', data),
        // 充值
        recharge: (data) => request(POST, '/api/wallet/recharge', data),
        // 设置支付密码
        setPayPassWord: (data) => request(POST, '/api/user/setPayPassWord', data),
    },
    order: {
        // 创建余额转账订单
        transferOrder: (data) => request(POST, '/api/order/transferOrder', data),
        // 创建作品下载订单
        worksDownloadOrder: (data) => request(POST, '/api/order/worksDownloadOrder', data),
        // 创建视频打赏订单
        worksRewardOrder: (data) => request(POST, '/api/order/worksRewardOrder', data),
        // 创建点歌订单
        pickSongOrder: (data) => request(POST, '/api/order/pickSongOrder', data),
        // 创建插歌订单
        insertSongOrder: (data) => request(POST, '/api/order/insertSongOrder', data),
        // 创建大厅打赏订单
        rewardSongOrder: (data) => request(POST, '/api/order/rewardSongOrder', data),
        // 创建包厢打赏订单
        roomRewardOrder: (data) => request(POST, '/api/order/roomRewardOrder', data),
        // 包厢送祝福
        roomWishOrder: (data) => request(POST, '/api/order/roomWishOrder', data),
    },
    lobby: {
        // 绑定大厅卡座
        bindingBooth: (data) => request(POST, '/api/booth/bindingBooth', data),
        // 获取大厅推荐
        recommend: (data) => request(GET, '/api/program/recommend', data),
        // 获取大厅节目单列表
        getHallProgramList: (data) => request(GET, '/api/program/getHallProgramList', data),
        // 获取大厅节目单列表
        getHallSongClassifyList: (data) => request(GET, '/api/songClassify/getHallSongClassifyList', data),
        // 根据大厅分类获取歌曲列表
        getSongListByClassifyId: (data) => request(GET, '/api/songClassify/getSongListByClassifyId', data),
        // 根据大厅分类获取歌曲列表
        addHallProgram: (data) => request(POST, '/api/program/addHallProgram', data),
        // 大厅插歌
        insertHallProgram: (data) => request(POST, '/api/program/insertHallProgram', data),
        // 大厅退歌
        quitHallProgram: (data) => request(POST, '/api/program/quitHallProgram', data),
        // 大厅换歌
        changeHallProgram: (data) => request(POST, '/api/program/changeHallProgram', data),
        // 获取当前播放节目单
        nowHallProgramInfo: (data) => request(GET, '/api/program/nowHallProgramInfo', data),
        // 打赏当前播放节目
        rewardHallProgram: (data) => request(POST, '/api/program/rewardHallProgram', data),
        // 获取红包列表
        getPacketList: (data) => request(GET, '/api/program/getPacketList', data),
        // 获取大厅搜索歌曲
        getSongSearchPage: (data) => request(GET, '/api/song/getSongSearchPage', data),
        // 获取大厅搜索歌曲
        grabPacket: (data) => request(POST, '/api/packet/grabPacket', data),
    },
    room: {
        // 绑定包厢
        bindingRoom: (data) => request(POST, '/api/room/bindingRoom', data),
        // 获取包厢推荐
        recommend: (data) => request(GET, '/api/room/recommend', data),
        // 获取灯控类型
        getLightCtrlType: (data) => request(GET, '/api/room/getLightCtrlType', data),
        // 灯光控制
        lightCtrl: (data) => request(POST, '/api/room/lightCtrl', data),
        // 播控
        playCtrl: (data) => request(POST, '/api/room/playCtrl', data),
        // 获取包厢热门歌曲
        getPopularSongList: (data) => request(GET, '/api/room/getPopularSongList', data),
        // 获取包厢点歌最多
        getMostSongList: (data) => request(GET, '/api/room/getMostSongList', data),
        // 获取包厢打赏最多
        getRewardSongList: (data) => request(GET, '/api/room/getRewardSongList', data),
        // 获取包厢全部歌曲
        getRoomSongList: (data) => request(GET, '/api/room/getRoomSongList', data),
        // 获取歌曲曲风
        getStyleList: (data) => request(GET, '/api/song/getStyleList', data),
        // 获取歌曲语言
        getLanguageList: (data) => request(GET, '/api/song/getLanguageList', data),
        // 包厢点歌
        pickSong: (data) => request(POST, '/api/room/pickSong', data),
        // 打赏、判断当前是否可打赏
        nowRoomSingInfo: (data) => request(GET, '/api/room/nowRoomSingInfo', data),
        // 获取包厢当前直播用户信息
        getRoomLiveUserInfo: (data) => request(GET, '/api/live/getRoomLiveUserInfo', data),
        // 包厢打赏
        roomReward: (data) => request(POST, '/api/room/roomReward', data),
        // 包厢删歌
        deleteSong: (data) => request(POST, '/api/room/deleteSong', data),
        // 包厢顶歌
        topSong: (data) => request(POST, '/api/room/topSong', data),
        // 更改包厢直播类型
        updSingingStyle: (data) => request(POST, '/api/room/updSingingStyle', data),
        // 获取已点列表
        getSelectedList: (data) => request(GET, '/api/room/getSelectedList', data),
        // 获取包厢已唱歌曲
        getSungList: (data) => request(GET, '/api/room/getSungList', data),
        // 包厢歌曲搜索
        getSearchSongList: (data) => request(GET, '/api/room/getSearchSongList', data),
        // 送祝福
        roomWish: (data) => request(POST, '/api/room/roomWish', data),
    }
};

module.exports = {
    API: API
};