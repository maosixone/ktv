const upng = require('../static/UPNG');

const formatTime = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()

	return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const updUserInfo = (data) => {
	let userInfo = wx.getStorageSync('user_info');
	userInfo = {
		...userInfo,
		...data
	};
	wx.setStorageSync('user_info', userInfo);
}

const formatNumber = n => {
	n = n.toString()
	return n[1] ? n : `0${n}`
}

const formatSeconds = (value) => {
	let theTime = parseInt(value), theTime1 = 0, theTime2 = 0, theTime3 = 0;// 天
	if (theTime > 60) {
		theTime1 = parseInt(theTime / 60);
		theTime = parseInt(theTime % 60);
		if (theTime1 > 60) {
			theTime2 = parseInt(theTime1 / 60);
			theTime1 = parseInt(theTime1 % 60);
			if (theTime2 > 24) {
				//大于24小时
				theTime3 = parseInt(theTime2 / 24);
				theTime2 = parseInt(theTime2 % 24);
			}
		}
	}
	let result = '';
	// if (theTime > 0) {
	//   result = "" + parseInt(theTime) + "秒";
	// }
	// if (theTime1 > 0) {
	//   result = "" + parseInt(theTime1) + "分" + result;
	// }
	if (theTime2 > 0) {
		result = "" + parseInt(theTime2) + "小时" + result;
	}
	if (theTime3 > 0) {
		result = "" + parseInt(theTime3) + "天" + result;
	}
	return result;
}

const Trim = (str) => {
	return str = str.replace(/\s+/g, '');
}

const formatDate = (now, chat = false) => {
	const year = now.getFullYear();
	const month = now.getMonth() + 1;
	const day = now.getDate();
	const hour = now.getHours() == 0 ? '00' : now.getHours() < 10 ? `0${now.getHours()}` : now.getHours();
	const minute = now.getMinutes() == 0 ? '00' : now.getMinutes() < 10 ? `0${now.getMinutes()}` : now.getMinutes();
	const second = now.getSeconds() == 0 ? '00' : now.getSeconds() < 10 ? `0${now.getSeconds()}` : now.getSeconds();
	console.log()
	let result = null;
	if (!chat) {
		result = `${year}年${month}月${day}日 ${hour}:${minute}`;
	}
	else if (chat) {
		const cur_year = new Date().getFullYear();
	}

	return result;
};

const compress = ({ width, height }) => {
	let { screenWidth } = wx.getSystemInfoSync();
	let contentWidth = screenWidth * 0.65;
	if (width > contentWidth) {
		height = height * (contentWidth / width);
		width = contentWidth;
	}
	return {
		width,
		height
	}
};

const checkOrientation = function (canvasId) {
	// 取得canvas上下文，然后在这个2px*2px的canvas左上角绘制一个1px*1px的红点，右下角绘制一个1px*1px的黄点。
	const query = wx.createSelectorQuery();
	//在这个场景下canvasId="judgeCanvas"
	query.select(`#${canvasId}`).fields({ node: true, size: true }).exec((res) => {
		const canvas = res[0].node;
		const ctx = canvas.getContext('2d');
		ctx.fillStyle = "red";
		ctx.fillRect(0, 0, 1, 1);
		ctx.fillStyle = "yellow";
		ctx.fillRect(1, 1, 1, 1);
		const that = this;
		// console.log(ctx);
		//一个红点的RGB值和透明度	一个黄点的RGB值和透明度
		const expectedFirstPoint = [255, 0, 0, 255], expectedLastPoint = [255, 255, 0, 255];
		const imageData = ctx.getImageData(0, 0, 2, 2);
		const w = imageData.width, h = imageData.height, data = imageData.data;
		const totalCount = w * h * 4;
		const realFirstPoint = data.slice(0, 4);
		const realLastPoint = data.slice(totalCount - 4, totalCount);
		const isOrientationRight = areTwoArraysIdentical(expectedFirstPoint, realFirstPoint) && areTwoArraysIdentical(expectedLastPoint, realLastPoint);
		console.log(imageData);
		try {
			wx.setStorageSync('isOrientationRight', isOrientationRight); //将当前设备导出图片是否会颠倒的信息储存在storage中，方便以后判断
		} catch (ex) {
			console.error(ex.message);
		};
		console.log('isOrientationRight: ' + isOrientationRight);
		// wx.canvasGetImageData({
		// 	//调用wx.canvasGetImageData获得返回像素点数据，并与已知像素点数据比较是否相同。
		// 	canvasId: canvasId,
		// 	x: 0,
		// 	y: 0,
		// 	width: 2,
		// 	height: 2,
		// 	success(res) {
		// 		console.log('res', res);
		// 		var expectedFirstPoint = [255, 0, 0, 255]; //一个红点的RGB值和透明度
		// 		var expectedLastPoint = [255, 255, 0, 255]; //一个黄点的RGB值和透明度
		// 		var w = res.width;
		// 		var h = res.height;
		// 		var data = res.data;
		// 		var totalCount = w * h * 4;
		// 		var realFirstPoint = data.slice(0, 4);
		// 		var realLastPoint = data.slice(totalCount - 4, totalCount);
		// 		var isOrientationRight = areTwoArraysIdentical(expectedFirstPoint, realFirstPoint) && areTwoArraysIdentical(expectedLastPoint, realLastPoint);
		// 		try {
		// 			wx.setStorageSync('isOrientationRight', isOrientationRight); //将当前设备导出图片是否会颠倒的信息储存在storage中，方便以后判断
		// 		} catch (ex) {
		// 			console.error(ex.message);
		// 		};
		// 		console.log('isOrientationRight: ' + isOrientationRight);
		// 	},
		// 	fail(err) {
		// 		console.error(err);
		// 	}
		// })
	});
};

//	转换处理函数
const getBase64Image = function (canvasId, imgUrl, callback, imgWidth, imgHeight) {
	const query = wx.createSelectorQuery();
	query.select(`#${canvasId}`).fields({ node: true, size: true }).exec((res) => {
		const canvas = res[0].node, canvasWidth = res[0].width, canvasHeight = res[0].height;
		canvas.width = canvasWidth
		canvas.height = canvasHeight
		const ctx = canvas.getContext('2d');
		let img = canvas.createImage();
		img.src = imgUrl;
		img.onload = (e) => {
			console.log(img, imgWidth, imgHeight);
			ctx.drawImage(img, 0, 0, imgWidth || 160, imgHeight || 160);
			const imageData = ctx.getImageData(0, 0, imgWidth || 160, imgHeight || 160);
			let result = imageData;
			const isOrientationRight = wx.getStorageSync('isOrientationRight');
			if (isOrientationRight) {
				result = imageData;
			}
			else {
				result = reverseData(imageData);
			}
			// png编码
			const pngData = upng.encode([result.data.buffer], result.width, result.height);
			// base64编码
			const base64 = wx.arrayBufferToBase64(pngData);
			// const base64Data = 'data:image/png;base64,' + base64;
			callback(base64);
		};
		img.onerror = (e) => {
			wx.hideLoading()
			console.error('err:', e)
		};
	});
};

const reverseData = function (res) {
	var w = res.width;
	var h = res.height;
	let con = 0;
	for (var i = 0; i < h / 2; i++) {
		for (var j = 0; j < w * 4; j++) {
			con = res.data[(i * w * 4 + j) + ""];
			res.data[(i * w * 4 + j) + ""] = res.data[((h - i - 1) * w * 4 + j) + ""];
			res.data[((h - i - 1) * w * 4 + j) + ""] = con;
		}
	}
	return res;
};

//比较两个数组内部信息是否相同
const areTwoArraysIdentical = function (array1, array2) {
	var totalCount = array1.length, isIdentical = true;
	var i;
	for (i = 0; i < totalCount; i++) {
		if (!isIdentical) {
			return isIdentical;  //只要一个不同，直接跳出
		}
		isIdentical = array1[i] === array2[i];
	}
	return isIdentical;
};

const getAdapterheight = () => {
	let adapterHeight = 0;

	let { system, model } = wx.getSystemInfoSync();
	let isIOS = () => {
		return (system.indexOf('iOS') != -1);
	};
	let isIPhoneX = () => {
		return (isIOS() && model.indexOf('X') != -1);
	};

	if (isIPhoneX()) {
		adapterHeight = 35;
	}
	return adapterHeight;
};

const sliceArray = (arr, options) => {
	let result = [];
	let { size } = options;
	let index = 0;
	let len = arr.length;
	let group = Math.ceil(len / size);
	for (let i = 0; i < group; i++) {
		result.push(arr.splice(0, size));
	}
	return result;
}

module.exports = {
	formatTime,
	formatSeconds,
	formatDate,
	sliceArray,
	compress,
	getAdapterheight,
	Trim,
	updUserInfo,
	checkOrientation,
	getBase64Image
}
