### KTV 小程序

git clone 项目到本地后，需要先通过`npm install`下载相关依赖，再通过微信开发者工具再次构建(菜单：工具 ---> 构建 npm)，会生成 miniprogram_npm。

```bash
.
├─ components/          	# 自定义组件
├─ static/              	# 静态资源文件
├─ utils/               	# API请求封装以及公用方法的目录
├─ pages/               	# 主包
│   ├─ chatMessage/     	# 聊天页面，消息列表
│   │   ├─ chat/     		# 消息发送页面
│   ├─ cropImage/      		# 选择图片后的裁剪页面
│   ├─ home/            	# 首页
│   ├─ index/          		# 引导页
│   ├─ ktvRoom/         	# 包厢
│   │   ├─ broadcast/     	# 播控
│   │   ├─ classifyList/    # 曲库二级分类
│   │   ├─ hasOrdered/    	# 已点歌曲
│   │   ├─ musicHall/    	# 曲库二级分类
│   │   ├─ sendBless/    	# 发送祝福
│   │   ├─ songList/    	# 曲库-歌曲列表(点歌)
│   ├─ livePlay/          	# 更多直播=>直播播放页面
│   ├─ login/         		# 大厅
│   │   ├─ component/     	# 登录模块自定义title
│   │   ├─ forgetPassword/    # 忘记密码
│   │   ├─ register/    	# 注册
│   │   ├─ setPassword/    	# 设置密码
│   │   ├─ webView/    	    # 注册协议、隐私政策
│   ├─ lobby/         		# 大厅
│   │   ├─ backSuccess/     	# 退歌成功页面
│   │   ├─ classifyList/    # 点歌曲库二级分类
│   │   ├─ orderSong/    	# 点歌曲库一级分类
│   │   ├─ songList/    	# 点歌->歌单列表（换歌列表）
│   ├─ moreWorks            # 更多推荐、作品、直播
│   ├─ myWallet             # 我的钱包
│   │   ├─ accountBind/     # 提现绑定
│   │   ├─ billstatistics/  # 账单统计
│   │   ├─ cash/    	    # 提现
│   │   ├─ payment/    	    # 支付管理
│   │   ├─ payPassword/    	# 支付密码设置
│   │   ├─ recharge/    	# 充值
│   │   ├─ walletCode/    	# 我的打赏码
│   ├─ programList          # 大厅节目单(退歌、换歌、插歌)
│   ├─ reward               # 打赏(所有打赏全在这里处理)
│   ├─ searchSongList      # 歌曲搜索页面(大厅、包厢)
│   ├─ userCenter/         # 用户中心
│   │   ├─ accountSafety/     # 账号与安全
│   │   ├─ bgImage/     # 背景图修改
│   │   ├─ contactList/     # 关注、粉丝列表
│   │   ├─ editInfo/     # 资料编辑
│   │   ├─ editNickName/     # 修改昵称
│   │   ├─ editSignature/     # 修改签名
│   │   ├─ loginPassword/     # 修改登录密码
│   │   ├─ myQrcode/     # 我的火鸡码
│   │   ├─ myVideo/     # 我的作品(下载、未下载)
│   │   ├─ packetPool/     # 红包池
│   │   ├─ settin/     # 设置
│   │   ├─ verifyCode/     # 验证码验证操作(登录密码、支付密码)
│   ├─ videoPlay      # 更多作品->视频播放页
│   ├─ services.js      # 融云IMSDK二次封装
├── .gitignore          # （配置）需被 Git 忽略的文件（夹）
├── app.js
├── app.wxss			# 全局公用样式
├── app.json			# 全局配置
├── package.json
```


#### 注意：目前系统的资源文件，后续需切换成https安全域名，上线需要审核；