// app.js

App({
	onLaunch() {
		// 展示本地存储能力
		const logs = wx.getStorageSync('logs') || []
		logs.unshift(Date.now())
		wx.setStorageSync('logs', logs)
		// console.log(wx.getMenuButtonBoundingClientRect())
		wx.getSystemInfo({
			success: res => {
				// console.log(res)
				this.globalData.statusBarRight = res.screenWidth - wx.getMenuButtonBoundingClientRect().right
			}
		})

		// 登录
		wx.login({
			success: res => {
				// 发送 res.code 到后台换取 openId, sessionKey, unionId
			}
		})
	},
	onShow() {
		wx.setKeepScreenOn({
			keepScreenOn: true,
			fail: function () {
				wx.setKeepScreenOn({
					keepScreenOn: true
				});
			},
		});
	},
	systemInfo: () => {
		let isIos = false
		try {
			const res = wx.getSystemInfoSync(),
			system = res.system
			system.search('iOS') !== -1 ? isIos = true : isIos = false
		} catch (e) {
				// Do something when catch error
		}
		return isIos
	},
	globalData: {
		// Service: Service,
		userInfo: null,
		statusBarHeight: wx.getSystemInfoSync()['statusBarHeight'],
		statusBarRight: wx.getMenuButtonBoundingClientRect()
	}
})
