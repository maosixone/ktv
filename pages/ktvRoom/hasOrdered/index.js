// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    formatSeconds
} from '../../../utils/util.js';
import {
    API as $API
} from '../../../utils/request';
let payForModal,
    roomInfo,
    cur_page = 1,	// 	当前页
    cur_song_id,	//	点歌song_id
    cur_id;		//	dict_id  歌曲曲风id || 歌曲语言id

Page({
    data: {
        activeSrc: imgHosts + 'active.png',
        noDataSrc: imgHosts + 'home/no_data.png',
        deleteSrc: imgHosts + 'orderSong/delete.png',
        singingSrc: imgHosts + 'orderSong/singing.png',
        live_songSrc: imgHosts + 'orderSong/live_song.png',
        un_live_songSrc: imgHosts + 'orderSong/un_live_song.png',
        record_songSrc: imgHosts + 'orderSong/record_song.png',
        un_record_songSrc: imgHosts + 'orderSong/un_record_song.png',
        showType: '0',
        songList: false, //  已点歌曲列表
        sungList: false, //	已唱列表
        userInfo: {}, //
        visible: false, //
        is_record: false, //	是否录播
        is_live: false, //	是否直播
    },
    // 事件处理函数
    onLoad(options) {
        const userInfo = wx.getStorageSync('user_info');
        roomInfo = wx.getStorageSync('ktv_roomInfo');
        cur_page = 1;
        this.setData({ userInfo });
        this.getSongList(cur_page);
    },
    updShowType(e) {
        const { type } = e.currentTarget.dataset;
        this.setData({ showType: type }, () => {
            this.getSongList(1);
        });
    },
    getMoreSongList() {
        // 获取下一页数据
        // this.getSongList(cur_page);
    },
    getSongList(page = 1, pageSize = 100) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const { showType } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|length|${pageSize}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|start|${(page - 1) * pageSize}|token|${token}`),
            token: token,
            start: (page - 1) * pageSize,
            length: pageSize,
            roombindtoken: roomInfo.roombindtoken,
            ...publicParams
        };
        if (showType == '0') {
            $API.room.getSelectedList(params).then(res => {
                const new_songList = page == 1 ? res.data.songList : [...this.data.songList, ...res.data.songList];
                this.setData({
                    refresher: false,
                    songList: new_songList
                });
                setTimeout(function () {
                    wx.hideLoading();
                    cur_page++;
                }, 0);
            });
        }
        else if (showType == '1') {
            $API.room.getSungList(params).then(res => {
                const new_songList = page == 1 ? res.data.record : [...this.data.sungList, ...res.data.record];
                this.setData({
                    refresher: false,
                    sungList: new_songList
                });
                setTimeout(function () {
                    wx.hideLoading();
                    cur_page++;
                }, 0);
            });
        }
    },
    downRefresh() {
        this.getSongList();
    },
    preventTap() {
        return false;
    },
    closeModal() {
        cur_song_id = null;
        this.setData({
            visible: false,
            is_record: false,
            is_live: false,
        });
    },
    chooseMode(e) {
        const { type, value } = e.currentTarget.dataset;
        if (type == 'record') {
            this.setData({ is_record: value });
        }
        else if (type == 'live') {
            this.setData({ is_live: value });
        }
    },
    handleTopThis(e) {
        wx.showLoading({ title: '加载中', });
        const { songList } = this.data, { id } = e.currentTarget.dataset, token = wx.getStorageSync('sysToken');
        const this_song = songList.find(item => item.songid = id);
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|songidlist|${this_song.songid}|songkplist|${this_song.songkp}|token|${token}`),
            token: token,
            songidlist: this_song.songid,
            songkplist: this_song.songkp,
            roombindtoken: roomInfo.roombindtoken,
            ...publicParams
        }
        $API.room.topSong(params).then(res => {
            this.getSongList(1);
            setTimeout(() => {
                wx.hideLoading();
                wx.showToast({
                    title: '顶歌成功',
                    icon: 'none'
                });
            }, 0);
        });
    },
    handleDeleteThis(e) {
        wx.showLoading({ title: '加载中', });
        const { songList } = this.data, { id } = e.currentTarget.dataset, token = wx.getStorageSync('sysToken');
        const this_song = songList.find(item => item.songid = id);
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|roombindtoken|${roomInfo.roombindtoken}|songidlist|${this_song.songid}|songkplist|${this_song.songkp}|token|${token}`),
            token: token,
            room_id: roomInfo.room_id,
            songidlist: this_song.songid,
            songkplist: this_song.songkp,
            roombindtoken: roomInfo.roombindtoken,
            ...publicParams
        }
        $API.room.deleteSong(params).then(res => {
            this.getSongList(1);
            setTimeout(() => {
                wx.hideLoading();
                wx.showToast({
                    title: '删除成功',
                    icon: 'none'
                });
            }, 0);
        });
    },
    // 切歌
    cutThis(e) {
        const { type } = e.currentTarget.dataset, token = wx.getStorageSync('sysToken'), that = this;
        wx.showLoading({ title: '加载中', });
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|roomplayctrldata|1|roomplayctrltype|${type}|token|${token}`),
            token: token,
            roombindtoken: roomInfo.roombindtoken,
            roomplayctrltype: type,
            roomplayctrldata: 1,
            ...publicParams
        };
        $API.room.playCtrl(params).then(res => {
            setTimeout(() => {
                wx.hideLoading();
                wx.showToast({
                    title: '切歌成功',
                    icon: 'none'
                });
                that.getSongList(1);
            }, 1000);
        });
    },
    startSing() {
        const { is_live, is_record } = this.data;
        if (!is_record) {
            return wx.showToast({
                title: '请选择是否录播',
                icon: 'none'
            });
        }
        else if (!is_live) {
            return wx.showToast({
                title: '请选择是否直播',
                icon: 'none'
            });
        }
        this.pickSong();
    },
    pickSong() {
        const { songList, sungList, showType, is_live, is_record } = this.data, token = wx.getStorageSync('sysToken');

        const this_song = showType === '0' ? songList.find(item => item.songid == cur_song_id) : sungList.find(item => item.songid == cur_song_id);
        let singing_style = (!is_record && !is_live) && '';
        if (is_live === '0' && is_record === '0') {
            singing_style = '0';
        }
        else if (is_live === '0' && is_record === '1') {
            singing_style = '1';
        }
        else if (is_live === '1' && is_record === '0') {
            singing_style = '2';
        }
        else if (is_live === '1' && is_record === '1') {
            singing_style = '3';
        }

        const params = {
            token: token,
            song_name: this_song.songname,
            singing_style: singing_style,
            room_id: roomInfo.room_id,
            ...publicParams
        };

        if (showType === '0') {
            params.song_id = this_song.songid;
            params.sign = md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|singing_style|${singing_style}|song_id|${this_song.songid}|song_name|${this_song.songname}|token|${token}`);
            $API.room.updSingingStyle(params).then(res => {
                this.getSongList(1);
                this.closeModal();
                setTimeout(() => {
                    wx.showToast({
                        title: '修改成功',
                        icon: 'none'
                    });
                }, 0);
            });
        }
        else if (showType === '1') {
            params.songidlist = this_song.songid;
            params.songkplist = this_song.songkp;
            params.roombindtoken = roomInfo.roombindtoken;
            params.sign = md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|roombindtoken|${roomInfo.roombindtoken}|singing_style|${singing_style}|song_name|${this_song.songname}|songidlist|${this_song.songid}|songkplist|${this_song.songkp}|token|${token}`);

            $API.room.pickSong(params).then(res => {
                this.closeModal();
                wx.showToast({
                    title: '添加歌曲成功',
                    icon: 'none'
                });
            });
        }
    },
    jumpPage(e) {
        const { type } = e.currentTarget.dataset;
        let link = null;
        if (type == 1) {
            link = '../../programList/index';
            wx.navigateTo({
                url: link,
            });
        }
        this.setData({ visible: false });
    },
    add_directly(e) {
        const { id } = e.currentTarget.dataset;
        cur_song_id = id;
        this.pickSong();
    },
    updateThis(e) {
        const { id } = e.currentTarget.dataset;
        cur_song_id = id;
        this.setData({ visible: true });
    },
    orderThis(e) {
        const { id } = e.currentTarget.dataset;
        cur_song_id = id;
        this.setData({ visible: true });
    },
})