// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
const { Conversation, Message, ChatRoom } = require('../services');
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    formatSeconds
} from '../../utils/util.js';
import {
    API as $API
} from '../../utils/request';

let stream_name = null, userInfo = null, roomInfoList = [];

Page({
    data: {
        showType: 1,
        recommendLists: false, //  推荐列表
        closeSrc: imgHosts + 'modal_close.png',
        sweepSrc: imgHosts + 'sweep.png',
        controlSrc: imgHosts + 'ktvRoom/control.png',
        librarySrc: imgHosts + 'ktvRoom/library.png',
        selectedSrc: imgHosts + 'selected.png',
        blessingSrc: imgHosts + 'blessing.png',
        back_songSrc: imgHosts + 'back_song.png',
        presentSrc: imgHosts + 'ktvRoom/present.png',
        more_musicSrc: imgHosts + 'more_music.png',
        more_starSrc: imgHosts + 'more_star.png',
        live_endSrc: imgHosts + 'home/live_end.jpg',
        refresher: false, // 当前下拉刷新状态
        refresherText: '下拉刷新',
        sao_visible: false,
        roomInfo: false,
    },
    onLoad() {
        roomInfoList = wx.getStorageSync('roomInfoList');
        this.setData({ roomInfo: wx.getStorageSync('ktv_roomInfo') }, () => {
            stream_name = (roomInfoList && this.data.roomInfo) && roomInfoList.find(item => item.room_id == this.data.roomInfo.room_id) && roomInfoList.find(item => item.room_id == this.data.roomInfo.room_id).stream_name;
        });
        userInfo = wx.getStorageSync('user_info');

    },
    onHide() {
        Conversation.c_watch((list) => {
            return false;
        });
    },
    // 事件处理函数
    onShow() {
        if (typeof this.getTabBar === 'function' && this.getTabBar()) {
            this.getTabBar().setData({
                selected: 2,
                show: true
            });
        }
        Conversation.c_watch((list) => {
            this.getTotalUnreadCount();
        });
        this.getRecommends();
        this.getTotalUnreadCount();
    },
    getTotalUnreadCount() {
        Conversation.getTotalUnreadCount().then((count) => {
            if (count > 0) {
                wx.setTabBarBadge({
                    index: 3,
                    text: count,
                });
            }
            else {
                wx.removeTabBarBadge({ index: 3 });
            }
        });
    },
    getRecommends() {
        const token = wx.getStorageSync('sysToken'),
            {
                roomInfo
            } = this.data;
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|token|${token}`),
            token: token,
            room_id: roomInfo.room_id,
            ...publicParams
        };
        $API.room.recommend(params).then(res => {
            for (let i = 0, len = res.data.length; i < len; i++) {
                res.data[i].format_expiration_time = formatSeconds(Math.abs(res.data[i].expiration_time));
            }
            this.setData({
                refresher: false,
                recommendLists: res.data
            });
        });
    },
    refresherPulling(e) {
        const { dy } = e.detail;
        if (dy > 35) {
            this.setData({
                refresherText: '释放立即刷新'
            });
        }
    },
    downRefresh() {
        this.getRecommends();
        this.setData({ refresherText: '下拉刷新' });
    },
    jumpVideoPlay(e) {
        const {
            id
        } = e.currentTarget.dataset, { recommendLists } = this.data;
        const cur_recommend = recommendLists.find(item => item.id == id);
        if (cur_recommend.recommend_type == 3) {
            wx.navigateTo({
                url: `../videoPlay/index?id=${id}&ad_type=${cur_recommend.ad_type}&url=${cur_recommend.play_url}`,
            });
        } else if (cur_recommend.recommend_type == 2) {
            wx.navigateTo({
                url: `../livePlay/index?id=${id}&broadcast_id=${id}`,
            });
        }
        else {
            wx.navigateTo({
                url: `../videoPlay/index?id=${id}`,
            });
        }
    },
    closeModal() {
        this.setData({
            sao_visible: false
        });
    },
    jumpPage(e) {
        const {
            type
        } = e.currentTarget.dataset, {
            roomInfo
        } = this.data;
        if (!roomInfo) {
            return this.setData({
                sao_visible: true
            });
        }
        let link = null;
        if (type == '1') {
            link = `./broadcast/index`;
        }
        else if (type == '2') {
            link = './musicHall/index';
        }
        else if (type == '3') {
            link = './hasOrdered/index';
        }
        else if (type == '4') {
            link = './sendBless/index';
        }
        else if (type == '5') {
            return this.getRoomLiveUserInfo();
        }
        wx.navigateTo({
            url: link,
        });
    },
    getRoomLiveUserInfo() {
        const { roomInfo } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|token|${token}`),
            token: token,
            room_id: roomInfo.room_id,
            ...publicParams
        };
        $API.room.getRoomLiveUserInfo(params).then(res => {
            if (res.data.isok != 1) {
                return wx.showToast({
                    title: '暂无播放节目',
                    icon: 'none',
                    duration: 2000
                });
            }
            const that = this;
            wx.navigateTo({
                url: `../reward/index?room=true&name=${res.data.nick_name}&live_rm_id=${roomInfo.room_id}&images=${res.data.images}&in_live=false`,
                events: {
                    sendGift: function (data) {
                        console.log(data);
                        const content = {
                            id: userInfo.rong_id,
                            user: {
                                id: userInfo.rong_id,
                                name: userInfo.nick_name,
                                portraitUri: userInfo.images,
                            }
                        };
                        ChatRoom.join(stream_name, content).then(() => {

                            const content = {
                                id: data.data,
                                number: 1,
                                total: 0,
                                user: {
                                    id: userInfo.rong_id,
                                    name: userInfo.nick_name,
                                    portraitUri: userInfo.images,
                                }
                            };
                            // 发送礼物
                            return ChatRoom.sendGift({
                                targetId: stream_name,
                                content,
                            });
                        });
                    },
                    sendText: async function (data) {
                        const result = await that.getRongInfo(res.data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    // 获取融云信息
    getRongInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|ktv_user_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ktv_user_id: id,
            ...publicParams
        };
        return new Promise((resolve, reject) => {
            $API.user.getRongInfo(params).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        });
    },
    getChatInfoById(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`chat_id|${id}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            chat_id: id,
            ...publicParams
        };
        return new Promise((resolve, reject) => {
            $API.user.getChatInfoById(params).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        })
    },
    // 对人打赏
    getUserInfoByPhone(value) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${value}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            phone: value,
            ...publicParams
        };
        $API.user.getUserInfoByPhone(params).then(res => {
            wx.navigateTo({
                url: `../reward/index?user_id=${res.data.ktv_user_id}&name=${res.data.nick_name}&images=${res.data.images}`,
                events: {
                    sendText: function (data) {
                        const content = {
                            targetId: res.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    queryQRCode(value) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_bind_code|${value}|token|${token}`),
            token: token,
            room_bind_code: value,
            ...publicParams
        };
        $API.room.bindingRoom(params).then(res => {
            if (res.data.isok == 1) {
                wx.showToast({
                    title: '绑定成功',
                    icon: 'none',
                    duration: 1500,
                    mask: true
                });
                wx.setStorageSync('ktv_roomInfo', res.data.roomInfo);
                roomInfoList = wx.getStorageSync('roomInfoList')
                if (roomInfoList) {
                    const cur_room = roomInfoList.find(item => item.room_id == res.data.roomInfo.room_id);
                    cur_room && (stream_name = cur_room.stream_name);
                }

                this.setData({
                    roomInfo: res.data.roomInfo
                });
            }
        }).catch(err => {
            wx.showToast({
                title: '包厢码错误',
                icon: 'none',
                duration: 1500,
                mask: true
            });
        });
    },
    scanCode() {
        wx.scanCode({
            success: (res) => {
                console.log('success', res);
                // typeTurkey  1加好友  2大厅  3是红包 	4是打赏
                try {
                    const result = res.result.substring(res.result.indexOf('barcode=')).split('=');
                    console.log('typeof result', typeof result)
                    if (result[1] && result[1] != 'undefined') {
                        this.queryQRCode(result[1]);
                    }
                    else {
                        const new_result = JSON.parse(res.result);
                        console.log(new_result);
                        if (new_result && new_result.typeTurkey == 4) {
                            return this.getUserInfoByPhone(new_result.value);
                        }
                        else if (new_result && new_result.code == '00001') {
                            if (!this.data.roomInfo) {
                                return this.setData({
                                    sao_visible: true
                                });
                            }
                            return this.getRoomLiveUserInfo();
                        }
                        else if (res.result && res.result.length <= 20) {
                            return this.queryQRCode(res.result);
                        }
                        throw new Error;
                    }
                    this.closeModal();
                } catch (e) {
                    let title = '无效二维码'
                    if (JSON.parse(res.result).typeTurkey == 2) {
                        title = '请使用大厅扫一扫'
                    }
                    wx.showToast({
                        title: title,
                        icon: 'none',
                        duration: 1500,
                        mask: true
                    });
                }
            },
            fail: (res) => {
                console.log('fail', res);
                wx.showToast({
                    title: '扫码失败',
                    icon: 'none',
                    duration: 1500,
                    mask: true
                });
            },
        })
    }
})