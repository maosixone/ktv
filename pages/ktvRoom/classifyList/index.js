// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

let new_change_song = false, cur_name = null, bgImageList = [
	'http://livectest.the12f.cn/ktv/file/qufeng-1-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-2-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-3-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-4-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-5-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-6-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-7-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-8-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-9-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-10-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-11-3.png',
	'http://livectest.the12f.cn/ktv/file/qufeng-12-3.png',
];

Page({
	data: {
		searchSrc: imgHosts + 'search.png',
		mtvSrc: imgHosts + 'orderSong/mtv.png',
		liveSrc: imgHosts + 'orderSong/live.png',
		classifyType: '',
		classifyList: [],
	},
	onLoad(option) {
		const {
			type,
			name
		} = option;
		cur_name = name;
		this.setData({ classifyType: type });
		this.getSongClassifyList(type);
	},
	gotoSearch() {
		wx.navigateTo({
			url: `../../searchSongList/index?type=2`,
		});
	},
	getSongClassifyList(type) {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		if (type == 'language') {
			$API.room.getLanguageList(params).then(res => {
				if (res.data.length > 0) {
					for (let i = 0, len = res.data.length; i < len; i++) {
						const bg_index = (i + 1) % 12;
						bg_index == 0 ? res.data[i].classify_icon = bgImageList[11] : res.data[i].classify_icon = bgImageList[bg_index - 1];
					}
					console.log('res.data', res.data);
					this.setData({ classifyList: res.data });
				}
			});
		}
		else if (type == 'style') {
			$API.room.getStyleList(params).then(res => {
				if (res.data.length > 0) {
					for (let i = 0, len = res.data.length; i < len; i++) {
						const bg_index = (i + 1) % 12;
						bg_index == 0 ? res.data[i].classify_icon = bgImageList[11] : res.data[i].classify_icon = bgImageList[bg_index - 1];
					}
					this.setData({ classifyList: res.data });
				}
			});
		}
	},
	jumpPage(e) {
		const { type, id, name } = e.currentTarget.dataset;
		wx.navigateTo({
			url: `../songList/index?type=${type}&name=${name}&id=${id}`,
		});
	}
})