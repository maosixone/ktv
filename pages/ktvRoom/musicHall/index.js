// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

let new_change_song = false;

Page({
	data: {
		searchSrc: imgHosts + 'search.png',
		popular_songSrc: imgHosts + 'orderSong/popular_song.png',
		most_songSrc: imgHosts + 'orderSong/most_song.png',
		reward_songSrc: imgHosts + 'orderSong/reward_song.png',
		all_songSrc: imgHosts + 'orderSong/all_song.png',
		languageSrc: imgHosts + 'orderSong/language.png',
		style_songSrc: imgHosts + 'orderSong/style_song.png',
	},
	onLoad(option) {

	},
	gotoSearch() {
		wx.navigateTo({
			url: `../../searchSongList/index?type=2`,
		});
	},
	jumpPage(e) {
		const {
			type,
			name
		} = e.currentTarget.dataset;
		let link = null;
		switch (type) {
			case 'most':
			case 'change':
			case 'reward':
			case 'popular':
			case 'all':
				link = `../songList/index?type=${type}&name=${name}`;
				break;
			case 'language':
			case 'style':
				link = `../classifyList/index?type=${type}&name=${name}`;
				break;
		}
		wx.navigateTo({
			url: link,
		});
	}
})