// index.js
// 获取应用实例
const app = getApp()
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    formatSeconds
} from '../../../utils/util.js';
import {
    API as $API
} from '../../../utils/request';

Page({
    data: {
        backupSrc: imgHosts + 'ktvRoom/backup.png',
        pauseSrc: imgHosts + 'ktvRoom/pause.png',
        cutSrc: imgHosts + 'ktvRoom/cut.png',
        againSrc: imgHosts + 'ktvRoom/again.png',
        addSrc: imgHosts + 'ktvRoom/add.png',
        minusSrc: imgHosts + 'ktvRoom/minus.png',
        roomInfo: wx.getStorageSync('ktv_roomInfo') || false,
        lightTypeData: false,
        current_lightid: '',
    },
    onShow() {
        this.setData({
            roomInfo: wx.getStorageSync('ktv_roomInfo')
        });
        this.getLightCtrlType();
    },
    getLightCtrlType() {
        const token = wx.getStorageSync('sysToken'), { roomInfo } = this.data;
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|token|${token}`),
            token: token,
            roombindtoken: roomInfo.roombindtoken,
            ...publicParams
        };
        $API.room.getLightCtrlType(params).then(res => {
            this.setData({
                lightTypeData: res.data,
                current_lightid: res.data.lightid,
            });
        });
    },
    playCtrlByType(e) {
        const { type } = e.currentTarget.dataset, token = wx.getStorageSync('sysToken'), { roomInfo } = this.data;
        wx.showLoading({ title: '加载中', });
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|roomplayctrldata|1|roomplayctrltype|${type}|token|${token}`),
            token: token,
            roombindtoken: roomInfo.roombindtoken,
            roomplayctrltype: type,
            roomplayctrldata: 1,
            ...publicParams
        };
        $API.room.playCtrl(params).then(res => {
            setTimeout(() => {
                wx.hideLoading();
                wx.showToast({
                    title: '操作成功',
                });
            }, 0);
        });
    },
    chooseThis(e) {
        const { id } = e.currentTarget.dataset;
        wx.showLoading({ title: '加载中', });
        const token = wx.getStorageSync('sysToken'), { roomInfo, lightTypeData } = this.data;
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|lightctrldata|${id}|lightctrlsubtype|lightid|lightctrltype|${lightTypeData.lightctrltype}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|token|${token}`),
            token: token,
            roombindtoken: roomInfo.roombindtoken,
            lightctrltype: lightTypeData.lightctrltype,
            lightctrldata: id,
            lightctrlsubtype: 'lightid',
            ...publicParams
        };
        $API.room.lightCtrl(params).then(res => {
            this.setData({
                current_lightid: id,
            });
            setTimeout(() => {
                wx.hideLoading();
                wx.showToast({
                    title: '操作成功',
                });
            }, 0);
        });
    }
})
