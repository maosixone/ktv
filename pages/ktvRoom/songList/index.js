// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    formatSeconds
} from '../../../utils/util.js';
import {
    API as $API
} from '../../../utils/request';
let roomInfo,
    cur_page,	// 	当前页
    cur_type,	//	歌库类型
    cur_song_id,	//	点歌song_id
    cur_id;		//	item_value  歌曲曲风id || 歌曲语言id

Page({
    data: {
        searchSrc: imgHosts + 'search.png',
        addSrc: imgHosts + 'orderSong/add.png',
        live_songSrc: imgHosts + 'orderSong/live_song.png',
        un_live_songSrc: imgHosts + 'orderSong/un_live_song.png',
        record_songSrc: imgHosts + 'orderSong/record_song.png',
        un_record_songSrc: imgHosts + 'orderSong/un_record_song.png',
        songList: false, //  节目单列表
        userInfo: {}, //
        visible: false, //
        classify_name: '',
        order_data: false, //
        change_visible: false,
        is_record: false, //	是否录播
        is_live: false, //	是否直播
    },
    // 事件处理函数
    onLoad(options) {
        const userInfo = wx.getStorageSync('user_info');
        roomInfo = wx.getStorageSync('ktv_roomInfo')
        const { type, name, id } = options;
        cur_type = type, cur_id = id, cur_page = 1;
        this.setData({ userInfo, classify_name: name });
        this.getSongList(type, cur_page);
    },
    getMoreSongList() {
        // 获取下一页数据
        this.getSongList(cur_type, cur_page);
    },
    gotoSearch() {
        wx.navigateTo({
            url: `../../searchSongList/index?type=2`,
        });
    },
    getSongList(type, page = 1, pageSize = 20) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const token = wx.getStorageSync('sysToken');
        const params = {
            token: token,
            ...publicParams
        };
        if (['popular', 'most', 'reward'].indexOf(type) > -1) {
            params.sign = md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|position|1|privateKey|${publicParams.privateKey}|token|${token}`);
            params.position = '1';
            params.currentPage = page;
            params.pageSize = pageSize;
        }
        if (type == 'popular') {
            $API.room.getPopularSongList(params).then(res => {
                if (cur_page > res.data.page.totalPageSize) {
                    return wx.showToast({
                        title: '已是最后一页',
                        icon: 'none'
                    });
                }
                if (res.data.MostSongList.errno == 0) {
                    const new_songList = page == 1 ? res.data.MostSongList.result : [...this.data.songList, ...res.data.MostSongList.result];
                    this.setData({
                        refresher: false,
                        songList: new_songList
                    });
                    setTimeout(function () {
                        wx.hideLoading();
                        cur_page++;
                    }, 0);
                }
            });
        }
        else if (type == 'most') {
            $API.room.getMostSongList(params).then(res => {
                if (cur_page > res.data.page.totalPageSize) {
                    return wx.showToast({
                        title: '已是最后一页',
                        icon: 'none'
                    });
                }
                if (res.data.MostSongList.errno == 0) {
                    const new_songList = page == 1 ? res.data.MostSongList.result : [...this.data.songList, ...res.data.MostSongList.result];
                    this.setData({
                        refresher: false,
                        songList: new_songList
                    });
                    setTimeout(function () {
                        wx.hideLoading();
                        cur_page++;
                    }, 0);
                }
            });
        }
        else if (type == 'reward') {
            $API.room.getRewardSongList(params).then(res => {
                if (cur_page > res.data.page.totalPageSize) {
                    return wx.showToast({
                        title: '已是最后一页',
                        icon: 'none'
                    });
                }
                if (res.data.MostSongList.errno == 0) {
                    const new_songList = page == 1 ? res.data.MostSongList.result : [...this.data.songList, ...res.data.MostSongList.result];
                    this.setData({
                        refresher: false,
                        songList: new_songList
                    });
                    setTimeout(function () {
                        wx.hideLoading();
                        cur_page++;
                    }, 0);
                }
            });
        }
        else if (type == 'all') {
            params.roombindtoken = roomInfo.roombindtoken;
            params.start = (page - 1) * pageSize;
            params.length = pageSize;
            params.sign = md5(`dbkey|${publicParams.dbkey}|length|${pageSize}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|start|${(page - 1) * pageSize}|token|${token}`);
            $API.room.getRoomSongList(params).then(res => {
                if (res.data.length < pageSize) {
                    return wx.showToast({
                        title: '已是最后一页',
                        icon: 'none'
                    });
                }
                if (res.data.errno == 0) {
                    const new_songList = page == 1 ? res.data.result : [...this.data.songList, ...res.data.result];
                    this.setData({
                        refresher: false,
                        songList: new_songList
                    });
                    setTimeout(function () {
                        wx.hideLoading();
                        cur_page++;
                    }, 0);
                }
            });
        }
        else if (type == 'language') {
            params.roombindtoken = roomInfo.roombindtoken;
            params.start = (page - 1) * pageSize;
            params.length = pageSize;
            params.song_language_id = cur_id;
            params.sign = md5(`dbkey|${publicParams.dbkey}|length|${pageSize}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|song_language_id|${cur_id}|start|${(page - 1) * pageSize}|token|${token}`);
            $API.room.getRoomSongList(params).then(res => {
                if (res.data.result.length < 1) {
                    return wx.showToast({
                        title: '已是最后一页',
                        icon: 'none'
                    });
                }
                if (res.data.errno == 0) {
                    const new_songList = page == 1 ? res.data.result : [...this.data.songList, ...res.data.result];
                    this.setData({
                        refresher: false,
                        songList: new_songList
                    });
                    setTimeout(function () {
                        wx.hideLoading();
                        cur_page++;
                    }, 0);
                }
            });
        }
        else if (type == 'style') {
            params.roombindtoken = roomInfo.roombindtoken;
            params.start = (page - 1) * pageSize;
            params.length = pageSize;
            params.song_style_id = cur_id;
            params.sign = md5(`dbkey|${publicParams.dbkey}|length|${pageSize}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|song_style_id|${cur_id}|start|${(page - 1) * pageSize}|token|${token}`);
            $API.room.getRoomSongList(params).then(res => {
                if (res.data.result.length < 1) {
                    return wx.showToast({
                        title: '已是最后一页',
                        icon: 'none'
                    });
                }
                if (res.data.errno == 0) {
                    this.setData({
                        refresher: false,
                        songList: res.data.result
                    });
                    setTimeout(function () {
                        wx.hideLoading();
                        cur_page++;
                    }, 0);
                }
            });
        }
    },
    downRefresh() {
        this.getSongList(cur_type);
    },
    closeModal() {
        cur_song_id = null;
        this.setData({
            visible: false,
            change_visible: false,
            is_record: false,
            is_live: false,
        });
    },
    chooseMode(e) {
        const { type, value } = e.currentTarget.dataset;
        if (type == 'record') {
            this.setData({ is_record: value });
        }
        else if (type == 'live') {
            this.setData({ is_live: value });
        }
    },
    startSing() {
        const { is_live, is_record } = this.data;
        if (!is_record) {
            return wx.showToast({
                title: '请选择是否录播',
                icon: 'none'
            });
        }
        else if (!is_live) {
            return wx.showToast({
                title: '请选择是否直播',
                icon: 'none'
            });
        }
        this.pickSong();
    },
    pickSong() {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const { songList, is_live, is_record } = this.data, token = wx.getStorageSync('sysToken');

        const this_song = songList.find(item => item.songid == cur_song_id);
        let singing_style = (!is_record && !is_live) && '';
        if (is_live === '0' && is_record === '0') {
            singing_style = '0';
        }
        else if (is_live === '0' && is_record === '1') {
            singing_style = '1';
        }
        else if (is_live === '1' && is_record === '0') {
            singing_style = '2';
        }
        else if (is_live === '1' && is_record === '1') {
            singing_style = '3';
        }

        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|roombindtoken|${roomInfo.roombindtoken}|singing_style|${singing_style}|song_name|${this_song.songname}|songidlist|${this_song.songid}|songkplist|${this_song.songkp}|token|${token}`),
            token: token,
            songidlist: this_song.songid,
            songkplist: this_song.songkp,
            song_name: this_song.songname,
            singing_style: singing_style,
            roombindtoken: roomInfo.roombindtoken,
            room_id: roomInfo.room_id,
            ...publicParams
        }
        $API.room.pickSong(params).then(res => {
            this.closeModal();
            wx.hideLoading();
            wx.showToast({
                title: '添加歌曲成功',
                icon: 'none'
            });
        });
    },
    jumpPage(e) {
        const { type } = e.currentTarget.dataset;
        let link = null;
        if (type == 1) {
            link = '../../programList/index';
            wx.navigateTo({
                url: link,
            });
        }
        this.setData({ visible: false });
    },
    add_directly(e) {
        const { id } = e.currentTarget.dataset;
        cur_song_id = id;
        this.pickSong();
    },
    orderThis(e) {
        const { id } = e.currentTarget.dataset;
        cur_song_id = id;
        this.setData({ visible: true });
    },
})