// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    formatSeconds
} from '../../../utils/util.js';
import {
    API as $API
} from '../../../utils/request';
let payForModal, roomInfo, wish_time = '1';

Page({
    data: {
        rewardBgSrc: imgHosts + 'reward/reward_bg.png',
        rewardSuccessSrc: imgHosts + 'reward/reward_success.png',
        poster_01Src: imgHosts + 'ktvRoom/poster_01.png',
        poster_02Src: imgHosts + 'ktvRoom/poster_02.png',
        poster_03Src: imgHosts + 'ktvRoom/poster_03.png',
        poster_04Src: imgHosts + 'ktvRoom/posters_04.png',
        poster_05Src: imgHosts + 'ktvRoom/posters_05.png',
        poster_06Src: imgHosts + 'ktvRoom/poster_06.png',
        poster_value: '0',
        duration_value: '0',
        write_value: '',
        preview_visible: false,
        preview_image: imgHosts + 'ktvRoom/poster_01.png',
    },
    onLoad() {
        payForModal = this.selectComponent('#payForModal');
        roomInfo = wx.getStorageSync('ktv_roomInfo');
    },
    changeValue(e) {
        const { type, value, time } = e.currentTarget.dataset, { poster_01Src, poster_02Src, poster_03Src, poster_04Src, poster_05Src, poster_06Src } = this.data;
        if (type == 'poster') {
            let src = poster_01Src;
            switch (value) {
                case '0':
                    src = poster_01Src
                    break;
                case '1':
                    src = poster_02Src
                    break;
                case '2':
                    src = poster_03Src
                    break;
                case '3':
                    src = poster_04Src
                    break;
                case '4':
                    src = poster_05Src
                    break;
                case '5':
                    src = poster_06Src
                    break;
            }
            this.setData({ poster_value: value, preview_image: src });
        }
        else if (type == 'duration') {
            wish_time = time;
            this.setData({ duration_value: value });
        }
    },
    writeInput(e) {
        let { value } = e.detail;
        if (value.length > 30) { value = value.substr(0, 30); }
        this.setData({ write_value: value });
    },
    preview() {
        this.setData({
            preview_visible: true,
        });
    },
    preventTap() {
        return false;
    },
    closeModal() {
        this.setData({
            preview_visible: false,
        });
    },
    wishOrder() {
        setTimeout(function () {
            wx.showLoading({
                title: '加载中',
            });
        }, 0);
        const { duration_value } = this.data;
        payForModal.checkAccount(() => {
            const token = wx.getStorageSync('sysToken');
            const params = {
                sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|8|room_id|${roomInfo.room_id}|token|${token}|wish_fee|${duration_value}`),
                token: token,
                product_id: '8',
                room_id: roomInfo.room_id,
                wish_fee: duration_value,
                ...publicParams
            };
            $API.order.roomWishOrder(params).then(res => {
                if (res.data.isok == 1) {
                    setTimeout(function () {
                        wx.hideLoading()
                    }, 0)
                    payForModal.showModal(res.data.wish_fee, res.data.balance_fee);
                    this.setData({
                        order_data: res.data,
                    });
                }
            });
        });
    },
    // 余额转账
    balance_Payment() {
        const { order_data } = this.data, token = wx.getStorageSync('sysToken');

        const params = {
            sign: md5(`balance_cost|${order_data.wish_fee}|dbkey|${publicParams.dbkey}|order_id|${order_data.order_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            order_id: order_data.order_id,
            balance_cost: order_data.wish_fee,
            ...publicParams
        }
        $API.pay.balancePayment(params).then(res => {
            if (res.data.isok == 1) {
                // 余额支付
                this.roomWish();
            }
        });
    },
    roomWish() {
        const { order_data, write_value, poster_value } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|room_sing_id|${order_data.room_sing_id}|roombindtoken|${roomInfo.roombindtoken}|time|${wish_time}|token|${token}|wish|${write_value}|wish_type|${poster_value}`),
            token: token,
            roombindtoken: roomInfo.roombindtoken,
            room_sing_id: order_data.room_sing_id,
            room_id: roomInfo.room_id,
            wish: write_value,
            time: wish_time,
            wish_type: poster_value,
            ...publicParams
        }
        $API.room.roomWish(params).then(res => {
            if (res.data.isok == 1) {
                // 祝福投屏成功
                payForModal._closeModal();
                wx.showToast({
                    title: '祝福投屏成功',
                    icon: 'none'
                });
            }
        });
    }
})
