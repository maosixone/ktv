// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';;
import {
    publicParams,
    imgHosts
} from '../../config';
import { Trim } from '../../utils/util';
import {
    API as $API
} from '../../utils/request';
let payForModal = null, cur_page = 1, cur_type = '1', roomInfo = null, isSearched = false, cur_song_id = '';	//	当前模式，1: 大厅	2: 包厢

Page({
    data: {
        searchSrc: imgHosts + 'search.png',
        modal_closeSrc: imgHosts + 'modal_close.png',
        deleteSrc: imgHosts + 'delete.png',
        noDataSrc: imgHosts + 'home/no_data.png',
        live_songSrc: imgHosts + 'orderSong/live_song.png',
        un_live_songSrc: imgHosts + 'orderSong/un_live_song.png',
        record_songSrc: imgHosts + 'orderSong/record_song.png',
        un_record_songSrc: imgHosts + 'orderSong/un_record_song.png',
        searchValue: '',
        songList: false,
        showHistoryValue: true,
        historyList: wx.getStorageSync('search_history') || [],
        visible: false,
        suc_visible: false,
        is_record: false, //	是否录播
        is_live: false, //	是否直播
        userInfo: wx.getStorageSync('user_info') || false,
    },
    // 事件处理函数
    onLoad(option) {
        const { type } = option;
        cur_type = type;
        payForModal = this.selectComponent('#payForModal');
        type == 2 && (roomInfo = wx.getStorageSync('ktv_roomInfo'));
    },
    onShow() {
        this.setData({
            historyList: wx.getStorageSync('search_history'),
            userInfo: wx.getStorageSync('user_info'),
        });
    },
    inputChange(e) {
        const { value } = e.detail;
        if (value == '' && isSearched) {
            isSearched = false;
            this.setData({ showHistoryValue: true });
        }
        this.setData({ searchValue: value, });
    },
    clearValue() {
        isSearched = false;
        this.setData({ searchValue: '', showHistoryValue: true });
    },
    clearHistory() {
        wx.removeStorageSync('search_history');
        this.setData({ historyList: [] });
    },
    searchThis(e) {
        const { value } = e.currentTarget.dataset;
        this.setData({ searchValue: value, });
    },
    on_searchList() {
        if (Trim(this.data.searchValue) == '') {
            return wx.showToast({
                title: '请输入搜索内容',
                icon: 'none'
            });
        }
        this.searchList();
    },
    getMoreSongList() {
        this.searchList(cur_page);
    },
    searchList(page = 1, pageSize = 15) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const { searchValue } = this.data, token = wx.getStorageSync('sysToken'), historyNames = wx.getStorageSync('search_history') || [];
        const params = {
            sign: md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|search_name|${searchValue}|token|${token}`),
            token: token,
            pageSize: pageSize,
            currentPage: page,
            ...publicParams
        };
        if (historyNames.indexOf(searchValue) == -1) {
            historyNames.unshift(searchValue);
            wx.setStorageSync('search_history', historyNames);
        }
        else {
            const index = historyNames.indexOf(searchValue);
            historyNames.splice(index, 1);
            historyNames.unshift(searchValue);
        }
        this.setData({ historyList: historyNames });
        if (cur_type == 1) {
            params.search_name = searchValue;
            $API.lobby.getSongSearchPage(params).then(res => {
                if (res.data.length == 0) {
                    setTimeout(function () {
                        wx.hideLoading();
                    }, 0);
                }
                const new_songList = page == 1 ? res.data : [...this.data.songList, ...res.data];
                this.setData({
                    songList: new_songList,
                    showHistoryValue: false,
                });
                setTimeout(function () {
                    wx.hideLoading();
                    cur_page++;
                }, 0);
            });
        }
        else if (cur_type == 2) {
            params.songname = searchValue;
            params.roombindtoken = roomInfo.roombindtoken;
            params.sign = md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|roombindtoken|${roomInfo.roombindtoken}|songname|${searchValue}|token|${token}`)
            $API.room.getSearchSongList(params).then(res => {
                if (res.data.length == 0) {
                    setTimeout(function () {
                        wx.hideLoading();
                    }, 0);
                }
                const new_songList = page == 1 ? res.data : [...this.data.songList, ...res.data];
                this.setData({
                    songList: new_songList,
                    showHistoryValue: false,
                });
                setTimeout(function () {
                    wx.hideLoading();
                    cur_page++;
                }, 0);
            });
        }
        isSearched = true;
    },
    closeModal() {
        cur_song_id = null;
        this.setData({
            visible: false,
            is_record: false,
            is_live: false,
        });
    },
    chooseMode(e) {
        const { type, value } = e.currentTarget.dataset;
        if (type == 'record') {
            this.setData({ is_record: value });
        }
        else if (type == 'live') {
            this.setData({ is_live: value });
        }
    },
    startSing() {
        const { is_live, is_record } = this.data;
        if (!is_record) {
            return wx.showToast({
                title: '请选择是否录播',
                icon: 'none'
            });
        }
        else if (!is_live) {
            return wx.showToast({
                title: '请选择是否直播',
                icon: 'none'
            });
        }
        this.pickSong();
    },
    pickSong() {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const { songList, is_live, is_record } = this.data, token = wx.getStorageSync('sysToken');

        const this_song = songList.find(item => item.song_id == cur_song_id);
        let singing_style = (!is_record && !is_live) && '';
        if (is_live === '0' && is_record === '0') {
            singing_style = '0';
        }
        else if (is_live === '0' && is_record === '1') {
            singing_style = '1';
        }
        else if (is_live === '1' && is_record === '0') {
            singing_style = '2';
        }
        else if (is_live === '1' && is_record === '1') {
            singing_style = '3';
        }

        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${roomInfo.room_id}|roombindtoken|${roomInfo.roombindtoken}|singing_style|${singing_style}|song_name|${this_song.song_name}|songidlist|${this_song.song_id}|songkplist|${this_song.song_kp}|token|${token}`),
            token: token,
            songidlist: this_song.song_id,
            songkplist: this_song.song_kp,
            song_name: this_song.song_name,
            singing_style: singing_style,
            roombindtoken: roomInfo.roombindtoken,
            room_id: roomInfo.room_id,
            ...publicParams
        }
        $API.room.pickSong(params).then(res => {
            this.closeModal();
            wx.hideLoading();
            wx.showToast({
                title: '添加歌曲成功',
                icon: 'none'
            });
        });
    },
    orderThis(e) {
        const { id, song_id, type } = e.currentTarget.dataset;
        const song_data = this.data.songList.find(item => item.km_songid == id);
        if (cur_type == 2) {
            cur_song_id = song_id;
            return this.setData({ visible: true });
        }
        setTimeout(function () {
            wx.showLoading({
                title: '加载中',
            });
        }, 0);
        payForModal.checkAccount(() => {
            const token = wx.getStorageSync('sysToken');
            const params = {
                sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|1|song_id|${id}|song_type|${type}|token|${token}`),
                token: token,
                product_id: '1',
                song_id: id,
                song_type: type,
                ...publicParams
            };
            $API.order.pickSongOrder(params).then(res => {
                if (res.data.isok == 1) {
                    setTimeout(function () {
                        wx.hideLoading()
                    }, 0)
                    payForModal.showModal(res.data.song_cost, res.data.balance_fee);
                    this.setData({
                        new_song: song_data,
                        order_data: res.data,
                    });
                }
            });
        });
    },
    // 余额转账
    balance_Payment() {
        const { order_data } = this.data, token = wx.getStorageSync('sysToken');

        const params = {
            sign: md5(`balance_cost|${order_data.song_cost}|dbkey|${publicParams.dbkey}|order_id|${order_data.order_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            order_id: order_data.order_id,
            balance_cost: order_data.song_cost,
            ...publicParams
        }
        $API.pay.balancePayment(params).then(res => {
            if (res.data.isok == 1) {
                // 余额支付
                this.add_HallProgram();
            }
        });
    },
    add_HallProgram() {
        const { order_data, new_song } = this.data, booth = wx.getStorageSync('cur_booth'), token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`booth_id|${booth.booth_id}|dbkey|${publicParams.dbkey}|order_id|${order_data.order_id}|privateKey|${publicParams.privateKey}|song_id|${new_song.song_id}|song_name|${new_song.song_name}|token|${token}`),
            token: token,
            order_id: order_data.order_id,
            song_id: new_song.song_id,
            song_name: new_song.song_name,
            booth_id: booth.booth_id,
            ...publicParams
        }
        $API.lobby.addHallProgram(params).then(res => {
            if (res.data.isok == 1) {
                // 点歌成功
                payForModal._closeModal();
                this.setData({ suc_visible: true });
            }
        });
    },
    jumpPage(e) {
        const { type } = e.currentTarget.dataset;
        let link = null;
        if (type == 1) {
            link = '/pages/programList/index';
            wx.switchTab({
                url: '/pages/lobby/index',
                success() {
                    wx.navigateTo({
                        url: link,
                    });
                }
            });
        }
        this.setData({ suc_visible: false });
    },
})