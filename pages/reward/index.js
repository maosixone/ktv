// index.js
// 获取应用实例
const app = getApp();
const { ChatRoom } = require('../services');
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    API as $API
} from '../../utils/request';
let eventChannel = null,	// 事件通信通道
    roomInfo = null,	// 包厢信息
    live_room_id = null,
    cur_in_live = null;	// 直播打赏room_id

Page({
    data: {
        rewardBgSrc: imgHosts + 'reward/reward_bg.png',
        closeSrc: imgHosts + 'modal_close.png',
        rewardWorkInfo: false, //  被打赏的（作品或者 大厅节目单）信息
        walletInfo: false, //  钱包信息
        waveEnum: [],
        rewardAmount: 0,
        visible: false,
        Length: 6,
        Value: '',
        isFocus: true,
        order_id: '',
        live_reward: false,
        lobby_reward: false,
        room_reward: false,
        scan_reward: false,
    },
    onLoad(option) {
        const {
            worksId,
            liveId,
            live_rm_id,
            live,
            lobby,
            room,
            in_live,	// 判断是否从直播界面跳转过来
            name,
            images,
            user_id,
        } = option;
        eventChannel = this.getOpenerEventChannel();
        if (in_live && in_live != 'undefined') {
            cur_in_live = in_live;
        }
        if (live_rm_id && live_rm_id != 'undefined') {
            live_room_id = live_rm_id;
            this.setData({
                live_reward: true
            });
        }
        // 大厅打赏
        if (lobby && lobby != 'undefined') {
            const rewardWorkInfo = {
                nick_name: name,
                images: images
            };
            const waveEnum = [
                { name: '1个礼炮', value: 300 },
                { name: '3个礼炮', value: 900 },
                { name: '5个礼炮', value: 1500 },
                { name: '1个花环', value: 250 },
                { name: '3个花环', value: 750 },
                { name: '5个花环', value: 1250 },
                { name: '1个权杖', value: 1800 },
                { name: '1个财富自由', value: 2000 },
                { name: '5个财富自由', value: 8888 },
            ];
            this.setData({ lobby_reward: true, rewardWorkInfo, waveEnum });
        }
        else {
            const waveEnum = [
                { name: '小机车', value: 50 },
                { name: '小超跑', value: 500 },
                { name: '大游艇', value: 800 },
                { name: '大平层', value: 1000 },
                { name: '大别墅', value: 1600 },
                { name: '财富自由', value: 5000 },
            ];
            this.setData({ waveEnum });
        }
        // 作品打赏
        if (worksId && worksId != 'undefined') {
            this.getRewardInfo(worksId);
        }

        // 包厢打赏
        if (room && room != 'undefined') {
            const rewardWorkInfo = {
                nick_name: name,
                images: images
            };
            roomInfo = wx.getStorageSync('ktv_roomInfo');
            this.setData({ room_reward: true, rewardWorkInfo });
        }
        // 扫码打赏
        if (user_id && user_id != 'undefined') {
            const rewardWorkInfo = {
                nick_name: name,
                images: images,
                ktv_user_id: user_id
            };
            this.setData({ scan_reward: true, rewardWorkInfo });
        }
    },
    onShow() {
        this.getThisWalletInfo();
    },
    setWaveValue(e) {
        const {
            wave_value
        } = e.currentTarget.dataset;
        this.setData({
            rewardAmount: wave_value
        });
    },
    customWaveValue(e) {
        const {
            value
        } = e.detail;
        this.setData({
            rewardAmount: value
        });
    },
    // 打赏
    handleReward() {
        const {
            rewardAmount,
            walletInfo
        } = this.data;
        const token = wx.getStorageSync('sysToken');
        if (+rewardAmount <= 0) {
            return wx.showToast({
                title: '请选择打赏金额',
                icon: 'none'
            });
        }
        // 先判断是否开通账户
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getAccountDetail(params).then(res => {
            if (res.data.is_set_pay_password) {
                // 创建余额转账订单
                this.createOrder();
            } else {
                // 未设置支付密码
                wx.showToast({
                    title: '未设置支付密码',
                    success() {
                        wx.navigateTo({
                            url: '../myWallet/payPassword/index',
                        });
                    }
                })
            }
        });
    },
    // 创建打赏订单
    createOrder() {
        const {
            rewardAmount,
            lobby_reward,
            room_reward,
            rewardWorkInfo,
        } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            token: token,
            ...publicParams
        };

        // 包厢打赏订单创建
        if (room_reward) {
            params.reward_fee = rewardAmount;
            params.product_id = '5';
            params.room_id = live_room_id || roomInfo.room_id;
            params.sign = md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|5|reward_fee|${rewardAmount}|room_id|${live_room_id || roomInfo.room_id}|token|${token}`);
            $API.order.roomRewardOrder(params).then(res => {
                const new_rewardWorkInfo = Object.assign({}, rewardWorkInfo, { ktv_user_id: res.data.transfer_to }, res.data);
                this.setData({
                    order_id: res.data.order_id,
                    rewardWorkInfo: new_rewardWorkInfo,
                    isFocus: true,
                    visible: true,
                });
            });
        }
        // 大厅打赏订单创建
        else if (lobby_reward) {
            params.reward_fee = rewardAmount;
            params.product_id = '4';
            params.sign = md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|4|reward_fee|${rewardAmount}|token|${token}`);
            $API.order.rewardSongOrder(params).then(res => {
                const new_rewardWorkInfo = Object.assign({}, rewardWorkInfo, { ktv_user_id: res.data.transfer_to }, res.data);
                this.setData({
                    order_id: res.data.order_id,
                    rewardWorkInfo: new_rewardWorkInfo,
                    isFocus: true,
                    visible: true,
                });
            });
        }
        else {
            params.transfer_fee = rewardAmount;
            params.product_id = '7';
            params.sign = md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|7|token|${token}|transfer_fee|${rewardAmount}`);
            $API.order.transferOrder(params).then(res => {
                this.setData({
                    order_id: res.data.order_id,
                    isFocus: true,
                    visible: true
                });
            });
        }
    },
    // 关闭打赏弹窗
    closeModal() {
        this.setData({
            visible: false,
            Value: ''
        });
    },
    // 输入支付密码
    Focus(e) {
        const inputValue = e.detail.value,
            ilen = inputValue.length,
            token = wx.getStorageSync('sysToken');
        this.setData({
            Value: inputValue
        });
        if (ilen == 6) {
            this.setData({ isFocus: false });
            const params = {
                sign: md5(`dbkey|${publicParams.dbkey}|pay_password|${inputValue}|privateKey|${publicParams.privateKey}|token|${token}`),
                token: token,
                pay_password: inputValue,
                ...publicParams
            };
            $API.pay.payPasswordVerify(params).then(res => {
                // 支付密码验证通过
                this.balanceTransfer();
            }).catch(err => {
                // 密码错误清空密码
                this.setData({
                    Value: '',
                    isFocus: true,
                });
            });
        }
    },
    // 余额转账
    balanceTransfer() {
        const {
            rewardAmount,
            rewardWorkInfo,
            order_id,
            lobby_reward,
            room_reward,
            scan_reward,
            waveEnum
        } = this.data, token = wx.getStorageSync('sysToken');

        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|order_id|${order_id}|privateKey|${publicParams.privateKey}|token|${token}|transfer_money|${rewardAmount}|transfer_to|${rewardWorkInfo.ktv_user_id}`),
            token: token,
            order_id: order_id,
            transfer_money: rewardAmount,
            transfer_to: rewardWorkInfo.ktv_user_id,
            ...publicParams
        }
        $API.pay.balanceTransfer(params).then(res => {
            // 转账成功跳转
            if (lobby_reward) {
                this.rewardHallProgram();
            }
            else if (room_reward) {
                this.roomReward();
            }
            else if (scan_reward) {
                const wave = waveEnum.find(item => item.value === rewardAmount);
                wave && eventChannel.emit('sendText', { name: wave.name, value: wave.value });
                this.closeModal();
                wx.navigateTo({
                    url: './success/index',
                });
            }
            else {
                this.worksReward();
            }
        });
    },
    rewardHallProgram() {
        const {
            rewardAmount,
            rewardWorkInfo,
            live_reward,
            order_id,
            waveEnum
        } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|hall_program_info_id|${rewardWorkInfo.hall_program_info_id}|order_id|${order_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            hall_program_info_id: rewardWorkInfo.hall_program_info_id,
            order_id: order_id,
            ...publicParams
        }
        $API.lobby.rewardHallProgram(params).then(res => {
            // 转账成功跳转
            this.closeModal();
            const wave = waveEnum.find(item => item.value === rewardAmount);
            wave && eventChannel.emit('sendText', { name: wave.name, value: wave.value });
            if (live_reward) {
                eventChannel.emit('sendGift', { data: rewardAmount });
                cur_in_live == 'false' ? wx.navigateTo({
                    url: './success/index',
                }) : wx.navigateBack();
            }
            else {
                wx.navigateTo({
                    url: './success/index',
                });
            }
        });
    },
    roomReward() {
        setTimeout(() => {
            wx.showLoading({
                title: '加载中',
                mask: true,
            });
        }, 0);
        const {
            rewardAmount,
            rewardWorkInfo,
            live_reward,
            order_id,
            waveEnum
        } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|order_id|${order_id}|privateKey|${publicParams.privateKey}|room_id|${live_room_id || roomInfo.room_id}|room_sing_id|${rewardWorkInfo.room_sing_id}|roombindtoken|${roomInfo.roombindtoken || ''}|token|${token}`),
            token: token,
            room_id: live_room_id || roomInfo.room_id,
            order_id: order_id,
            room_sing_id: rewardWorkInfo.room_sing_id,
            roombindtoken: roomInfo.roombindtoken || '',
            ...publicParams
        }
        $API.room.roomReward(params).then(res => {
            // 转账成功跳转
            setTimeout(() => {
                wx.hideLoading();
            }, 0);
            this.closeModal();
            const wave = waveEnum.find(item => item.value === rewardAmount);
            wave && eventChannel.emit('sendText', { name: wave.name, value: wave.value });
            if (live_reward) {
                eventChannel.emit('sendGift', { data: rewardAmount });
                cur_in_live == 'false' ? wx.navigateTo({
                    url: './success/index',
                }) : wx.navigateBack();
            }
            else {
                wx.navigateTo({
                    url: './success/index',
                });
            }
        });
    },
    worksReward() {
        const {
            rewardAmount,
            rewardWorkInfo,
            order_id,
            waveEnum
        } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|order_id|${order_id}|privateKey|${publicParams.privateKey}|token|${token}|version|${rewardWorkInfo.version}|works_id|${rewardWorkInfo.works_id}`),
            token: token,
            works_id: rewardWorkInfo.works_id,
            order_id: order_id,
            version: rewardWorkInfo.version,
            ...publicParams
        }
        $API.works.worksReward(params).then(res => {
            // 转账成功跳转
            this.closeModal();
            const wave = waveEnum.find(item => item.value === rewardAmount);
            wave && eventChannel.emit('sendText', { ktv_user_id: rewardWorkInfo.ktv_user_id, nick_name: rewardWorkInfo.nick_name, name: wave.name, value: wave.value });
            wx.navigateTo({
                url: './success/index',
            });
        });
    },
    // 跳转充值页面
    jumpToRecharge() {
        wx.navigateTo({
            url: '../myWallet/recharge/index',
        });
    },
    Tap() {
        this.setData({
            isFocus: true
        });
    },
    // 获取钱包信息
    getThisWalletInfo() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getWalletInfo(params).then(res => {
            this.setData({
                walletInfo: res.data
            });
        });
    },
    getLiveInfoById(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|live_broadcast_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            live_broadcast_id: id,
            ...publicParams
        };
        $API.works.getLiveInfoById(params).then(res => {
            this.setData({
                rewardWorkInfo: res.data
            });
        });
    },
    getRewardInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${id}`),
            token: token,
            works_id: id,
            ...publicParams
        };
        $API.works.getWorksInfoById(params).then(res => {
            this.setData({
                rewardWorkInfo: res.data
            });
        });
    }
})