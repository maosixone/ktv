// index.js
// 获取应用实例
const app = getApp()
import { imgHosts } from '../../../config';

Page({
	data: {
		rewardBgSrc: imgHosts + 'reward/reward_bg.png',
		rewardSuccessSrc: imgHosts + 'reward/reward_success.png',
	},
	onLoad() {

	},
	continue() {
		wx.navigateBack({ delta: 2 });
	},
})
