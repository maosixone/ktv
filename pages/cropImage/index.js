// index.js
// 获取应用实例
const app = getApp();

import md5 from 'md5';
import {
	hosts,
	publicParams,
	imgHosts
} from '../../config';
import {
	API as $API
} from '../../utils/request';

let cropper, eventChannel;

Page({
	data: {
		type: '',
		toRightSrc: imgHosts + 'to_right.png',
		image_visible: false,
		user_image: '',
		aspectRatio: 1,
		isProportion: true,
		quality: 1,
	},
	onLoad(option) {
		const { type, url } = option;
		cropper = this.selectComponent('#my-cropper');
		eventChannel = this.getOpenerEventChannel();
		let aspectRatio;
		if (type == 'bg') {
			aspectRatio = 1.7;
		}
		else if (type == 'hi') {
			aspectRatio = 1;
		}
		this.setData({
			type,
			user_image: option.url,
			aspectRatio,
		});
	},
	cancelUpd() {
		wx.navigateBack();
	},
	getResult() {
		cropper.getImagePath(res => {
			const token = wx.getStorageSync('sysToken');

			if (this.data.type == 'bg') {
				wx.uploadFile({
					url: `${hosts}/api/personal/updUserBackImages`, //仅为示例，非真实的接口地址
					filePath: res.path,
					name: 'file',
					formData: {
						'sign': md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
						'token': token,
						...publicParams,
					},
					success(res) {
						const data = JSON.parse(res.data);
						//do something
						if (data.code == 10000) {
							const images = data.data.background_picture;
							eventChannel.emit('finished', {
								data: images
							});
							wx.navigateBack();
						}
					}
				});
			}
			else if (this.data.type == 'hi') {
				wx.uploadFile({
					url: `${hosts}/api/personal/updUserImages`, //仅为示例，非真实的接口地址
					filePath: res.path,
					name: 'file',
					formData: {
						'sign': md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
						'token': token,
						...publicParams,
					},
					success(res) {
						const data = JSON.parse(res.data);
						//do something
						if (data.code == 10000) {
							const images = data.data.images;
							eventChannel.emit('finished', {
								data: images
							});
							wx.navigateBack();
						}
					}
				});
			}

		});
	}
})