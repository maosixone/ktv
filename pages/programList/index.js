// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    formatSeconds
} from '../../utils/util.js';
import {
    API as $API
} from '../../utils/request';
let cur_id, cur_song_id, payForModal;

Page({
    data: {
        noDataSrc: imgHosts + 'home/no_data.png',
        interlude_songSrc: imgHosts + 'interlude_song.png',
        change_songSrc: imgHosts + 'change_song.png',
        back_songSrc: imgHosts + 'back_song.png',
        red_packetSrc: imgHosts + 'red_packet.png',
        refresher: false, // 当前下拉刷新状态
        programList: false, //  节目单列表
        packetList: false,	//	红包列表
        handle_visible: false,
        rule_visible: false,
        cur_handle: false,
        order_id: '',
        userInfo: {},
        handle_type: '',
        headerTitle: '节目单',
    },
    // 事件处理函数
    onLoad(option) {
        const {
            type
        } = option, userInfo = wx.getStorageSync('user_info');
        if (type == 'insert') {
            this.setData({
                headerTitle: '插歌'
            });
        } else if (type == 'change') {
            this.setData({
                headerTitle: '换歌',
            });
        } else if (type == 'back') {
            this.setData({
                headerTitle: '退歌'
            });
        }
        this.setData({
            userInfo,
            handle_type: type
        });
        payForModal = this.selectComponent('#payForModal');

        // this.getHallProgramList();
    },
    onShow() {
        this.getHallProgramList();
    },
    showRule() {
        this.setData({ rule_visible: true });
    },
    closeRule() {
        this.setData({ rule_visible: false });
    },
    // 获取节目单列表
    getHallProgramList() {
        setTimeout(function () {
            wx.showLoading({
                title: '加载中',
            });
        }, 0);
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.lobby.getHallProgramList(params).then(res => {
            // for (let i = 0, len = res.data.length; i < len; i++) {
            // 	res.data[i].format_expiration_time = formatSeconds(Math.abs(res.data[i].expiration_time));
            // }
            this.setData({
                refresher: false,
                programList: res.data
            });
            setTimeout(function () {
                wx.hideLoading()
            }, 0);
            this.getPacketList(res.data);
        })
    },
    // 获取红包列表
    getPacketList(list) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.lobby.getPacketList(params).then(res => {
            if (list.length > 0) {
                const new_data = [];
                for (let i = 0, len = res.data.length; i < len; i++) {
                    if (new_data.length == 0) {
                        new_data.push(res.data[i]);
                    }
                    else if (new_data[new_data.length - 1].sort != res.data[i].sort) {
                        new_data.push(res.data[i]);
                    }
                }
                for (let i = 0, len = list.length; i < len; i++) {
                    for (let j = 0; j < new_data.length; j++) {
                        if (i == new_data[j].sort) {
                            list[i].has_packet = true;
                        }
                    }
                }
            }
            this.setData({
                programList: list
            });
        })
    },
    // 插歌订单
    insertSongOrder() {
        const index = this.data.programList.findIndex(item => item.hall_program_info_id == cur_id);
        if (this.data.programList[index].rule_type == 1 || this.data.programList[index].rule_type == 2) {
            return wx.showToast({
                title: '当前歌曲不可插歌',
                icon: 'none'
            });
        }
        setTimeout(function () {
            wx.showLoading({
                title: '加载中',
            });
        }, 0);
        payForModal.checkAccount(() => {
            const token = wx.getStorageSync('sysToken');
            const params = {
                sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|2|token|${token}`),
                token: token,
                product_id: '2',
                ...publicParams
            };
            $API.order.insertSongOrder(params).then(res => {
                if (res.data.isok == 1) {
                    setTimeout(function () {
                        wx.hideLoading()
                    }, 0);
                    payForModal.showModal(res.data.song_cost, res.data.balance_fee);
                    this.setData({
                        order_id: res.data.order_id,
                    });
                }
            });
        });
    },
    // 插歌支付密码输入成功后的监听
    paySuccess_event() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|hall_program_info_id|${cur_id}|order_id|${this.data.order_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            order_id: this.data.order_id,
            hall_program_info_id: cur_id,
            ...publicParams
        }
        $API.lobby.insertHallProgram(params).then(res => {
            if (res.data.isok == 1) {
                // 插歌
                payForModal._closeModal();
                this.closeModal();
                wx.showToast({
                    title: '插歌成功',
                    icon: 'none',
                });
                this.getHallProgramList();
            }
        });
    },
    // 换歌
    changeSong() {
        const index = this.data.programList.findIndex(item => item.hall_program_info_id == cur_id);
        wx.setStorageSync('change_song', this.data.programList[index]);
        wx.navigateTo({
            url: `../lobby/orderSong/index?change=true`,
        });
    },
    // 退歌
    backSong() {
        const index = this.data.programList.findIndex(item => item.hall_program_info_id == cur_id);
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|hall_program_info_id|${cur_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            hall_program_info_id: cur_id,
            ...publicParams
        };
        $API.lobby.quitHallProgram(params).then(res => {
            if (res.data.isok == 1) {
                // 退歌
                wx.navigateTo({
                    url: `../lobby/backSuccess/index?song_name=${this.data.programList[index].song_name}&back_fee=${res.data.back_fee}&balance_fee=${res.data.balance_fee}`,
                });
                this.closeModal();
            }
        });
    },
    showModal(e) {
        const {
            id,
            song_id
        } = e.currentTarget.dataset;
        cur_id = id, cur_song_id = song_id;
        this.setData({
            handle_visible: 1
        });
    },
    closeModal() {
        cur_id = '';
        this.setData({
            cur_handle: false,
            handle_visible: false,
            rule_visible: false,
            order_id: ''
        });
    },
    preventTap() {
        return false;
    },
    chooseThis(e) {
        const {
            type
        } = e.currentTarget.dataset;
        this.setData({
            cur_handle: type
        });
    },
    downRefresh() {
        this.getHallProgramList();
    },
    jumpVideoPlay(e) {
        const {
            id
        } = e.currentTarget.dataset;
        wx.navigateTo({
            url: `../videoPlay/index?id=${id}`,
        });
    },
    // 首页选择功能进入
    handleThis(e) {
        const {
            handle_type
        } = this.data, {
            id,
            song_id
        } = e.currentTarget.dataset;
        cur_id = id, cur_song_id = song_id;
        const index = this.data.programList.findIndex(item => item.hall_program_info_id == cur_id);
        if (handle_type == 'back') {
            if (index == 0) {
                return wx.showToast({
                    title: '第一首不允许退歌',
                    icon: 'none'
                });
            }
            this.setData({
                handle_visible: 2
            });
        } else if (handle_type == 'change') {
            if (index < 3) {
                return wx.showToast({
                    title: '前三首不允许换歌',
                    icon: 'none'
                });
            }
            if (this.data.programList[index].change_count > 0) {
                return wx.showToast({
                    title: '已换过歌',
                    icon: 'none'
                });
            }
            this.changeSong();
        } else if (handle_type == 'insert') {
            if (index == 0) {
                return wx.showToast({
                    title: '第一首歌不可以插歌',
                    icon: 'none'
                });
            }
            this.insertSongOrder();
        }
    },
    // 节目单选择操作
    confirm() {
        const {
            cur_handle
        } = this.data;
        const index = this.data.programList.findIndex(item => item.hall_program_info_id == cur_id);
        if (cur_handle == 1) {
            // 插歌
            if (index == 0) {
                wx.showToast({
                    title: '第一首歌不可以插歌',
                    icon: 'none'
                });
            }
            else {
                this.insertSongOrder();
            }
            this.setData({
                handle_visible: false,
            });
        } else if (cur_handle == 2) {
            // 换歌
            if (index < 3) {
                wx.showToast({
                    title: '前三首不允许换歌',
                    icon: 'none'
                });
            }
            else if (this.data.programList[index].change_count > 0) {
                wx.showToast({
                    title: '已换过歌',
                    icon: 'none'
                });
            }
            else {
                this.changeSong();
            }
            this.setData({
                handle_visible: false,
            });
        } else if (cur_handle == 3) {
            // 退歌
            if (index == 0) {
                this.setData({
                    handle_visible: false,
                });
                return wx.showToast({
                    title: '第一首不允许退歌',
                    icon: 'none'
                });
            }
            this.setData({
                handle_visible: 2
            });
        }
    },
})