// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	imgHosts,
	publicParams,
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

const date = new Date();
const years = [], months = [], days = [];

for (let i = 1990; i <= date.getFullYear(); i++) {
	years.push(i)
}

for (let i = 1; i <= 12; i++) {
	months.push(i)
}

for (let i = 1; i <= 31; i++) {
	days.push(i)
}

Page({
	data: {
		toRightSrc: imgHosts + 'to_right.png',
		userInfo: false,
		image_visible: false,
		sex_visible: false,
		birthday_visible: false,
		area_visible: false,
		years,
		year: date.getFullYear(),
		months,
		month: date.getMonth() + 1,
		days,
		day: date.getDate(),
		current_sex: null,
		sex_value: [],
		birthday_value: [years.findIndex(item => item == date.getFullYear()), months.findIndex(item => item == date.getMonth() + 1), days.findIndex(item => item == date.getDate())],
		provinceList: [],
		province: '',
		cityList: {},
		city: '',
		area_value: [0, 0],
	},
	onLoad() {
		this.getUserInfo();
		const provinceList = wx.getStorageSync('provinceList'), cityList = wx.getStorageSync('cityList');
		if (cityList) {
			this.setData({ cityList: cityList });
		}
		if (provinceList) {
			this.setData({
				provinceList: provinceList
			});
		}
		else {
			this.getProvinceList();
		}
	},
	onShow() {
		this.getUserInfo();
	},
	preventTap() {
		return false;
	},
	cancelModal() {
		this.setData({
			image_visible: false,
			sex_visible: false,
			birthday_visible: false,
			area_visible: false,
		});
	},
	jumpPage(e) {
		const {
			type
		} = e.currentTarget.dataset;
		let link = null;
		if (type == 1) {
			return this.setData({
				image_visible: true,
			});
		} else if (type == 2) {

		} else if (type == 3) {
			link = `../editNickName/index?nick_name=${this.data.userInfo.nick_name}`;
		} else if (type == 4) {
			return this.setData({
				sex_value: [+this.data.userInfo.sex],
				sex_visible: true,
			});
		} else if (type == 5) {
			if (this.data.userInfo.birthday) {
				const val = this.data.userInfo.birthday.split('-'), { years, year, months, month, days, day } = this.data;
				const yearIndex = years.findIndex(item => item == val[0]), monthIndex = months.findIndex(item => item == val[1]), dayIndex = days.findIndex(item => item == val[2]);
				this.setData({
					birthday_value: [yearIndex, monthIndex, dayIndex],
				});
			}
			return this.setData({
				birthday_visible: true,
			});
		} else if (type == 6) {
			link = '../myQrcode/index';
		} else if (type == 7) {
			link = `../editSignature/index?personal_signature=${this.data.userInfo.personal_signature}`;
		} else if (type == 8) {
			const { provinceList, cityList, userInfo } = this.data;
			const provinceIndex = userInfo.province_id > 0 ? provinceList.findIndex(item => item.id == userInfo.province_id) : 0,
				cityIndex = userInfo.city_id > 0 ? cityList[userInfo.province_id].findIndex(item => item.id == userInfo.city_id) : 0;
			return this.setData({
				province: provinceList[provinceIndex],
				area_value: [provinceIndex, cityIndex],
				area_visible: true,
			});
		}
		wx.navigateTo({
			url: link,
		});
	},
	chooseImage(e) {
		const { type } = e.currentTarget.dataset, token = wx.getStorageSync('sysToken'), newUserInfo = Object.assign({}, this.data.userInfo), that = this;
		wx.chooseImage({
			count: 1,
			sizeType: ['original', 'compressed'],
			sourceType: [type],
			success: (res) => {
				const tempFilePaths = res.tempFilePaths;
				console.log(tempFilePaths);
				that.setData({ image_visible: false });
				wx.navigateTo({
					url: `../../cropImage/index?type=hi&url=${tempFilePaths}`,
					complete: (res) => { },
					events: {
						finished: (data) => {
							const images = data.data;
							newUserInfo.images = images;
							that.setData({ userInfo: newUserInfo });
						}
					},
				});
				return;
			},
			complete: (res) => { },
		})
	},
	changeSexEnd(e) {
		const val = e.detail.value;
		this.setData({ current_sex: val[0] });
	},
	// 性别更新
	chooseSex() {
		const token = wx.getStorageSync('sysToken'), newUserInfo = Object.assign({}, this.data.userInfo);
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|sex|${this.data.current_sex}|token|${token}`),
			token: token,
			sex: this.data.current_sex,
			...publicParams,
		};
		$API.user.updateUserInfo(params).then(res => {
			if (res.data.isok == 1) {
				// 更新性别成功
				newUserInfo.sex = this.data.current_sex;
				this.setData({ userInfo: newUserInfo, sex_visible: false });
			}
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
	// 生日更新
	chooseBirthday() {
		const { year, month, day } = this.data, token = wx.getStorageSync('sysToken'), newUserInfo = Object.assign({}, this.data.userInfo);
		const params = {
			sign: md5(`birthday|${year}-${month}-${day}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			birthday: `${year}-${month}-${day}`,
			...publicParams,
		};
		$API.user.updateUserInfo(params).then(res => {
			if (res.data.isok == 1) {
				// 更新生日成功
				newUserInfo.birthday = `${year}-${month}-${day}`;
				this.setData({ userInfo: newUserInfo, birthday_visible: false });
			}
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
	// 地区更新
	chooseArea() {
		const { province, city } = this.data, token = wx.getStorageSync('sysToken'), newUserInfo = Object.assign({}, this.data.userInfo);
		const params = {
			sign: md5(`city_id|${city.id}|city_name|${city.area_name}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|province_id|${province.id}|province_name|${province.area_name}|token|${token}`),
			token: token,
			province_id: province.id,
			city_id: city.id,
			province_name: province.area_name,
			city_name: city.area_name,
			...publicParams,
		};
		$API.user.updateUserInfo(params).then(res => {
			if (res.data.isok == 1) {
				// 更新成功
				newUserInfo.province_id = province.id,
					newUserInfo.province_name = province.area_name,
					newUserInfo.city_id = city.id,
					newUserInfo.city_name = city.area_name,
					this.setData({ userInfo: newUserInfo, area_visible: false });
			}
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
	// 生日选择切换监听
	bind_birthdayChange(e) {
		const val = e.detail.value;
		this.setData({
			year: this.data.years[val[0]],
			month: this.data.months[val[1]],
			day: this.data.days[val[2]],
		});
	},
	// 地区选择切换监听
	bind_areaChange(e) {
		const val = e.detail.value;
		(val[1] < 0) && (val[1] = 0);
		const province_id = this.data.provinceList[val[0]].id;
		this.setData({
			province: this.data.provinceList[val[0]],
			city: this.data.cityList[province_id][val[1]] || {
				area_name: '',
				id: 0,
			},
		});
		console.log('this.data.cityList[province_id][val[1]]', this.data.cityList[province_id][val[1]]);
	},
	getUserInfo() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getUser(params).then(res => {
			this.setData({
				userInfo: res.data
			});
		});
	},
	getProvinceList() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getProvinceList(params).then(res => {
			wx.setStorageSync('provinceList', res.data);
			this.setData({
				provinceList: res.data
			});
			// for (let i = 0, len = res.data.length; i < len; i++) {
			// 	this.getCityList(res.data[i].id);
			// }
			const cityList = wx.getStorageSync('cityList');
			if (!cityList) {
				this.recursion(res.data);
			}
			else {
				this.setData({ cityList: cityList });
			}
		});
	},
	async recursion(arr) {
		let cityList = {};
		for (let i = 0, len = arr.length; i < len; i++) {
			const results = await this.getCityList(arr[i].id);
			cityList = Object.assign(cityList, results);
		}
		// console.log(cityList);
		wx.setStorageSync('cityList', cityList);
		return this.setData({ cityList: cityList });
	},
	getCityList(id) {
		return new Promise((resolve, reject) => {
			const token = wx.getStorageSync('sysToken');
			const params = {
				sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|province_id|${id}|token|${token}`),
				token: token,
				province_id: id,
				...publicParams
			};
			$API.user.getCityList(params).then(res => {
				const new_cityList = {};
				new_cityList[id] = res.data;
				resolve(new_cityList);
				// this.setData({
				// 	cityList: new_cityList,
				// });
			});
		});
	}
})