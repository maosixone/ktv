// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		downloadSrc: imgHosts + 'user/download.png',
		scanSrc: imgHosts + 'user/scan_icon.png',
		userInfo: false,
	},
	onLoad() {
		this.getUserInfo();
	},
	jumpPage(e) {
		const {
			type
		} = e.currentTarget.dataset;
		let link = null;
		if (type == 3) {
			link = '../loginPassword/index';
		}
		wx.navigateTo({
			url: link,
		});
	},
	saveQrCode() {
		setTimeout(function () {
			wx.showToast({
				title: '下载中',
				icon: 'loading',
				duration: 9999999,
				mask: true
			})
		}, 0);
		this.downloadFile(this.data.userInfo.qr_code);
	},
	scanCode() {
		wx.scanCode({
			success: (res) => {
				// typeTurkey  1加好友  2大厅  3是红包 	4是打赏
				try {
					const result = JSON.parse(res.result);
					if (result && result.typeTurkey == 1) {
						this.queryQRCode(result.value);
					}
				}
				catch (e) {
					wx.showToast({
						title: '无效二维码',
						icon: 'none',
						duration: 1500,
						mask: true
					});
				}
			},
			complete: (res) => {
				console.log('complete', res);
			},
		});
	},
	queryQRCode(value) {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|qrcode|${value}|token|${token}`),
			token: token,
			qrcode: value,
			...publicParams
		};
		$API.user.queryQRCode(params).then(res => {
			if (res.data.isok == 1) {
				wx.showToast({
					title: '添加成功',
					icon: 'none',
					duration: 1500,
					mask: true
				});
			}
		});
	},
	downloadFile(url) {
		const downloadTask = wx.downloadFile({
			url: url,
			success: res => {
				// console.log(res.filePath);
				let tempFilePath = res.tempFilePath;
				wx.saveImageToPhotosAlbum({
					filePath: tempFilePath,
					success(res) {
						console.log(res.errMsg)
						setTimeout(function () {
							wx.showToast({
								title: '保存成功',
								icon: 'success',
								duration: 1500,
								mask: true
							});
						}, 0);
						// 要删除的文件(本地路径)
						// let fileMgr = wx.getFileSystemManager();
						// fileMgr.unlink({
						// 	filePath: wx.env.USER_DATA_PATH + '/' + fileName + '.mp4',
						// 	success: function (r) {

						// 	},
						// });
					}
				})
			},
			fail: err => {
				console.log('downloadThis-err', err);
			}
		});
		downloadTask.onProgressUpdate(res => {

		});
	},
	getUserInfo() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getUser(params).then(res => {
			this.setData({
				userInfo: res.data
			});
		});
	},
});