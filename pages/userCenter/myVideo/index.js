// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    API as $API
} from '../../../utils/request';

let works_rule = null, cur_worksId = null, cur_index = null, download_workInfo = null;	// 要下载的作品详情

Page({
    data: {
        is_download: 0,
        has_ownWorkLists: false, //  已下载作品列表
        no_ownWorkLists: false, //  未下载作品列表
        liveLists: false, //  直播列表
        currentPage: 1, //  当前页码
        recommendCurrent: 0,
        activeSrc: imgHosts + 'active.png',
        more_musicSrc: imgHosts + 'more_music.png',
        more_starSrc: imgHosts + 'more_star.png',
        downloadSrc: imgHosts + 'user/download.png',
        deleteSrc: '../../../static/images/delete.png',
        closeSrc: imgHosts + 'modal_close.png',
        order_id: '', //	订单id
        payAmount: '', //	支付火鸡币
        walletInfo: false, //	钱包信息
        Length: 6,
        Value: '',
        isFocus: true,
        visible: false,
        delete_visible: false,
        down_visible: false,
        down_animation: false,
        // 下载进度存储
        progress_list: {},
    },
    // 事件处理函数
    onLoad() {
        this.getOwnWorkLists();
        setTimeout(() => {
            this.getWorksRule();
        }, 100);
    },
    handleDownload(e) {
        const {
            id,
            index,
        } = e.currentTarget.dataset;
        if (JSON.stringify(this.data.progress_list) != '{}') {
            return wx.showToast({
                title: '当前有下载任务，请稍后再试',
                icon: 'none',
                duration: 1500,
            });
        }
        if (this.data.is_download == 0) {
            cur_worksId = id;
            cur_index = index;
            this.setData({
                down_visible: true,
            });
        }
    },
    deleteThis(e) {
        const {
            id,
        } = e.currentTarget.dataset;
        if (!id) {
            return;
        }
        cur_worksId = id;
        this.setData({ delete_visible: true });
    },
    deleteVideo() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${cur_worksId}`),
            token: token,
            works_id: cur_worksId,
            ...publicParams
        };
        $API.works.deleteWorks(params).then(res => {
            const that = this;
            wx.showToast({
                title: '删除成功',
                icon: 'none',
                success() {
                    that.closeModal();
                    that.getOwnWorkLists();
                }
            });
        })
    },
    downloadThis(e) {
        const {
            id,
            index
        } = e.currentTarget.dataset;
        console.log(this.data.progress_list);
        if (JSON.stringify(this.data.progress_list) != '{}') {
            return wx.showToast({
                title: '当前有下载任务，请稍后再试',
                icon: 'none',
                duration: 1500,
            });
        }
        if (!id) {
            return;
        }
        cur_worksId = id;
        cur_index = index;
        if (this.data.is_download == 0) {
            const work_item = this.data.no_ownWorkLists.find(item => item.works_id == id);
            this.downloadFile('in_com', work_item);
        } else {
            const work_item = this.data.has_ownWorkLists.find(item => item.works_id == id);
            if (work_item.all_works_mp4_url.length > 0) {
                this.downloadFile('com', work_item);
            }
            else {
                this.downloadFile('in_com', work_item);
            }
        }
    },
    downloadFile(type = 'in_com', item) {
        this.setData({
            down_visible: false,
        });
        setTimeout(function () {
            wx.showToast({
                title: '开始下载',
                icon: 'loading',
                duration: 1500,
                mask: true
            })
        }, 0);
        const downloadTask = wx.downloadFile({
            // url: 'https://bsc.96koo.net/1/source/20220108/BHrwYrkRXe/BHrwYrkRXe.mp4',
            url: type == 'com' ? item.all_works_mp4_url : item.works_mp4_url,
            // filePath: `${wx.env.USER_DATA_PATH}/${item.song_name}.mp4`,
            success: res => {
                // console.log(res.filePath);
                let tempFilePath = res.tempFilePath;
                wx.saveVideoToPhotosAlbum({
                    filePath: tempFilePath,
                    success(res) {
                        // console.log(res.errMsg)
                        setTimeout(function () {
                            wx.showToast({
                                title: '保存成功',
                                icon: 'success',
                                duration: 1500,
                                mask: true
                            })
                        }, 0);
                        // 要删除的文件(本地路径)
                        // let fileMgr = wx.getFileSystemManager();
                        // fileMgr.unlink({
                        // 	filePath: wx.env.USER_DATA_PATH + '/' + fileName + '.mp4',
                        // 	success: function (r) {

                        // 	},
                        // });
                    },
                    fail() {
                        setTimeout(function () {
                            wx.showToast({
                                title: '保存失败',
                                icon: 'error',
                                duration: 1500,
                                mask: true
                            })
                        }, 0);
                    }
                })
            },
            fail: err => {
                console.log('downloadThis-err', err);
            }
        });
        downloadTask.onProgressUpdate(res => {
            const data = Object.assign({}, this.data.progress_list);
            data[`${cur_worksId}_${cur_index}`] = res.progress;
            if (res.progress == 100) {
                return this.setData({ progress_list: {} });
            }
            this.setData({ progress_list: data });
        });
    },
    // 选择下载类型
    chooseDownload(e) {
        const {
            type
        } = e.currentTarget.dataset;
        if (type == 'thirty') {
            this.downLoadWorks('0');
            // const work_item = this.data.no_ownWorkLists.find(item => item.works_id == cur_worksId);
            // this.downloadFile('com', work_item);
        }
        else if (type == 'full') {
            this.payForDownload();
        }
    },
    // 关闭打赏弹窗
    closeModal() {
        cur_worksId = null;
        this.setData({
            down_visible: false,
            visible: false,
            delete_visible: false,
        });
    },
    // 跳转作品播放
    jumpVideoPlay(e) {
        const {
            id
        } = e.currentTarget.dataset;
        wx.navigateTo({
            url: `../../videoPlay/index?id=${id}`,
        });
    },
    Tap() {
        this.setData({
            isFocus: true
        });
    },
    updateShow(e) {
        const {
            type
        } = e.currentTarget.dataset;
        const {
            has_ownWorkLists,
            no_ownWorkLists
        } = this.data;
        this.setData({
            is_download: type
        }, () => {
            this.getOwnWorkLists();
        });
    },
    getOwnWorkLists(currentPage = 1, pageSize = 15) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${currentPage}|dbkey|${publicParams.dbkey}|is_download|${this.data.is_download}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            is_download: this.data.is_download,
            currentPage: currentPage,
            pageSize: pageSize,
            ...publicParams
        };
        $API.works.getPersonWorksList(params).then(res => {
            if (this.data.is_download == 0) {
                this.setData({
                    no_ownWorkLists: res.data.worksList
                });
            } else {
                this.setData({
                    has_ownWorkLists: res.data.worksList
                });
            }
        });
    },

    // 获取钱包信息
    getThisWalletInfo() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getWalletInfo(params).then(res => {
            this.setData({
                walletInfo: res.data
            });
        });
    },
    payForDownload() {
        // 先判断是否开通账户
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getAccountDetail(params).then(res => {
            if (res.data.is_set_pay_password) {
                this.getThisWalletInfo();
                // 创建余额转账订单
                this.getWorksInfoById();
            } else {
                // 未设置支付密码
                wx.showToast({
                    title: '未设置支付密码',
                    success() {
                        wx.navigateTo({
                            url: '../../myWallet/payPassword/index',
                        });
                    }
                })
            }
        });
    },
    getWorksRule() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.works.getWorksRule(params).then(res => {
            works_rule = res.data;
        });
    },
    getWorksInfoById() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${cur_worksId}`),
            token: token,
            works_id: cur_worksId,
            ...publicParams
        };
        $API.works.getWorksInfoById(params).then(res => {
            download_workInfo = res.data;
            if (download_workInfo.pay_state == 1) {
                const work_item = this.data.no_ownWorkLists.find(item => item.works_id == cur_worksId);
                this.downloadFile('com', work_item);
            }
            else {
                this.createOrder();
            }
        });
    },
    // 创建作品下载订单
    createOrder() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|11|token|${token}|work_id|${cur_worksId}`),
            token: token,
            work_id: cur_worksId,
            product_id: 11,
            ...publicParams
        };
        $API.order.worksDownloadOrder(params).then(res => {
            if (res.data.isok == 1) {
                this.setData({
                    order_id: res.data.order_id,
                    payAmount: res.data.fee,
                });
                this.setData({
                    visible: true,
                });
            }
        });
    },
    // 输入支付密码
    Focus(e) {
        const inputValue = e.detail.value,
            ilen = inputValue.length,
            token = wx.getStorageSync('sysToken');
        this.setData({
            Value: inputValue
        });
        if (ilen == 6) {
            const params = {
                sign: md5(`dbkey|${publicParams.dbkey}|pay_password|${inputValue}|privateKey|${publicParams.privateKey}|token|${token}`),
                token: token,
                pay_password: inputValue,
                ...publicParams
            };
            $API.pay.payPasswordVerify(params).then(res => {
                if (res.data.isok == 1) {
                    // 支付密码验证通过
                    this.balance_Payment();
                    this.setData({
                        visible: false,
                        Value: ''
                    });
                }
            });
        }
    },
    // 余额转账
    balance_Payment() {
        const { order_id, payAmount } = this.data, token = wx.getStorageSync('sysToken');

        const params = {
            sign: md5(`balance_cost|${payAmount}|dbkey|${publicParams.dbkey}|order_id|${order_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            order_id: order_id,
            balance_cost: payAmount,
            ...publicParams
        }
        $API.pay.balancePayment(params).then(res => {
            if (res.data.isok == 1) {
                this.downLoadWorks();
            }
        });
    },
    // 修改下载状态
    downLoadWorks(type = '1') {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|download_type|${type}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${cur_worksId}`),
            token: token,
            works_id: cur_worksId,
            download_type: type,
            ...publicParams
        }
        $API.works.downLoadWorks(params).then(res => {
            const work_item = this.data.no_ownWorkLists.find(item => item.works_id == cur_worksId);
            if (type == '1') {
                this.downloadFile('com', work_item);
            }
            else if (type == '0') {
                this.downloadFile('in_com', work_item);
            }
        });
    },
})