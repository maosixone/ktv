// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		personal_signature: '',
	},
	onLoad(option) {
		this.setData({
			personal_signature: option.personal_signature
		});
	},
	// 获取验证码
	formSubmit(e) {
		const results = e.detail.value,
			token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|personal_signature|${results.personal_signature}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			personal_signature: results.personal_signature,
			...publicParams,
		};
		$API.user.updPersonalSignature(params).then(res => {
			if (res.data.isok == 1) {
				// 更新签名成功
				wx.navigateBack();
			}
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
})