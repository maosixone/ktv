// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	imgHosts,
	publicParams,
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		toRightSrc: imgHosts + 'to_right.png',
		userBgSrc: imgHosts + 'user/background.png',
		image_visible: false,
		user_image: '',
	},
	onLoad(option) {
		this.setData({
			user_image: option.url
		});
	},
	cancelModal() {
		this.setData({
			image_visible: false,
		});
	},
	showModal() {
		this.setData({
			image_visible: true
		});
	},
	chooseImage(e) {
		const { type } = e.currentTarget.dataset, token = wx.getStorageSync('sysToken'), newUserInfo = Object.assign({}, this.data.userInfo), that = this;
		wx.chooseImage({
			count: 1,
			sizeType: ['original', 'compressed'],
			sourceType: [type],
			success: (res) => {
				const tempFilePaths = res.tempFilePaths;
				console.log(tempFilePaths);
				this.setData({
					image_visible: false,
				});
				wx.navigateTo({
					url: `../../cropImage/index?type=bg&url=${tempFilePaths}`,
					complete: (res) => { },
					events: {
						finished: (data) => {
							console.log(data);
							that.setData({ user_image: data.data });
						}
					},
				});
				return;
			},
			complete: (res) => { },
		})
	},
	formSubmit(e) {
		const results = e.detail.value,
			token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|nick_name|${this.data.new_nickname}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			nick_name: this.data.new_nickname,
			...publicParams,
		};
		$API.user.updateUserInfo(params).then(res => {
			if (res.data.isok == 1) {
				// 更新昵称成功
				wx.navigateBack();
			}
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
})