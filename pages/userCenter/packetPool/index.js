// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		packetPoolInfo: false, //  钱包信息
	},
	onLoad() {
		this.getPacketPoolDetail();
	},
	getPacketPoolDetail() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getPacketPoolDetail(params).then(res => {
			this.setData({
				packetPoolInfo: res.data
			});
		});
	},
})