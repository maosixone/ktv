// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		toRightSrc: imgHosts + 'to_right.png',
		userInfo: false,
	},
	onLoad() {
		const userInfo = wx.getStorageSync('user_info');
		console.log(userInfo);
		this.setData({
			userInfo: userInfo
		});
	},
	jumpPage(e) {
		const {
			type
		} = e.currentTarget.dataset;
		let link = null;
		if (type == 3) {
			link = '../loginPassword/index';
		}
		wx.navigateTo({
			url: link,
		});
	},
	getAccountDetail() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getAccountDetail(params).then(res => {
			this.setData({
				accountDetail: res.data
			});
		});
	}
});