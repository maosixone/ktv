// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    formatSeconds
} from '../../../utils/util.js';
import {
    API as $API
} from '../../../utils/request';
let payForModal,
    cur_page = 1,	// 	当前页
    cur_song_id,	//	点歌song_id
    cur_id;		//	dict_id  歌曲曲风id || 歌曲语言id

Page({
    data: {
        activeSrc: imgHosts + 'active.png',
        chatSrc: imgHosts + 'user/chat.png',
        searchSrc: imgHosts + 'search.png',
        noDataSrc: imgHosts + 'home/no_data.png',
        deleteSrc: imgHosts + 'orderSong/delete.png',
        singingSrc: imgHosts + 'orderSong/singing.png',
        live_songSrc: imgHosts + 'orderSong/live_song.png',
        un_live_songSrc: imgHosts + 'orderSong/un_live_song.png',
        record_songSrc: imgHosts + 'orderSong/record_song.png',
        un_record_songSrc: imgHosts + 'orderSong/un_record_song.png',
        showType: '1',
        remark_name: '',	// 搜索昵称
        followList: false, //  关注列表
        fansList: false, //	粉丝列表
        userInfo: {}, //
        visible: false, //
        isFocus: false, //
    },
    // 事件处理函数
    onLoad(options) {
        const { type, search } = options, userInfo = wx.getStorageSync('user_info');
        cur_page = 1;
        search == 'true' && this.setData({ isFocus: true });
        this.setData({ userInfo, showType: type });
        this.getContactList(cur_page);
    },
    updShowType(e) {
        const { type } = e.currentTarget.dataset;
        this.setData({ showType: type }, () => {
            this.getContactList(1);
        });
    },
    getMoreSongList() {
        // 获取下一页数据
        this.getContactList(cur_page);
    },
    chatToUser(e) {
        const { id, image, name } = e.currentTarget.dataset;
        wx.navigateTo({
            url: `../../chatMessage/chat/index?id=${id}&name=${name}&image=${image}`,
        });
    },
    inputChange(e) {
        const { value } = e.detail;
        this.setData({ remark_name: value }, () => {
            this.getContactList();
        });
    },
    on_searchList() {
        this.getContactList();
    },
    getContactList(page = 1, pageSize = 20) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const { showType, remark_name } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|remark_name|${remark_name}|token|${token}`),
            token: token,
            currentPage: page,
            pageSize: pageSize,
            remark_name: remark_name,
            ...publicParams
        };
        if (showType == '1') {
            $API.user.getFollowList(params).then(res => {
                const new_followList = page == 1 ? res.data : [...this.data.followList, ...res.data];
                this.setData({
                    refresher: false,
                    followList: new_followList || [],
                });
                setTimeout(function () {
                    wx.hideLoading();
                    cur_page++;
                }, 0);
            });
        }
        else if (showType == '2') {
            $API.user.getFansList(params).then(res => {
                const new_fansList = page == 1 ? res.data : [...this.data.fansList, ...res.data];
                this.setData({
                    refresher: false,
                    fansList: new_fansList || [],
                });
                setTimeout(function () {
                    wx.hideLoading();
                    cur_page++;
                }, 0);
            });
        }
    },
    downRefresh() {
        this.getContactList();
    },
})