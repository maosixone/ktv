// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
} from '../../../config';
import {
    API as $API
} from '../../../utils/request';

Page({
    data: {
        new_nickname: '',
    },
    onLoad(option) {
        this.setData({
            new_nickname: option.nick_name
        });
    },
    updNickname(e) {
        const { value } = e.detail;
        this.setData({
            new_nickname: value
        });
    },
    formSubmit(e) {
        const results = e.detail.value,
            token = wx.getStorageSync('sysToken');
        if (results.nick_name.trim() == '') {
            return wx.showToast({
                title: '昵称不能为空',
                icon: 'none',
            });
        }
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|nick_name|${this.data.new_nickname}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            nick_name: this.data.new_nickname,
            ...publicParams,
        };
        $API.user.updateUserInfo(params).then(res => {
            if (res.data.isok == 1) {
                // 更新昵称成功
                wx.navigateBack();
            }
        }).catch(err => {
            wx.showToast({
                title: err,
                icon: 'none',
            });
        });
    },
})