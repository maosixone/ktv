// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
const { Conversation } = require('../services');
import {
	publicParams,
	imgHosts
} from '../../config';
import {
	formatSeconds
} from '../../utils/util.js';
import {
	API as $API
} from '../../utils/request';
let cur_page = [1, 1], cur_id = null, delayTimer = null;

Page({
	data: {
		showType: 1,
		visible: false,
		userInfo: false,
		followInfo: false,
		personWorksLists: false,
		worksTotal: 0,
		worksStarLists: false,
		star_worksTotal: 0,
		setting_iconSrc: imgHosts + 'user/setting_icon.png',
		edit_infoSrc: imgHosts + 'user/edit_info.png',
		icon_manSrc: imgHosts + 'user/icon_man.png',
		icon_womanSrc: imgHosts + 'user/icon_woman.png',
		userBgSrc: imgHosts + 'user/background.png',
		more_musicSrc: imgHosts + 'more_music.png',
		more_starSrc: imgHosts + 'more_star.png',
		set_bar1Src: imgHosts + 'user/set_bar1.png',
		set_bar2Src: imgHosts + 'user/set_bar2.png',
		set_bar3Src: imgHosts + 'user/set_bar3.png',
		set_bar4Src: imgHosts + 'user/set_bar4.png',
		noDataSrc: imgHosts + 'home/no_data.png',
		refresher: false,	// 当前下拉刷新状态
		lef_animation: false,	//
	},
	// 事件处理函数
	onLoad() {
		cur_page = [1, 1];

	},
	onReady() {
		this.ani = wx.createAnimation({
			duration: 100,
		});
	},
	onShow() {
		if (typeof this.getTabBar === 'function' && this.getTabBar()) {
			this.getTabBar().setData({
				selected: 4,
				show: true
			});
		}
		const { showType } = this.data, that = this;
		// if (cur_id) {
		// 	this.getWorksInfoById(cur_id);
		// }
		this.getUserInfo();
		this.getFollowInfo();
		if (showType == 1) {
			this.getOwnWorkLists(1, cur_page[0] * 10);
			delayTimer = setTimeout(() => {
				that.getWorksStarList(1, cur_page[1] * 10);
			}, 10)
		}
		else if (showType == 2) {
			that.getWorksStarList(1, cur_page[1] * 10);
			delayTimer = setTimeout(() => {
				that.getOwnWorkLists(1, cur_page[0] * 10);
			}, 10)
		}

		Conversation.c_watch((list) => {
			this.getTotalUnreadCount();
		});
		this.getTotalUnreadCount();
	},
	onHide() {

		Conversation.c_watch((list) => {
			return false;
		});
	},
	getTotalUnreadCount() {
		Conversation.getTotalUnreadCount().then((count) => {
			if (count > 0) {
				wx.setTabBarBadge({
					index: 3,
					text: count,
				});
			}
			else {
				wx.removeTabBarBadge({ index: 3 });
			}
		});
	},
	getWorksInfoById(id) {
		const { showType, personWorksLists, worksStarLists } = this.data, token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${id}`),
			token: token,
			works_id: id,
			...publicParams
		};
		$API.works.getWorksInfoById(params).then(res => {
			if (showType == 1) {
				const newLists = [].concat(personWorksLists), index = personWorksLists.findIndex(item => item.works_id == id);
				newLists[index] = res.data;
				cur_id = null;
				this.setData({ personWorksLists: newLists });
			}
			else if (showType == 2) {
				this.getWorksStarList(1, cur_page[1] * 10);
			}
		});
	},
	updateShowType(e) {
		const { type } = e.currentTarget.dataset;
		this.setData({ showType: type });
	},
	showSetBar() {
		this.ani.right('0').step();
		this.setData({
			lef_animation: this.ani.export()
		});
	},
	jumpPage(e) {
		const { page } = e.currentTarget.dataset, than = this;
		let link = null;
		if (page == 0) {
			link = `./bgImage/index?url=${this.data.userInfo.background_picture}`;
		}
		else if (page == 1) {
			link = `./myVideo/index`;
		}
		else if (page == 2) {
			link = `../myWallet/index`;
		}
		else if (page == 3) {
			link = './packetPool/index';
		}
		else if (page == 4) {
			link = './settin/index';
		}
		else if (page == 5) {
			link = './contactList/index?type=1';
		}
		else if (page == 6) {
			link = './contactList/index?type=2';
		}

		wx.navigateTo({
			url: link,
			success() {
				than.ani && than.hideSetBar();
			}
		});
	},
	editUserInfo() {
		wx.navigateTo({
			url: './editInfo/index',
		});
	},
	// 跳转作品播放
	jumpVideoPlay(e) {
		const { id } = e.currentTarget.dataset;
		cur_id = id;
		wx.navigateTo({
			url: `../videoPlay/index?id=${id}`,
		});
	},
	closeModal() {
		this.setData({ visible: false });
	},
	showFansCount() {
		this.setData({ visible: true });
	},
	hideSetBar() {
		this.ani.right('-100%').step();
		this.setData({
			lef_animation: this.ani.export()
		});
	},
	getUserInfo() {
		const token = wx.getStorageSync('sysToken'), localUserInfo = wx.getStorageSync('user_info');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getUser(params).then(res => {
			const new_user_info = Object.assign({}, localUserInfo, res.data,);
			wx.setStorageSync('user_info', new_user_info)
			this.setData({
				userInfo: new_user_info
			});
		})
	},
	getFollowInfo() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getFollowInfo(params).then(res => {
			this.setData({
				followInfo: res.data
			});
		})
	},
	getMoreWorks() {
		if (this.data.showType == 1) {
			this.getOwnWorkLists(cur_page[0]);
		}
		else {
			this.getWorksStarList(cur_page[1]);
		}
	},
	getOwnWorkLists(currentPage = 1, pageSize = 10) {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`currentPage|${currentPage}|dbkey|${publicParams.dbkey}|is_download||pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			is_download: '',
			currentPage: currentPage,
			pageSize: pageSize,
			...publicParams
		};
		$API.works.getPersonWorksList(params).then(res => {
			for (let i = 0, len = res.data.worksList.length; i < len; i++) {
				res.data.worksList[i].format_expiration_time = formatSeconds(Math.abs(res.data.worksList[i].expiration_time));
			}
			const new_list = currentPage == 1 ? res.data.worksList : [...this.data.personWorksLists, ...res.data.worksList];
			this.setData({
				refresher: false,
				worksTotal: res.data.page.totalResultSize,
				personWorksLists: new_list,
			}, () => { cur_page[0]++; });
		});
	},
	getWorksStarList(currentPage = 1, pageSize = 10) {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`currentPage|${currentPage}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			currentPage: currentPage,
			pageSize: pageSize,
			...publicParams
		};
		$API.works.getWorksStarList(params).then(res => {
			const new_list = currentPage == 1 ? res.data.worksList : [...this.data.worksStarLists, ...res.data.worksList];
			this.setData({
				refresher: false,
				star_worksTotal: res.data.page.totalResultSize,
				worksStarLists: new_list,
			}, () => { cur_page[1]++; });
		});
	},
	downRefresh() {
		if (this.data.showType == 1) {
			this.getOwnWorkLists(cur_page[0]);
		}
		else {
			this.getWorksStarList(cur_page[1]);
		}
	},
	handleSignature() {
		wx.navigateTo({
			url: `./editSignature/index?personal_signature=${this.data.userInfo.personal_signature}`,
		});
	},
})