// index.js
// 获取应用实例
const app = getApp();
const { Status } = require('../../services');
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		toRightSrc: imgHosts + 'to_right.png',
		accountDetail: false,
		visible: false,
	},
	onLoad() {
		this.getAccountDetail();
	},
	jumpPage(e) {
		const {
			type
		} = e.currentTarget.dataset;
		let link = null;
		if (type == 1) {
			link = '../accountSafety/index';
		} else if (type == 2) {
			link = '../accountBind/index?type=wechat';
		} else if (type == 3) {
			link = '../accountBind/index?type=alipay';
		}
		wx.navigateTo({
			url: link,
		});
	},
	showModal() {
		this.setData({ visible: true });
	},
	closeModal() {
		this.setData({ visible: false });
	},
	logout() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		Status.disconnect();
		$API.user.logout(params).then(res => {
			if (res.data.isok == 1) {
				wx.clearStorageSync();
				wx.reLaunch({
					url: '../../login/index',
				});
			}
		});
	},
	getAccountDetail() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getAccountDetail(params).then(res => {
			this.setData({
				accountDetail: res.data
			});
		});
	}
})