// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		warningSrc: imgHosts + 'login_warning.png',
	},
	onLoad() {

	},
	// 获取验证码
	formSubmit(e) {
		const results = e.detail.value,
			userInfo = wx.getStorageSync('user_info');
		if (results.login_password.length < 6 || results.login_password.length > 15) {
			return wx.showToast({
				title: '新密码由6~15位字母或数字组成',
				icon: 'none',
			});
		}
		const params = {
			phone: userInfo.phone || userInfo.beautiful_name,
			sign: md5(`dbkey|${publicParams.dbkey}|phone|${userInfo.phone || userInfo.beautiful_name}|privateKey|${publicParams.privateKey}`),
			...publicParams,
		};
		$API.user.getVerifyCode(params).then(res => {
			if (res.data.isok == 1) {
				// 获取验证码成功
				wx.navigateTo({
					url: `../verifyCode/index?login_password=${results.login_password}`,
				});
			}
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
})