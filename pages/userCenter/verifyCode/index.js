// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    API as $API
} from '../../../utils/request';

Page({
    data: {
        loginMode: 1,
        showPassword: false,
        pay_password: false,
        login_password: false,
        loginPhone: '', //  要登录的手机号
        warningSrc: imgHosts + 'login_warning.png',
    },
    // 事件处理函数
    changeLoginMode(e) {
        var type = e.currentTarget.dataset.type;
        this.setData({
            loginMode: +type
        });
    },
    jumpLogin() {
        wx.navigateBack();
    },
    onLoad(option) {
        if (option.pay_password) {
            wx.setNavigationBarTitle({
                title: '设置支付密码'
            });
            this.setData({
                pay_password: option.pay_password
            });
        }
        else if (option.login_password) {
            wx.setNavigationBarTitle({
                title: '设置登录密码'
            });
            this.setData({
                login_password: option.login_password
            });
        }
    },
    // 设置支付密码
    formSubmit(e) {
        const results = e.detail.value,
            token = wx.getStorageSync('sysToken'),
            userInfo = wx.getStorageSync('user_info');

        const params = {
            phone: userInfo.phone || userInfo.beautiful_name,
            verify: results.verify,
            token: token,
            ...publicParams,
        };
        if (this.data.pay_password) {
            params.pay_password = this.data.pay_password;
            params.sign = md5(`dbkey|${publicParams.dbkey}|pay_password|${this.data.pay_password}|phone|${userInfo.phone || userInfo.beautiful_name}|privateKey|${publicParams.privateKey}|token|${token}|verify|${results.verify}`);
            $API.pay.setPayPassWord(params).then(res => {
                // 设置成功
                wx.showToast({
                    title: '设置成功',
                    success() {
                        wx.navigateBack({
                            delta: 2
                        });
                    }
                });
            }).catch(err => {
                wx.showToast({
                    title: err,
                    icon: 'none',
                });
            });
        }
        else if (this.data.login_password) {
            params.password = this.data.login_password;
            params.sign = md5(`dbkey|${publicParams.dbkey}|password|${this.data.login_password}|phone|${userInfo.phone || userInfo.beautiful_name}|privateKey|${publicParams.privateKey}|token|${token}|verify|${results.verify}`);
            $API.user.setPassWordAndVerify(params).then(res => {
                // 设置成功
                wx.showToast({
                    title: '设置成功',
                    success() {
                        wx.navigateBack({
                            delta: 2
                        });
                    }
                });
            }).catch(err => {
                wx.showToast({
                    title: err,
                    icon: 'none',
                });
            });
        }
    },
})