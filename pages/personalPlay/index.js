// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';;
const { Status, Message, Conversation, ChatRoom } = require('../services');
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    API as $API
} from '../../utils/request';
let cur_duration = 0,
    cur_id = '',
    cur_videoContext = null,
    allowPause = false,		//	等到数据加载后才允许暂停操作
    updateSlider = false,
    perSonal = null;

Page({
    data: {
        playWorksId: '',
        playUrl: '',	//	广告播放地址: 图片/视频
        rewardWorkInfo: false,
        sliderValue: 0,
        isPause: false,
        index_playSrc: imgHosts + 'index_play.png',
        musicSrc: imgHosts + 'home/green_music.png',
        fabulousSrc: imgHosts + 'home/fabulous.png',
        noFabulousSrc: imgHosts + 'home/no_fabulous.png',
        rewardSrc: imgHosts + 'home/reward.png',
        noDataSrc: imgHosts + 'home/no_data.png',
        userInfo: false,
        ad_type: false,		//	是否为广告, 0: 图片  1: 视频
    },
    // 事件处理函数
    onLoad(option) {
        let { id, url, ad_type, personal } = option;
        cur_id = id, perSonal = personal;
        if (ad_type && ad_type != 'undefined') {
            ad_type = ad_type;
        }
        else {
            ad_type = false;
            this.getWorksInfoById(cur_id);
        }
        this.setData({ userInfo: wx.getStorageSync('user_info'), playWorksId: id, ad_type, playUrl: url });
    },
    onUnload() {
        cur_id = '', cur_videoContext = null;
    },
    onShow() {
        updateSlider = true;
    },
    getWorksInfoById(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${id}`),
            token: token,
            works_id: id,
            ...publicParams
        };
        $API.works.getWorksInfoById(params).then(res => {
            wx.showLoading({
                title: '加载中',
                mask: true,
            });
            this.setData({
                rewardWorkInfo: res.data
            });
        });
    },
    // 关注/取关
    handleFollow(e) {
        const token = wx.getStorageSync('sysToken');
        const {
            type,
            id,
            worksid,
        } = e.currentTarget.dataset
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|follow_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            follow_id: id,
            ...publicParams
        };
        const rewardWorkInfo = this.data.rewardWorkInfo;
        if (type == 'add') {
            params.remark_name = '';
            params.sign = md5(`dbkey|${publicParams.dbkey}|follow_id|${id}|privateKey|${publicParams.privateKey}|remark_name||token|${token}`);
            $API.works.addFollow(params).then(res => {
                if (res.data.isok > 0) {
                    rewardWorkInfo.is_follow = 1;
                    this.setData({
                        rewardWorkInfo: rewardWorkInfo
                    });
                }
            });
        } else {
            $API.works.cancelFollow(params).then(res => {
                if (res.data.isok > 0) {
                    rewardWorkInfo.is_follow = 0;
                    this.setData({
                        rewardWorkInfo: rewardWorkInfo
                    });
                }
            });
        }
    },
    handleReward(e) {
        const {
            worksid
        } = e.currentTarget.dataset, { userInfo } = this.data, that = this;
        wx.navigateTo({
            url: `../reward/index?worksId=${worksid}`,
            events: {
                sendText: async function (data) {
                    const result = await that.getRongInfo(data.ktv_user_id);
                    const content = {
                        targetId: result.data.rong_id,
                        content: `${userInfo.nick_name}打赏${data.nick_name}${data.name},价值${data.value}火鸡币`,
                    };
                    Message.sendText(content);
                }
            }
        });
    },
    // 获取融云信息
    getRongInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|ktv_user_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ktv_user_id: id,
            ...publicParams
        };
        return new Promise((resolve, reject) => {
            $API.user.getRongInfo(params).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        });
    },
    // 点赞收藏
    handleStar(e) {
        const token = wx.getStorageSync('sysToken');
        const {
            type,
            version,
            worksid,
        } = e.currentTarget.dataset
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|is_star|${type}|privateKey|${publicParams.privateKey}|token|${token}|version|${version}|works_id|${worksid}`),
            token: token,
            version: version,
            works_id: worksid,
            is_star: type,
            ...publicParams
        };
        const rewardWorkInfo = this.data.rewardWorkInfo;
        $API.works.updateStar(params).then(res => {
            if (res.data.isok > 0) {
                rewardWorkInfo.is_star = type;
                rewardWorkInfo.fabulous_count = res.data.star_count;
                this.setData({
                    rewardWorkInfo: rewardWorkInfo
                });
            }
        });
    },
    // 跳转个人主页
    jumpUserWorks() {
        if (perSonal == 'own') { return; }
        const type = 'rec_works', rewardWorkInfo = this.data.rewardWorkInfo;
        wx.navigateTo({
            url: `../personalWorks/index?type=${type}&user_id=${rewardWorkInfo.ktv_user_id}&user_name=${rewardWorkInfo.nick_name}`,
        });
    },
    onloadedmetadata(e) {
        wx.hideLoading();
        !cur_videoContext && (cur_videoContext = wx.createVideoContext(`v-${cur_id}`));
        allowPause = true;
    },
    videoTap() {
        const { isPause, rewardWorkInfo } = this.data;
        if (!allowPause) { return; }
        !cur_videoContext && (cur_videoContext = wx.createVideoContext(`v-${rewardWorkInfo.works_id}`));
        if (isPause) {
            cur_videoContext.play();
        }
        else {
            cur_videoContext.pause();
        }
        this.setData({ isPause: !isPause });
    },
    videoTimeUpdate(e) {
        let { currentTime, duration } = e.detail, sliderValue = 0;
        if (updateSlider) {
            cur_duration = duration;
            sliderValue = currentTime / duration * 100;
            this.setData({ sliderValue: sliderValue });
        }
    },
    sliderChanging() {
        updateSlider = false;
    },
    sliderChange(e) {
        let sliderValue = 0, rewardWorkInfo = this.data.rewardWorkInfo;
        if (cur_duration > 0) {
            !cur_videoContext && (cur_videoContext = wx.createVideoContext(`v-${rewardWorkInfo.works_id}`));
            cur_videoContext.seek(e.detail.value / 100 * cur_duration);
            updateSlider = true;
            sliderValue = e.detail.value;
            this.setData({ sliderValue: sliderValue });
        }
    },
    statechange(e) {
        console.log('live-player code:', e.detail.code)
    },
    error(e) {
        console.error('live-player error:', e.detail.errMsg)
    },
})