// pages/login/component/customizeTitle.js
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabTitle: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    navHeight: app.globalData.statusBarHeight,
    statusBarRight: app.globalData.statusBarRight,
    isIos: app.systemInfo()
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
