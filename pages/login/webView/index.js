// index.js
// 获取应用实例
const app = getApp();

Page({
    data: {
        url: '',
    },
    onLoad(option) {
        const { value, name } = option;
        wx.setNavigationBarTitle({
            title: name
        });
        this.setData({ url: value });
    },
})