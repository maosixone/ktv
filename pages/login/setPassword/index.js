// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';
import {
	updUserInfo
} from '../../../utils/util';

// 微信登录未绑定时，需做判断并绑定
let wx_info = {}, userInfo = null;

Page({
	data: {
		loginMode: 1,
		showPassword: false,
		codeImage: '',
		hasSendCode: false, // 是否已经发送获取验证码
		countDown: 60, // 验证码倒计时
		loginPhone: '', //  要登录的手机号
	},
	onLoad(option) {
		userInfo = wx.getStorageSync('user_info');
		wx_info = {
			wx_bind: option.wx_bind,
		};
		this.setData({
			loginPhone: option.phone
		});
		if (option.direct == 'true') {
			this.getRongInfo();
		}
	},
	jumpLogin() {
		wx.navigateBack();
	},
	// 设置密码submit
	formSubmit(e) {
		const results = e.detail.value;
		// console.log(results)
		if (results.password.length === 0) {
			wx.showToast({
				title: '请输入新密码',
				icon: 'none',
				duration: 2000
			})
			return;
		}
		if (results.password.length < 6 || results.password.length > 15) {
			wx.showToast({
				title: '密码长度应为6-15位',
				icon: 'none',
				duration: 2000
			})
			return;
		}

		const token = wx.getStorageSync('sysToken');
		if (results.againPassword.length === 0) {
			wx.showToast({
				title: '请再次输入',
				icon: 'none',
				duration: 2000
			})
			return;
		}
		if (results.password !== results.againPassword) {
			wx.showToast({
				title: '两次输入密码不一致',
				icon: 'none',
			});
			return;
		}

		const params = {
			phone: this.data.loginPhone,
			password: results.password,
			token: token,
			confirm_password: results.againPassword,
			sign: md5(`confirm_password|${results.againPassword}|dbkey|${publicParams.dbkey}|password|${results.password}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}|token|${token}`),
			...publicParams,
		};
		$API.user.setPassWord(params).then(res => {
			// 密码设置成功，
			this.getRongInfo();
		}).catch(err => {
			wx.showToast({
				title: err,
				icon: 'none',
			});
		});
	},
	// 微信登录绑定用户
	getUserProfile() {
		wx.getUserProfile({
			desc: '用于完善用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
			success: (res) => {
				userInfo = {
					...userInfo,
					nick_name: res.userInfo.nickName,
					sex: res.userInfo.gender || 0,	//	获取用户信息不在返回 性别以及地区信息
					images: res.userInfo.avatarUrl,
				};
				this.bindingWeChat();
			}
		})
	},
	bindingWeChat() {
		const token = wx.getStorageSync('sysToken'), { ktv_user_id, nick_name, images, sex, unionid, openid } = userInfo,
			params = {
				sign: md5(`dbkey|${publicParams.dbkey}|images|${images}|ktv_user_id|${ktv_user_id}|nick_name|${nick_name}|openid|${openid}|privateKey|${publicParams.privateKey}|sex|${sex}|token|${token}|unionid|${unionid}`),
				ktv_user_id: ktv_user_id,
				nick_name: nick_name,
				sex: sex,
				images: images,
				openid: openid,
				unionid: unionid,
				token: token,
				...publicParams
			};
		$API.user.bindingWeChat(params).then(res => {
			userInfo = {
				...userInfo,
				...res.data
			};
			wx.setStorageSync('user_info', userInfo);

			wx.switchTab({
				url: '../../home/index',
			});
		}).catch(err => {

		});
	},
	// 获取融云信息
	getRongInfo() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getRongInfo(params).then(res => {
			updUserInfo(res.data);

			// 判断是否为微信登录未绑定
			if (wx_info.wx_bind == 'true') {
				this.getUserProfile();
			}
			else {
				wx.switchTab({
					url: '../../home/index',
				});
			}
		}).catch(err => {

		});
	}
})