// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
} from '../../../config';
import {
    API as $API
} from '../../../utils/request';
import { showMessage } from '../loginTip'

Page({
    data: {
        loginMode: 1,
        showPassword: false,
        codeImage: '',
        hasSendCode: false, // 是否已经发送获取验证码
        countDown: 60, // 验证码倒计时
        loginPhone: '', //  要登录的手机号
    },
    // 事件处理函数
    changeLoginMode(e) {
        var type = e.currentTarget.dataset.type;
        this.setData({
            loginMode: +type
        });
    },
    setPhone(e) {
        const {
            value
        } = e.detail;
        this.setData({
            loginPhone: value
        });
    },
    startTimer() {
        this.codeTimer = setInterval(() => {
            if (this.data.countDown > 0) {
                this.setData({
                    countDown: this.data.countDown - 1
                });
            } else {
                this.setData({
                    hasSendCode: false,
                    countDown: 60
                });
                clearInterval(this.codeTimer);
            }
        }, 1000);
    },
    getverifyCode() {
        let results = {
            phone: this.data.loginPhone
        }
        if (this.data.hasSendCode) {
            wx.showToast({
                title: '请勿重复发送',
                icon: 'none'
            });
            return
        }
        if (showMessage(results)) {
            return;
        }
        const params = {
            phone: this.data.loginPhone,
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}`),
            ...publicParams,
        };
        $API.user.getVerifyCode(params).then(res => {
            if (res.data && res.data.isok == 1) {
                wx.showToast({
                    title: '验证码发送成功',
                    icon: 'none',
                    duration: 2000
                });
                this.setData({
                    hasSendCode: true,
                }, () => {
                    this.startTimer();
                });
            }
        }).catch(err => {

        });
    },
    onLoad(option) {
        this.setData({
            loginPhone: option.phone
        });
    },
    // 密码登录submit
    formSubmitPassword(e) {
        const results = e.detail.value;
        if (showMessage(results)) {
            return;
        }
        if (results.password.length < 6 && results.length > 15) {
            wx.showToast({
                title: '密码长度应为6-15位',
                icon: 'none',
                duration: 2000
            })
            return;
        }
        const sign = md5(`dbkey|${publicParams.dbkey}|password|${results.password}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}`);

        const params = {
            phone: this.data.loginPhone,
            password: results.password,
            sign: sign,
            ...publicParams,
        };
        $API.user.login(params).then(res => {
            console.log(res);
        }).catch(err => {

        })
    },
    // 设置密码submit
    formSubmit(e) {
        let results = e.detail.value;
        if (showMessage(results)) {
            return;
        }
        if (results.password.length < 6 || results.password.length > 15) {
            wx.showToast({
                title: '密码长度应为6-15位',
                icon: 'none',
                duration: 2000
            })
            return;
        }
        console.log('提交', results);
        const params = {
            phone: this.data.loginPhone,
            verify: results.verify,
            password: results.password,
            sign: md5(`dbkey|${publicParams.dbkey}|password|${results.password}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}|verify|${results.verify}`),
            ...publicParams,
        };
        $API.user.forgetPassWord(params).then(res => {
            if (res.data.isok == 1) {
                // 密码设置成功，
                wx.showToast({
                    title: '密码设置成功',
                    icon: 'success',
                    success: () => {
                        wx.navigateBack();
                    },
                });
            }
        }).catch(err => {
            wx.showToast({
                title: err,
                icon: 'none',
            });
        });
    },
})