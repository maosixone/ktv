// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';;
import {
    publicParams,
    imgHosts,
    appInfo
} from '../../config';
import {
    API as $API
} from '../../utils/request';
import {
    updUserInfo
} from '../../utils/util';
import {
    showMessage
} from './loginTip';

Page({
    data: {
        loginMode: 1,
        showPassword: false,
        codeImage: '',
        hasSendCode: false, // 是否已经发送获取验证码
        countDown: 60, // 验证码倒计时
        loginPhone: '', //  要登录的手机号
        activeSrc: imgHosts + 'active.png', //
        wechatSrc: imgHosts + 'wechat.png',
        toRightSrc: imgHosts + 'to_right.png',
        showSrc: imgHosts + 'login_show.png',
        hideSrc: imgHosts + 'login_hide.png',
    },
    // 事件处理函数
    changeLoginMode(e) {
        var type = e.currentTarget.dataset.type;
        this.setData({
            loginMode: +type,
            loginPhone: ''
        });
    },
    jumpRegister() {
        wx.navigateTo({
            url: './register/index',
        })
    },
    toggleShowPassword() {
        this.setData({
            showPassword: !this.data.showPassword
        });
    },
    setPhone(e) {
        const {
            value
        } = e.detail;
        this.setData({
            loginPhone: value
        });
    },
    startTimer() {
        this.codeTimer = setInterval(() => {
            if (this.data.countDown > 0) {
                this.setData({
                    countDown: this.data.countDown - 1
                });
            } else {
                this.setData({
                    hasSendCode: false,
                    countDown: 60
                });
                clearInterval(this.codeTimer);
            }
        }, 1000);
    },
    jumpView(e) {
        const { type } = e.currentTarget.dataset;
        let url = null, name = null;
        if (type == 1) {
            // 注册
            name = "用户注册协议";
            url = 'https://admin.turkeyklive.com/registration.html';
        }
        else if (type == 2) {
            // 隐私
            name = "用户隐私政策";
            url = 'https://admin.turkeyklive.com/privacy.html';
        }
        wx.navigateTo({
            url: `./webView/index?value=${url}&name=${name}`
        });
    },
    getverifyCode() {
        let results = {
            phone: this.data.loginPhone
        }
        if (this.data.hasSendCode) {
            wx.showToast({
                title: '请勿重复发送',
                icon: 'none'
            });
            return
        }
        if (showMessage(results)) {
            return;
        }
        const params = {
            phone: this.data.loginPhone,
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}`),
            ...publicParams,
        };
        $API.user.getVerifyCode(params).then(res => {
            if (res.data && res.data.isok == 1) {
                wx.showToast({
                    title: '验证码发送成功',
                    icon: 'none',
                    duration: 2000
                });
                this.setData({
                    hasSendCode: true,
                }, () => {
                    this.startTimer();
                });
            }
        }).catch(err => {

        });
    },
    onLoad() {
        const token = wx.getStorageSync('sysToken');
        const userInfo = wx.getStorageSync('user_info');
        console.log(token, userInfo);
        if (token && userInfo) {
            wx.switchTab({
                url: '../home/index',
            })
        }
    },
    forgetPassword() {
        wx.navigateTo({
            url: './forgetPassword/index',
        });
    },
    // 密码登录submit
    formSubmitPassword(e) {
        const results = e.detail.value;
        if (showMessage(results)) {
            return
        }
        const sign = md5(`dbkey|${publicParams.dbkey}|password|${results.password}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}`);

        const params = {
            phone: this.data.loginPhone,
            password: results.password,
            sign: sign,
            ...publicParams,
        };
        $API.user.login(params).then(res => {
            const userInfo = {
                phone: this.data.loginPhone,
                ...res.data
            };
            wx.setStorageSync('user_info', userInfo)
            this.getRongInfo();
        }).catch(err => {

        });
    },
    // 验证码登录submit
    formSubmitCode(e) {
        const results = e.detail.value;
        if (showMessage(results)) {
            return
        }
        const sign = md5(`dbkey|${publicParams.dbkey}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}|verify|${results.verify}`);
        const params = {
            phone: this.data.loginPhone,
            verify: results.verify,
            sign: sign,
            ...publicParams,
        };
        $API.user.codeLogin(params).then(res => {
            if (res.data.isSetPwd) {
                wx.navigateTo({
                    url: `./setPassword/index?phone=${this.data.loginPhone}`,
                });
            } else {
                // 不需要设置密码
                const userInfo = {
                    phone: this.data.loginPhone,
                    ...res.data
                };
                wx.setStorageSync('user_info', userInfo);
                this.getRongInfo();
            }
        }).catch(err => {

        });
    },
    // 微信登录
    getWeLogin: function () {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        wx.login({
            success: (res) => {
                if (res.code) {
                    // this.getOpenId(res.code);
                    this.weChatLogin(res.code);
                }
            }
        })
    },
    // 获取融云信息
    getRongInfo() {
        const token = wx.getStorageSync('sysToken');
        wx.login({
            success: (res) => {
                if (res.code) {
                    this.getOpenId(res.code);
                }
            },
            complete: () => {
                const params = {
                    sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
                    token: token,
                    ...publicParams
                };
                $API.user.getRongInfo(params).then(res => {

                    updUserInfo(res.data);
                    wx.switchTab({
                        url: '../home/index',
                    });
                }).catch(err => {

                });
            }
        });
    },
    // 微信登录
    weChatLogin(code) {
        const params = {
            sign: md5(`code|${code}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|type|ALYAPP`),
            code: code,
            type: 'ALYAPP',
            ...publicParams
        };
        $API.user.weChatLogin(params).then(res => {
            setTimeout(function () {
                wx.hideLoading();
            }, 0);
            // 未绑定时跳转注册
            if (res.data.ktv_user_id == 0) {
                updUserInfo(res.data);
                wx.navigateTo({
                    url: `./register/index?wx_bind=true`,
                });
            }
            // 已绑定手机号
            else {
                updUserInfo(res.data);
                wx.switchTab({
                    url: '../home/index',
                });
            }
        }).catch(err => {

        });
    },
    // 获取openId
    getOpenId(code) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|grant_type|authorization_code|js_code|${code}|privateKey|${publicParams.privateKey}|token|${token}`),
            js_code: code,
            grant_type: 'authorization_code',
            token: token,
            ...publicParams
        };
        $API.user.getOpenId(params).then(res => {
            const data = typeof res.data === 'object' ? res.data : JSON.parse(res.data);
            updUserInfo(data);
        }).catch(err => {

        });
    }
})