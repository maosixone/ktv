const phoneCheck = (phone) => {
	let myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
	if (!myreg.test(phone)) {
		wx.showToast({
			title: '请输入正确的手机号',
			icon: 'none',
			duration: 2000
		});
	}
	return !myreg.test(phone)
}

const tipObj = {
	phone: '请输入手机号',
	password: '请输入密码',
	verify: '请输入验证码',
	newPassword: '请设置新密码'
}
function showMessage(checkObj) {
	if (checkObj.phone === '') {
		wx.showToast({
			title: tipObj.phone,
			icon: 'none',
			duration: 2000
		});
		return true;
	}
	if (phoneCheck(checkObj.phone)) {
		return true;
	}
	if (checkObj.verify === '') {
		wx.showToast({
			title: tipObj.verify,
			icon: 'none',
			duration: 2000
		});
		return true;
	}
	if (checkObj.password === '') {
		wx.showToast({
			title: tipObj.password,
			icon: 'none',
			duration: 2000
		});
		return true;
	}
	if (checkObj.newPassword === '') {
		wx.showToast({
			title: tipObj.newPassword,
			icon: 'none',
			duration: 2000
		});
		return true;
	}
}
module.exports = {
	showMessage
} 