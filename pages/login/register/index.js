// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    API as $API
} from '../../../utils/request';
import {
    showMessage
} from '../loginTip';

// 微信登录未绑定时，需传递
let wx_info = {}, userInfo = null;

Page({
    data: {
        loginMode: 1,
        showPassword: false,
        codeImage: '',
        hasSendCode: false, // 是否已经发送获取验证码
        countDown: 60, // 验证码倒计时
        loginPhone: '', //  要登录的手机号
        toRightSrc: imgHosts + 'to_right.png',
    },
    onLoad(option) {
        const { wx_bind } = option;
        userInfo = wx.getStorageSync('user_info');
        wx_info = {
            wx_bind: wx_bind,
        };
    },
    jumpLogin() {
        wx.navigateBack();
    },
    setPhone(e) {
        const {
            value
        } = e.detail;
        this.setData({
            loginPhone: value
        });
    },
    startTimer() {
        this.codeTimer = setInterval(() => {
            if (this.data.countDown > 0) {
                this.setData({
                    countDown: this.data.countDown - 1
                });
            } else {
                this.setData({
                    hasSendCode: false,
                    countDown: 60
                });
                clearInterval(this.codeTimer);
            }
        }, 1000);
    },
    getverifyCode() {
        let results = {
            phone: this.data.loginPhone
        }
        if (this.data.hasSendCode) {
            wx.showToast({
                title: '请勿重复发送',
                icon: 'none'
            });
            return
        }
        if (showMessage(results)) {
            return;
        }
        const params = {
            phone: this.data.loginPhone,
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}`),
            ...publicParams,
        };
        $API.user.getVerifyCode(params).then(res => {
            console.log(this.data.hasSendCode);
            if (res.data && res.data.isok == 1) {
                wx.showToast({
                    title: '验证码发送成功',
                    icon: 'none',
                    duration: 2000
                });
                this.setData({
                    hasSendCode: true,
                }, () => {
                    this.startTimer();
                });
            }
        }).catch(err => {

        });
    },
    getUserIsExist() {
        // const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}`),
            // token: token,
            phone: this.data.loginPhone,
            ...publicParams
        };
        $API.user.getUserIsExist(params).then(res => {
            if (!res.data || wx_info.wx_bind != 'true') {
                return this.getverifyCode();
            }
            const { ktv_user_id, openid, unionid } = res.data;
            // 已注册, 可以绑定
            if (ktv_user_id != 0 && !unionid) {
                userInfo = {
                    ...userInfo,
                    ktv_user_id: ktv_user_id,
                };
                this.getUserProfile();
            }
            else if (ktv_user_id != 0 && unionid) {
                wx.showToast({
                    title: '手机号已绑定微信',
                    icon: 'none',
                    duration: 2000
                });
            }
            // 未注册  或 不可以绑定
            else {
                this.getverifyCode();
            }
        });
    },
    // 微信登录绑定用户
    getUserProfile() {
        wx.getUserProfile({
            desc: '用于完善用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                userInfo = {
                    ...userInfo,
                    nick_name: res.userInfo.nickName,
                    sex: res.userInfo.gender || 0,	//	获取用户信息不在返回 性别以及地区信息
                    images: res.userInfo.avatarUrl,
                };
                this.bindingWeChat();
            }
        })
    },
    jumpView(e) {
        const { type } = e.currentTarget.dataset;
        let url = null, name = null;
        if (type == 1) {
            // 注册
            name = "用户注册协议";
            url = 'https://admin.turkeyklive.com/registration.html';
        }
        else if (type == 2) {
            // 隐私
            name = "用户隐私政策";
            url = 'https://admin.turkeyklive.com/privacy.html';
        }
        wx.navigateTo({
            url: `../webView/index?value=${url}&name=${name}`
        });
    },
    bindingWeChat() {
        const token = wx.getStorageSync('sysToken'), { ktv_user_id, nick_name, images, sex, unionid, openid } = userInfo,
            params = {
                sign: md5(`dbkey|${publicParams.dbkey}|images|${images}|ktv_user_id|${ktv_user_id}|nick_name|${nick_name}|openid|${openid}|privateKey|${publicParams.privateKey}|sex|${sex}|token|${token}|unionid|${unionid}`),
                ktv_user_id: ktv_user_id,
                nick_name: nick_name,
                sex: sex,
                images: images,
                openid: openid,
                unionid: unionid,
                token: token,
                ...publicParams
            };
        $API.user.bindingWeChat(params).then(res => {
            userInfo = {
                ...userInfo,
                ...res.data
            };
            wx.setStorageSync('user_info', userInfo);

            wx.switchTab({
                url: '../../home/index',
            });
        }).catch(err => {

        });
    },
    // 验证码注册submit
    formSubmitCode(e) {
        const results = e.detail.value, { wx_bind } = wx_info;
        if (showMessage(results)) {
            return;
        }
        const params = {
            phone: this.data.loginPhone,
            verify: results.verify,
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${this.data.loginPhone}|privateKey|${publicParams.privateKey}|verify|${results.verify}`),
            ...publicParams,
        };
        $API.user.register(params).then(res => {
            if (res.data.isSetPwd) {
                let userInfo = wx.getStorageSync('user_info');
                userInfo = {
                    ...userInfo,
                    ...res.data
                };
                wx.setStorageSync('user_info', userInfo);
                wx.navigateTo({
                    url: `../setPassword/index?phone=${this.data.loginPhone}&wx_bind=${wx_bind}`,
                });
            } else {
                // 不弹出设置密码页面
                wx.switchTab({
                    url: '../home/index',
                });
            }
        }).catch(err => {

        });
    },
})