// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

let change_song = false;

Page({
	data: {
		searchSrc: imgHosts + 'search.png',
		mtvSrc: imgHosts + 'orderSong/mtv.png',
		liveSrc: imgHosts + 'orderSong/live.png',
		titleText: '',
	},
	onLoad(option) {
		const { change } = option;
		change_song = change;
	},
	gotoSearch() {
		wx.navigateTo({
			url: `../../searchSongList/index?type=1&change_song=${change_song}`,
		});
	},
	jumpPage(e) {
		const { type } = e.currentTarget.dataset;
		wx.navigateTo({
			url: `../classifyList/index?type=${type}&change_song=${change_song}`,
		});
	}
})