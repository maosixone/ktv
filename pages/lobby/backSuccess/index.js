// index.js
// 获取应用实例
const app = getApp();
import { imgHosts } from '../../../config';

Page({
	data: {
		back_songSuccessSrc: imgHosts + 'back_songSuccess.png',
		rewardBgSrc: imgHosts + 'reward/reward_bg.png',
		song_name: '',
		back_fee: 0,
		balance_fee: 0,
		userInfo: false,
		booth: false,
	},
	onLoad(option) {
		const { song_name, back_fee, balance_fee } = option, userInfo = wx.getStorageSync('user_info'), booth = wx.getStorageSync('cur_booth');
		this.setData({ song_name, back_fee, balance_fee, userInfo, booth });
	},
	continue() {
		wx.navigateBack();
	},
})
