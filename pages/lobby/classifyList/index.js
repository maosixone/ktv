// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

let new_change_song = false;

Page({
	data: {
		searchSrc: imgHosts + 'search.png',
		mtvSrc: imgHosts + 'orderSong/mtv.png',
		liveSrc: imgHosts + 'orderSong/live.png',
		classifyType: '',
		classifyList: [],
	},
	onLoad(option) {
		const {
			type,
			change_song
		} = option;
		new_change_song = change_song;
		this.setData({ classifyType: type });
		this.getHallSongClassifyList(type);
	},
	gotoSearch() {
		wx.navigateTo({
			url: `../../searchSongList/index?type=1&change_song=${new_change_song}`,
		});
	},
	getHallSongClassifyList(type) {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|song_type|${type}|token|${token}`),
			song_type: type,
			token: token,
			...publicParams
		};
		$API.lobby.getHallSongClassifyList(params).then(res => {
			this.setData({ classifyList: res.data });
		});
	},
	jumpPage(e) {
		const { type, id, name } = e.currentTarget.dataset;
		wx.navigateTo({
			url: `../songList/index?type=${type}&id=${id}&name=${name}&change_song=${new_change_song}`,
		});
	}
})