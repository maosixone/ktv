// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    formatSeconds
} from '../../../utils/util.js';
import {
    API as $API
} from '../../../utils/request';
let payForModal,
    cur_page,	// 	当前页
    cur_type,	//	song_type
    cur_id,		//	classify_id
    cur_song_id;	//	song_id

Page({
    data: {
        searchSrc: imgHosts + 'search.png',
        firstSrc: imgHosts + 'orderSong/first.png',
        secondSrc: imgHosts + 'orderSong/second.png',
        thirdSrc: imgHosts + 'orderSong/third.png',
        change_musicSrc: imgHosts + 'change_music.png',
        change_songSrc: imgHosts + 'orderSong/change_song.png',
        classifyType: '',
        songList: false, //  节目单列表
        userInfo: {}, //
        visible: false, //
        classify_name: '',
        order_data: false, //
        change_song: false, //	当前行为	换歌：true	点歌：false
        change_visible: false,
        old_song: false, //	换歌	原歌信息
        new_song: false, //	换歌	新歌信息
    },
    // 事件处理函数
    onLoad(options) {
        payForModal = this.selectComponent('#payForModal');
        const userInfo = wx.getStorageSync('user_info'), old_song = wx.getStorageSync('change_song');
        const { type, id, name, change_song } = options;
        cur_type = type, cur_id = id, cur_page = 1;
        if (change_song && change_song != 'undefined') {
            wx.setNavigationBarTitle({
                title: '换歌'
            });
            this.setData({ change_song, old_song });
        }
        this.setData({ userInfo, classifyType: type, classify_name: name });
        this.getSongListByClassifyId(type, id);
    },
    gotoSearch() {
        const { change_song } = this.data;
        wx.navigateTo({
            url: `../../searchSongList/index?type=1&change_song=${change_song}`,
        });
    },
    getMoreSongList() {
        // 获取下一页数据
        this.getSongListByClassifyId(cur_type, cur_id, cur_page);
    },
    getSongListByClassifyId(type, id, page = 1, pageSize = 15) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`classify_id|${id}|currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|song_type|${type}|token|${token}`),
            token: token,
            song_type: type,
            classify_id: id,
            pageSize: pageSize,
            currentPage: page,
            ...publicParams
        };
        $API.lobby.getSongListByClassifyId(params).then(res => {
            if (res.data && res.data.length < 1) {
                return wx.showToast({
                    title: '已是最后一页',
                    icon: 'none'
                });
            }
            setTimeout(function () {
                wx.hideLoading();
                cur_page++;
            }, 0);
            const new_songList = page == 1 ? res.data : [...this.data.songList, ...res.data];
            this.setData({
                refresher: false,
                songList: new_songList
            });
        });
    },
    downRefresh() {
        this.getSongListByClassifyId(cur_type, cur_id);
    },
    closeModal() {
        this.setData({
            visible: false,
            change_visible: false,
        });
    },
    jumpPage(e) {
        const { type } = e.currentTarget.dataset;
        let link = null;
        if (type == 1) {
            link = '/pages/programList/index';
            wx.switchTab({
                url: '/pages/lobby/index',
                success() {
                    wx.navigateTo({
                        url: link,
                    });
                }
            });
        }
        this.setData({ visible: false });
    },
    changeThis(e) {
        const { id } = e.currentTarget.dataset;
        const song_data = this.data.songList.find(item => (item.km_songid || item.song_id) == id);

        this.setData({ new_song: song_data, change_visible: true });
    },
    handleChangeSong(e) {
        const { type } = e.currentTarget.dataset;
        if (type == '') {
            return this.setData({ change_visible: false });
        }
        const { old_song, new_song } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|hall_program_info_id|${old_song.hall_program_info_id}|new_song_id|${new_song.song_id}|privateKey|${publicParams.privateKey}|song_id|${old_song.song_id}|token|${token}`),
            token: token,
            song_id: old_song.song_id,
            new_song_id: new_song.song_id,
            hall_program_info_id: old_song.hall_program_info_id,
            ...publicParams
        }
        $API.lobby.changeHallProgram(params).then(res => {
            if (res.data.isok == 1) {
                // 换歌成功
                wx.showToast({
                    title: '换歌成功',
                    icon: 'success',
                    duration: 2000
                });
                wx.navigateBack({ delta: 3 });
            }
        });
    },
    orderThis(e) {
        const { id, type } = e.currentTarget.dataset;
        const song_data = this.data.songList.find(item => (item.km_songid || item.song_id) == id);
        setTimeout(function () {
            wx.showLoading({
                title: '加载中',
            });
        }, 0);
        payForModal.checkAccount(() => {
            const token = wx.getStorageSync('sysToken');
            const params = {
                sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|1|song_id|${id}|song_type|${type}|token|${token}`),
                token: token,
                product_id: '1',
                song_id: id,
                song_type: type,
                ...publicParams
            };
            $API.order.pickSongOrder(params).then(res => {
                setTimeout(function () {
                    wx.hideLoading()
                }, 0)
                payForModal.showModal(res.data.song_cost, res.data.balance_fee);
                this.setData({
                    new_song: song_data,
                    order_data: res.data,
                });
            });
        });
    },
    // 余额转账
    balance_Payment() {
        const { order_data } = this.data, token = wx.getStorageSync('sysToken');

        const params = {
            sign: md5(`balance_cost|${order_data.song_cost}|dbkey|${publicParams.dbkey}|order_id|${order_data.order_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            order_id: order_data.order_id,
            balance_cost: order_data.song_cost,
            ...publicParams
        }
        $API.pay.balancePayment(params).then(res => {
            if (res.data.isok == 1) {
                // 余额支付
                this.add_HallProgram();
            }
        });
    },
    add_HallProgram() {
        const { order_data, new_song } = this.data, booth = wx.getStorageSync('cur_booth'), token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`booth_id|${booth.booth_id}|dbkey|${publicParams.dbkey}|order_id|${order_data.order_id}|privateKey|${publicParams.privateKey}|song_id|${new_song.song_id}|song_name|${new_song.song_name}|token|${token}`),
            token: token,
            order_id: order_data.order_id,
            song_id: new_song.song_id,
            song_name: new_song.song_name,
            booth_id: booth.booth_id,
            ...publicParams
        }
        $API.lobby.addHallProgram(params).then(res => {
            // 点歌成功
            payForModal._closeModal();
            this.setData({ visible: true });
        });
    }
})