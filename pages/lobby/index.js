// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
const { Conversation, Message, ChatRoom } = require('../services');
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    formatSeconds
} from '../../utils/util.js';
import {
    API as $API
} from '../../utils/request';

let stream_name = 'hanhall', userInfo = null;

Page({
    data: {
        recommendLists: false, //  推荐列表
        closeSrc: imgHosts + 'modal_close.png',
        sweepSrc: imgHosts + 'sweep.png',
        packet_pondSrc: '../../static/images/packet_pond.png',
        programmeSrc: imgHosts + 'programme.png',
        order_songSrc: imgHosts + 'order_song.png',
        interlude_songSrc: imgHosts + 'interlude_song.png',
        change_songSrc: imgHosts + 'change_song.png',
        back_songSrc: imgHosts + 'back_song.png',
        rewardSrc: imgHosts + 'ktvRoom/present.png',
        more_musicSrc: imgHosts + 'more_music.png',
        more_starSrc: imgHosts + 'more_star.png',
        live_endSrc: imgHosts + 'home/live_end.jpg',
        refresher: false,	// 当前下拉刷新状态
        refresherText: '下拉刷新',
        booth: false,	//	卡座信息
        sao_visible: false,	//	未绑定卡座时
        now_programInfo: false,	//	当前播放节目信息
    },
    onLoad() {
        this.setData({ booth: wx.getStorageSync('cur_booth') });
        userInfo = wx.getStorageSync('user_info');
    },
    // 事件处理函数
    onShow() {
        if (typeof this.getTabBar === 'function' && this.getTabBar()) {
            this.getTabBar().setData({
                selected: 1,
                show: true
            });
        }
        Conversation.c_watch((list) => {
            this.getTotalUnreadCount();
        });
        this.getRecommends();
        this.getTotalUnreadCount();
    },
    onHide() {
        Conversation.c_watch((list) => {
            return false;
        });
    },
    getTotalUnreadCount() {
        Conversation.getTotalUnreadCount().then((count) => {
            if (count > 0) {
                wx.setTabBarBadge({
                    index: 3,
                    text: count,
                });
            }
            else {
                wx.removeTabBarBadge({ index: 3 });
            }
        });
    },
    getRecommends() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.lobby.recommend(params).then(res => {
            for (let i = 0, len = res.data.length; i < len; i++) {
                res.data[i].format_expiration_time = formatSeconds(Math.abs(res.data[i].expiration_time));
            }
            this.setData({
                refresher: false,
                recommendLists: res.data
            });
        });
    },
    nowHallProgramInfo() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        }, that = this;
        $API.lobby.nowHallProgramInfo(params).then(res => {
            if (res.data.isok != 1) {
                return wx.showToast({
                    title: '暂无播放节目',
                    icon: 'none',
                    duration: 2000
                });
            }
            wx.navigateTo({
                url: `../reward/index?lobby=true&name=${res.data.nick_name}&live_rm_id=${stream_name}&images=${res.data.images}&in_live=false`,
                events: {
                    sendGift: function (data) {
                        const content = {
                            id: userInfo.rong_id,
                            user: {
                                id: userInfo.rong_id,
                                name: userInfo.nick_name,
                                portraitUri: userInfo.images,
                            }
                        };
                        ChatRoom.join(stream_name, content).then(() => {
                            console.log('加入聊天室成功');
                            const content = {
                                id: data.data,
                                number: 1,
                                total: 0,
                                user: {
                                    id: userInfo.rong_id,
                                    name: userInfo.nick_name,
                                    portraitUri: userInfo.images,
                                }
                            };
                            // 发送礼物
                            return ChatRoom.sendGift({
                                targetId: stream_name,
                                content,
                            });
                        });
                    },
                    sendText: async function (data) {
                        const result = await that.getRongInfo(res.data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
            this.setData({
                now_programInfo: res.data
            });
        });
    },
    // 获取融云信息
    getRongInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|ktv_user_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ktv_user_id: id,
            ...publicParams
        };
        return new Promise((resolve, reject) => {
            $API.user.getRongInfo(params).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        });
    },
    refresherPulling(e) {
        const { dy } = e.detail;
        if (dy > 35) {
            this.setData({
                refresherText: '释放立即刷新'
            });
        }
    },
    downRefresh() {
        this.getRecommends();
        this.setData({ refresherText: '下拉刷新' });
    },
    closeModal() {
        this.setData({ sao_visible: false });
    },
    jumpVideoPlay(e) {
        const { id } = e.currentTarget.dataset, { recommendLists } = this.data;
        const cur_recommend = recommendLists.find(item => item.id == id);
        if (cur_recommend.recommend_type == 3) {
            wx.navigateTo({
                url: `../videoPlay/index?id=${id}&ad_type=${cur_recommend.ad_type}&url=${cur_recommend.play_url}`,
            });
        } else if (cur_recommend.recommend_type == 2) {
            wx.navigateTo({
                url: `../livePlay/index?id=${id}&broadcast_id=${id}`,
            });
        }
        else {
            wx.navigateTo({
                url: `../videoPlay/index?id=${id}`,
            });
        }
    },
    jumpPage(e) {
        const { type } = e.currentTarget.dataset, { booth } = this.data;
        if (!booth && type != 5 && type != 6) {
            return this.setData({ sao_visible: true });
        }
        let link = null;
        if (type == 1) {
            link = '../programList/index';
        }
        else if (type == 2) {
            link = './orderSong/index';
        }
        else if (type == 3) {
            link = '../programList/index?type=insert';
        }
        else if (type == 4) {
            link = '../programList/index?type=change';
        }
        else if (type == 5) {
            return;
            link = '../userCenter/packetPool/index';
        }
        else if (type == 6) {
            return this.nowHallProgramInfo();
        }
        wx.navigateTo({
            url: link,
        });
    },
    // 对人打赏
    getUserInfoByPhone(value) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|phone|${value}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            phone: value,
            ...publicParams
        };
        $API.user.getUserInfoByPhone(params).then(res => {
            wx.navigateTo({
                url: `../reward/index?user_id=${res.data.ktv_user_id}&name=${res.data.nick_name}&images=${res.data.images}`,
                events: {
                    sendText: function (data) {
                        const content = {
                            targetId: res.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    queryQRCode(value) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`booth_tag_id|${value}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            booth_tag_id: value,
            ...publicParams
        };
        $API.lobby.bindingBooth(params).then(res => {
            if (res.data.isok == 1) {
                wx.showToast({
                    title: '绑定成功',
                    icon: 'none',
                    duration: 1500,
                    mask: true
                });
                this.setData({ booth: res.data.booth });
                wx.setStorageSync('cur_booth', res.data.booth);
            }
        }).catch(error => {
            wx.showToast({
                title: '大厅码错误',
                icon: 'none',
                duration: 1500,
                mask: true
            });
        });
    },
    scanCode() {
        wx.scanCode({
            success: (res) => {
                console.log('success', res);
                // typeTurkey  1加好友  2大厅  3是红包 	4是打赏
                try {
                    const result = JSON.parse(res.result);
                    if (result && result.typeTurkey == 2) {
                        this.queryQRCode(result.value);
                    }
                    else if (result && result.typeTurkey == 4) {
                        this.getUserInfoByPhone(result.value);
                    }
                    else if (result && result.typeTurkey == 3) {
                        wx.navigateTo({
                            url: `./packet/index?code=${result.value}`
                        });
                    }
                    else if (result && result.code == '00002') {
                        this.nowHallProgramInfo();
                    }
                    this.closeModal();
                }
                catch (e) {
                    let title = '无效二维码'
                    if (res.result.includes("barcode")) {
                        title = '请使用包厢扫一扫'
                    }
                    wx.showToast({
                        title: title,
                        icon: 'none',
                        duration: 1500,
                        mask: true
                    });
                }
            },
            fail: (res) => {
                console.log('fail', res);
                wx.showToast({
                    title: '扫码失败',
                    icon: 'none',
                    duration: 1500,
                    mask: true
                });
            },
        });
    }
})