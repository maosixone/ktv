// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import { publicParams, imgHosts } from '../../../config';
import {
    API as $API
} from '../../../utils/request';

Page({
    data: {
        packet_bgSrc: '../../../static/images/packet_bg.png',
        fee_bgSrc: '../../../static/images/fee_bg.png',
        money: 0,
        packetDetail: [],
        packetInfo: {},
    },
    onLoad(option) {
        const { code, } = option, userInfo = wx.getStorageSync('user_info');
        this.grabPacket(code);
    },
    grabPacket(value) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|packet_barcode|${value}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            packet_barcode: value,
            ...publicParams
        };
        $API.lobby.grabPacket(params).then(res => {
            wx.setNavigationBarTitle({ title: res.data.packetInfo.title });
            this.setData({
                money: res.data.money,
                packetInfo: res.data.packetInfo,
                packetDetail: res.data.packetDetail,
            })
        });
    },
})
