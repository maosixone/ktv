// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    API as $API
} from '../../utils/request';
let cur_type = '', cur_page = 1, cur_id = '', cur_user = {};

Page({
    data: {
        showType: 1,
        preRecommendLists: false, //  上一页推荐列表
        recommendLists: false, //  推荐列表
        afterRecommendLists: false, //  下一页推荐列表
        ownWorkLists: false, //  作品列表
        liveLists: false, //  直播列表
        refresher: false, //
        currentPage: 1, //  当前页码
        recommendCurrent: 0,
        playWorksId: '',
        playOwnWorksId: '',
        playerLiveId: '',
        more_musicSrc: imgHosts + 'more_music.png',
        more_starSrc: imgHosts + 'more_star.png',
        live_endSrc: imgHosts + 'home/live_end.jpg',
        noDataSrc: imgHosts + 'home/no_data.png',
    },
    // 事件处理函数
    onLoad(option) {
        const {
            type,
            user_id,
            user_name,
        } = option;
        cur_type = type, cur_page = 1;
        if (user_id && user_name !== 'undefined') {
            cur_user = {
                id: user_id,
                name: user_name,
            };
        }
        else {
            cur_user = {};
        }
        this.setData({ showType: type == 'rec_works' ? 1 : type == 'own_works' ? 2 : 3 });
        if (type == 'rec_works') {
            this.getRecommendLists(cur_page);
        }
        else if (type == 'own_works') {
            this.getOwnWorkLists(cur_page);
        }
        else if (type == 'lives') {
            this.getLiveLists(cur_page);
        }
    },
    onShow() {
        if (!cur_user.id && cur_id && this.showType != 3) {
            this.getWorksInfoById(cur_id);
        }
    },
    onReady() {
        const name = cur_type == 'rec_works' ? '推荐' : cur_type == 'own_works' ? '作品' : '直播';
        wx.setNavigationBarTitle({
            title: cur_user.name || name
        });
    },
    downRefresh() {
        const { showType } = this.data;
        if (showType == 1) {
            this.getRecommendLists();
        }
        else if (showType == 2) {
            this.getOwnWorkLists();
        }
        else if (showType == 3) {
            this.getLiveLists();
        }
    },
    getWorksInfoById(id) {
        const { showType, recommendLists, ownWorkLists } = this.data, token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}|works_id|${id}`),
            token: token,
            works_id: id,
            ...publicParams
        };
        $API.works.getWorksInfoById(params).then(res => {
            if (showType == 1) {
                const newLists = [].concat(recommendLists), index = recommendLists.findIndex(item => item.works_id == id);
                newLists[index] = res.data;
                cur_id = null;
                this.setData({ recommendLists: newLists });
            }
            else if (showType == 2) {
                const newLists = [].concat(ownWorkLists), index = ownWorkLists.findIndex(item => item.works_id == id);
                newLists[index] = res.data;
                cur_id = null;
                this.setData({ ownWorkLists: newLists });
            }
        });
    },
    getMoreWorksList() {
        const { showType } = this.data;
        if (showType == 1) {
            this.getRecommendLists(cur_page);
        }
        else if (showType == 2) {
            this.getOwnWorkLists(cur_page);
        }
        else if (showType == 3) {
            this.getLiveLists(cur_page);
        }
    },
    // 跳转作品播放
    jumpVideoPlay(e) {
        const { id, broadcast_id, live_name, stream_name } = e.currentTarget.dataset, { showType } = this.data;
        cur_id = id;
        if (showType == 3) {
            wx.navigateTo({
                url: `../livePlay/index?broadcast_id=${broadcast_id}`,
            });
        }
        else {
            if (cur_user.id || showType == 2) {
                wx.navigateTo({
                    url: `../videoPlay/index?id=${id}&personal=own`,
                });
            }
            else {
                wx.navigateTo({
                    url: `../videoPlay/index?id=${id}`,
                });
            }
        }
    },
    getRecommendLists(page = 1, pageSize = 15) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}|user_id|${cur_user.id || ''}`),
            token: token,
            currentPage: page,
            pageSize: pageSize,
            user_id: cur_user.id || '',
            ...publicParams
        };
        $API.works.getWorksListPage(params).then(res => {
            if (page > res.data.page.totalPageSize) {
                return wx.showToast({
                    title: '已是最后一页',
                    icon: 'none'
                });
            }
            const new_list = page == 1 ? res.data.worksList : [...this.data.recommendLists, ...res.data.worksList];
            this.setData({
                refresher: false,
                recommendLists: new_list
            });
            setTimeout(function () {
                wx.hideLoading();
                cur_page++;
            }, 0);
        })
    },
    getOwnWorkLists(page = 1, pageSize = 10) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|is_download||pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            is_download: '',
            currentPage: page,
            pageSize: pageSize,
            ...publicParams
        };
        $API.works.getPersonWorksList(params).then(res => {
            if (page > res.data.page.totalPageSize) {
                return wx.showToast({
                    title: '已是最后一页',
                    icon: 'none'
                });
            }
            const new_list = page == 1 ? res.data.worksList : [...this.data.ownWorkLists, ...res.data.worksList];
            this.setData({
                refresher: false,
                ownWorkLists: new_list
            });
            setTimeout(function () {
                wx.hideLoading();
                cur_page++;
            }, 0);
        });
    },
    getLiveLists(page = 1, pageSize = 10) {
        setTimeout(function () {
            wx.showLoading({ title: '加载中', });
        }, 0);
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            currentPage: page,
            pageSize: pageSize,
            ...publicParams
        };
        $API.works.getLiveListPage(params).then(res => {
            if (res.data && res.data.length < 1) {
                return wx.showToast({
                    title: '已是最后一页',
                    icon: 'none'
                });
            }
            const new_list = page == 1 ? res.data : [...this.data.liveLists, ...res.data];
            this.setData({
                refresher: false,
                liveLists: new_list || [],
            });
            setTimeout(function () {
                wx.hideLoading();
                cur_page++;
            }, 0);
        });
    },
    changeArea(e) {
        var type = e.currentTarget.dataset.type;
        this.setData({
            showType: +type
        });
    },
    changeSwiper(e) {
        const {
            current,
        } = e.detail;
        this.setData({
            showType: current + 1
        });
    },
})