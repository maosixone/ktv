/**
 * RongIMLib-3.0.2-upload.js 该 SDK 为 BOS 上传分支 SDK, 调用 getFileUrl 会返回上传必要的 header 头 Authorization（ bosToken ） 、 x-bce-date （ bosDate ） 
 */
const RongIMLib = require('@rongcloud/imlib-v4');
import {
	rongInfo,
} from '../config';

let currentUser = {}, conversationList = [];

let config = Object.assign({}, rongInfo, {
	appkey: 'n19jmcy5nbcv9',
	url: '',
	token: wx.getStorageSync('user_info').rong_token || '',
	wsScheme: 'wss://',
	protocol: 'https://'
});

let imInstance = RongIMLib.init(config);

const getExistedConversationList = () => {
	return imInstance.Conversation.getList().then(list => {
		console.log('获取会话列表成功', list);
		conversationList = list;
		return conversationList;
	});
};

const clearUnreadCount = (data) => {
	imInstance.Conversation.get({
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE,
		targetId: data.targetId,
	}).read();
}

const s_watch = (watcher) => {
	imInstance.watch({
		// 监听 IM 连接状态变化
		status(event) {
			const status = event.status;
			watcher(status);
		},
	});
};

const c_watch = (watcher) => {
	imInstance.watch({
		conversation: function (event) {
			const {
				updatedConversationList
			} = event;
			console.log('updatedConversationList', updatedConversationList);
			conversationList = imInstance.Conversation.merge({
				conversationList,
				updatedConversationList
			});
			console.log('conversationList', conversationList);
			watcher(conversationList);
		}
	});
};

const m_watch = (watcher) => {
	imInstance.watch({
		// 监听消息通知
		message(event) {
			// 新接收到的消息内容
			const message = event.message;
			watcher(message);
		},
	});
};


const chatRoom_watch = (watcher) => {
	imInstance.watch({
		// 监听聊天室 KV 数据变更，进入、退出聊天室，销毁聊天室
		chatroom(event) {
			/**
			 * 聊天室 KV 存储数据更新
			 * @example
			 * [
			 *  {
			 *    "key": "name",
			 *    "value": "我是小融融",
			 *    "timestamp": 1597591258338, 
			 *    "chatroomId": "z002", 
			 *    "type": 1 // 1: 更新（ 含:修改和新增 ）、2: 删除
			 *  },
			 * ]
			 */
			console.log(event);
			const updatedEntries = event.updatedEntries
			watcher(updatedEntries);
		},
	})
};

const cr_getInfo = (id) => {
	const chatRoom = imInstance.ChatRoom.get({
		id: id,
	});
	return chatRoom.getInfo().then(function (result) {
		const userCount = result.userCount;
		const userInfos = result.userInfos;
		console.log('获取聊天室信息成功', userCount, userInfos);
		return result;
	});
};

const cr_getMessages = (id) => {
	const chatRoom = imInstance.ChatRoom.get({
		id: id,
	});
	const option = {
		timestamp: +new Date(),
		count: 20
	};
	return chatRoom.getMessages(option).then(function (result) {
		var list = result.list; // 历史消息列表
		var hasMore = result.hasMore; // 是否还有历史消息可以获取
		console.log('获取聊天室历史消息成功', list, hasMore);
		return result;
	});
};


const cr_join = (id) => {
	const chatRoom = imInstance.ChatRoom.get({ id: id });
	return chatRoom.join({ count: 20 });
};

const cr_quit = (id) => {
	const chatRoom = imInstance.ChatRoom.get({ id: id });
	return chatRoom.quit();
};

const cr_sendJoin = (params) => {
	const { targetId, content } = params;
	return cr_sendMessage(targetId, {
		type: 'join',
		content
	});
};

const cr_sendGift = (params) => {
	const { targetId, content } = params;
	return cr_sendMessage(targetId, {
		type: 'gift',
		content
	});
};

const cr_sendText = (params) => {
	const { targetId, content } = params;
	return cr_sendMessage(targetId, {
		type: 'text',
		content
	});
};

const cr_sendMessage = (targetId, message) => {
	let messageMap = {
		text: (params) => {
			params.messageType = RongIMLib.MESSAGE_TYPE.TEXT;
			return params;
		},
		imageText: (params) => {
			params.messageType = RongIMLib.MESSAGE_TYPE.RICH_CONTENT;
			return params;
		},
		gif: (params) => {
			params.messageType = RongIMLib.MESSAGE_TYPE.GIF;
			return params;
		},
		image: (params) => {
			params.messageType = RongIMLib.MESSAGE_TYPE.IMAGE;
			return params;
		},
		voice: (params) => {
			params.messageType = RongIMLib.MESSAGE_TYPE.HQ_VOICE;
			return params;
		},
		file: (params) => {
			params.messageType = RongIMLib.MESSAGE_TYPE.FILE;
			return params;
		},
		join: (params) => {
			params.messageType = 'RC:Chatroom:Welcome';
			return params;
		},
		gift: (params) => {
			params.messageType = 'RC:Chatroom:Gift';
			return params;
		},
	};
	let params = message;
	params = messageMap[message.type](params);

	const chatRoom = imInstance.ChatRoom.get({
		id: targetId
	});
	return chatRoom.send(params).then(function (message) {
		return message;
	});
};

const connect = (token) => {
	return imInstance.connect({ token }).then(user => {
		currentUser.id = user.id;
		return user;
	}).catch(error => {
		console.log('链接失败: ', error, error.code, error.msg);
	});
};

const disconnect = () => {
	return imInstance.disconnect().then(() => {
		console.log('断开链接成功')
	});
};

const getTotalUnreadCount = () => {
	const conversationTypes = [RongIMLib.CONVERSATION_TYPE.PRIVATE]
	return imInstance.Conversation.getTotalUnreadCount(false, conversationTypes).then((totalUnreadCount) => {
		console.log('获取未读总数成功', totalUnreadCount)
		return String(totalUnreadCount);
	})
};

const setTop = (id, isTop) => {
	const conversation = imInstance.Conversation.get({
		targetId: id,
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE,
	})
	const statusItem = {
		isTop: isTop,
	}
	return conversation.setStatus(statusItem);
};

const c_delete = (id) => {
	const conversation = imInstance.Conversation.get({
		targetId: id,
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE
	});
	return conversation.destory();
};

const sendText = (params) => {
	let { targetId, content } = params;
	return sendMessage(targetId, {
		type: 'text',
		content
	});
};

const create = (params) => {
	let {
		name,
		targetId,
		content
	} = params;
	let message = {
		messageType: name,
		targetId: targetId,
		senderUserId: currentUser.id,
		content: content,
		messageDirection: 1,
		messageUId: 'M' + Date.now(),
		sentTime: Date.now()
	};
	return message;
};

const deleteMsg = (params) => {
	const { targetId, messageUId, sentTime, messageDirection } = params;
	const conversation = imInstance.Conversation.get({
		targetId: targetId,
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE
	});
	return conversation.deleteMessages([
		{ messageUId: messageUId, sentTime: sentTime, messageDirection: messageDirection },
	]);
};

const setDraft = (params) => {
	const { targetId, content } = params;
	const conversation = imInstance.Conversation.get({
		targetId: targetId,
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE
	});
	return conversation.setDraft(content);
};

const getDraft = (params) => {
	const { targetId } = params;
	const conversation = imInstance.Conversation.get({
		targetId: targetId,
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE
	});
	return conversation.getDraft();
}

const sendImage = (params) => {
	let {
		targetId,
		content,
		imageUri,
		extra
	} = params;
	return sendMessage(targetId, {
		type: 'image',
		content,
		imageUri,
		extra
	})
};

const sendVoice = (params) => {
	let {
		targetId,
		content
	} = params;
	let data = Object.assign({ type: 'voice' }, content);
	return sendMessage(targetId, data);
};


const sendMessage = (targetId, message) => {
	let messageMap = {
		text: (params) => {
			params.messageType = 'RC:TxtMsg';
			return params;
		},
		image: (params) => {
			params.messageType = 'RC:ImgMsg';
			return params;
		},
		voice: (params) => {
			params.messageType = 'RC:HQVCMsg';
			return params;
		},
		music: (params) => {
			params.messageType = 'seal:music';
			return params;
		},
		file: (params) => {
			params.messageType = 'RC:FileMsg';
			return params;
		}
	};
	let params = {
		content: message
	};
	params = messageMap[message.type](params);
	const conversation = imInstance.Conversation.get({
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE,
		targetId
	});
	return conversation.send(params).then((message) => {
		return message;
	});
};

const getMessageList = (params) => {
	let {
		targetId,
		position,
		count
	} = params;
	count = count || 20;
	const conversation = imInstance.Conversation.get({
		targetId: targetId,
		type: RongIMLib.CONVERSATION_TYPE.PRIVATE
	});
	const option = {
		// 获取历史消息的时间戳，默认为 0，表示从当前时间获取
		timestamp: position,
		// 获取条数，有效值 1-20，默认为 20
		count: count,
	};
	return conversation.getMessages(option).then(result => {
		const list = result.list;       // 获取到的消息列表
		const hasMore = result.hasMore; // 是否还有历史消息可获取
		// console.log('获取历史消息成功', list, hasMore);
		return {
			messageList: list,
			hasMore
		};
	}).catch(error => {
		console.log('发送文字消息失败', error.code, error.msg);
	});
};

const upLoad = (fileInfo, uploadType) => {
	let fileType = uploadType || RongIMLib.FILE_TYPE.FILE;
	let fileName = fileInfo.name || '';
	return imInstance.getFileToken(fileType, fileName).then(result => {
		let {
			token,
			bosToken,
			bosDate,
			bos,
			path
		} = result;
		let bosHeaders = {
			'authorization': bosToken,
			'x-bce-date': bosDate,
			'Content-Type': 'multipart/form-data',
		}
		let bosUrl = bos + path;
		return wxUpload(fileInfo, token, bosHeaders, bosUrl)
	}).then(res => {
		let qiniuHash, qiniuName;
		if (!res.isBosRes) {
			const {
				data
			} = res;
			const {
				hash,
				name
			} = JSON.parse(data);
			qiniuHash = hash, qiniuName = name;
		}
		return imInstance.getFileUrl(fileType, qiniuHash, qiniuName, res);
	});
};

const wxUpload = (fileInfo, token, bosHeaders, bosUrl) => {
	return new Promise((resolve, reject) => {
		const uploadTask = wx.uploadFile({
			url: config.qiniuHost,
			filePath: fileInfo.path,
			name: 'file',
			formData: {
				token: token
			},
			success: resolve,
			fail: function (err) {
				console.log('upload qiniu failed', err);
				uploadBos(bosUrl, fileInfo, bosHeaders).then(function (res) {
					resolve(res);
				}, function (err) {
					reject(err)
				});
			}
		});

		uploadTask.onProgressUpdate(res => {
			console.log('上传进度', res.progress)
			console.log('已经上传的数据长度', res.totalBytesSent)
			console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
		});
	});
};

const uploadBos = (url, fileInfo, header) => {
	return new Promise((resolve, reject) => {
		const fileData = wx.getFileSystemManager().readFileSync(fileInfo.path);
		console.log(url, fileData);
		wx.request({
			url: url,
			header: header,
			method: 'POST',
			data: fileData,
			success: function (res) {
				console.log(res);
				let data = {
					downloadUrl: url, //上传成功的 url 即为下载 url
					isBosRes: true // 判断是否是百度返回
				}
				resolve(data);
			},
			fail: function (err) {
				console.log(err)
				reject()
			}
		})
	})
};

let ErrorInfo = {
	4: {
		code: 4,
		msg: 'Appkey、Token 不匹配'
	},
	3: {
		code: 3,
		msg: '网络不可用'
	}
};


let modules = {
	CONNECTION_STATUS: RongIMLib.CONNECTION_STATUS,
	Status: {
		s_watch,
		connect,
		disconnect,
	},
	Conversation: {
		c_watch,
		getList: getExistedConversationList,
		setDraft,
		getDraft,
		clearUnreadCount,
		getTotalUnreadCount,
		setTop,
		delete: c_delete,
	},
	File: {
		upLoad
	},
	Message: {
		create,
		sendText: sendText,
		sendImage: sendImage,
		sendVoice: sendVoice,
		getMessageList: getMessageList,
		m_watch,
		deleteMsg,
	},
	ChatRoom: {
		join: cr_join,
		quit: cr_quit,
		cr_watch: chatRoom_watch,
		getInfo: cr_getInfo,
		getMessageList: cr_getMessages,
		sendJoin: cr_sendJoin,
		sendGift: cr_sendGift,
		sendText: cr_sendText,
	}
};

module.exports = modules;