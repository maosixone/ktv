// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
const { Status, Message, Conversation, ChatRoom } = require('../services');
const RongEmoji = require('../../static/RongIMEmoji-2.2.6');
RongEmoji.init();
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    API as $API
} from '../../utils/request';
import {
    sliceArray,
} from '../../utils/util';

let live_name = null,
    userInfo = null,
    targetUrl = null,
    cur_broadcast_id = null,
    cur_room_id = null,
    preventBlur = false;	// 阻止由于input失焦引起输入框下落
const formatEmojis = () => {
    let list = RongEmoji.list;
    return sliceArray(list, {
        size: 24
    });
};

Page({
    data: {
        liveInfo: {},
        live_end: false,
        app_iconSrc: imgHosts + 'app_icon.png',
        musicSrc: imgHosts + 'home/green_music.png',
        fabulousSrc: imgHosts + 'lous.png',
        noFabulousSrc: imgHosts + 'home/no_fabulous.png',
        rewardSrc: imgHosts + 'home/reward.png',
        live_rewardSrc: imgHosts + 'home/live_reward.png',
        live_endSrc: imgHosts + 'home/live_end.jpg',
        noDataSrc: imgHosts + 'home/no_data.png',
        liveMessages: [],
        isShowEmojiSent: false,
        isShowKeyboard: false,
        emojis: formatEmojis(),
        show_chatArea: false,
        isFocus: false,
        bottom: 0,
        toView: '',
        content: '',
        display: {
            emoji: 'none',
        },
    },
    // 事件处理函数
    onLoad(option) {
        const { id, broadcast_id, name, stream_name } = option;
        cur_broadcast_id = broadcast_id;
        userInfo = wx.getStorageSync('user_info');
        this.getLiveInfoById();

        Message.m_watch((message) => {
            if (message.isOffLineMessage) {
                return;
            }
            const new_list = this.data.liveMessages;
            if (!new_list) { new_list = []; }
            if (userInfo.rong_id == message.senderUserId && message.messageType == 'RC:Chatroom:Welcome') { return; }
            if (message.senderUserId == 'system' && message.content.content == 'live_end') {
                ChatRoom.quit(targetUrl).then(function () {
                    console.log('退出聊天室成功');
                    wx.showToast({
                        title: '直播已结束',
                        icon: 'none'
                    });
                });
                return this.setData({ live_end: true });
            }
            if (message.messageType == 'RC:Chatroom:Welcome' || message.messageType == 'RC:TxtMsg') {
                const index = new_list.findIndex(item => item.messageUId == message.messageUId);
                if (index != -1) { return; }
                new_list.push(message);
                this.setData({ liveMessages: new_list, toView: message.messageUId });
            }
        });
    },
    getLiveInfoById() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|live_broadcast_id|${cur_broadcast_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            live_broadcast_id: cur_broadcast_id,
            ...publicParams
        };
        $API.works.getLiveInfoById(params).then(res => {
            if (!res.data) { return wx.navigateBack(); }

            cur_room_id = res.data.room_id, targetUrl = res.data.stream_name;

            res.data.name = live_name;

            const content = {
                id: userInfo.rong_id,
                user: {
                    id: userInfo.rong_id,
                    name: userInfo.nick_name,
                    portraitUri: userInfo.images,
                }
            };
            // 首次进入直播间无需退出上一个
            ChatRoom.join(targetUrl, content).then(() => {
                console.log('加入聊天室成功');
                return ChatRoom.sendJoin({
                    targetId: targetUrl,
                    content,
                });
            }).then((message) => {
                const new_list = this.data.liveMessages;
                if (!new_list) { new_list = []; }
                const index = new_list.findIndex(item => item.messageUId == message.messageUId);
                if (index != -1) { return; }
                new_list.push(message);
                this.setData({ liveMessages: new_list, toView: message.messageUId });
            });
            this.setData({
                liveInfo: res.data
            });
        });
    },
    // 关注/取关
    handleFollow(e) {
        const token = wx.getStorageSync('sysToken');
        const {
            type,
            id,
        } = e.currentTarget.dataset
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|follow_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            follow_id: id,
            ...publicParams
        };
        const liveInfo = this.data.liveInfo;
        if (type == 'add') {
            params.remark_name = '';
            params.sign = md5(`dbkey|${publicParams.dbkey}|follow_id|${id}|privateKey|${publicParams.privateKey}|remark_name||token|${token}`);
            $API.works.addFollow(params).then(res => {
                if (res.data.isok > 0) {
                    liveInfo.is_follow = 1;
                    this.setData({
                        liveInfo: liveInfo
                    });
                }
            });
        } else {
            $API.works.cancelFollow(params).then(res => {
                if (res.data.isok > 0) {
                    liveInfo.is_follow = 0;
                    this.setData({
                        liveInfo: liveInfo
                    });
                }
            });
        }
    },
    handleReward(e) {
        const {
            broadcast_id
        } = e.currentTarget.dataset;
        if (targetUrl == 'hanhall') {
            this.nowHallProgramInfo('hanhall');
        }
        else {
            this.getRoomLiveUserInfo(cur_room_id);
        }
    },
    // 大厅直播打赏
    nowHallProgramInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        }, that = this;
        $API.lobby.nowHallProgramInfo(params).then(res => {
            if (res.data.isok != 1) {
                return wx.showToast({
                    title: '暂无播放节目',
                    icon: 'none',
                    duration: 2000
                });
            }
            wx.navigateTo({
                url: `../reward/index?lobby=true&name=${res.data.nick_name}&live_rm_id=${id}&images=${res.data.images}`,
                events: {
                    sendGift: function (data) {
                        const content = {
                            id: data.data,
                            number: 1,
                            total: 0,
                            user: {
                                id: userInfo.rong_id,
                                name: userInfo.nick_name,
                                portraitUri: userInfo.images,
                            }
                        };
                        // 发送礼物
                        return ChatRoom.sendGift({
                            targetId: targetUrl,
                            content,
                        });
                    },
                    sendText: async function (data) {
                        const result = await that.getRongInfo(res.data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    // 包厢打赏
    getRoomLiveUserInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${id}|token|${token}`),
            token: token,
            room_id: id,
            ...publicParams
        };
        $API.room.getRoomLiveUserInfo(params).then(res => {
            if (res.data.isok != 1) {
                return wx.showToast({
                    title: '暂无播放节目',
                    icon: 'none',
                    duration: 2000
                });
            }
            wx.navigateTo({
                url: `../reward/index?room=true&name=${res.data.nick_name}&live_rm_id=${id}&images=${res.data.images}`,
                events: {
                    sendGift: function (data) {
                        console.log(data);
                        const content = {
                            id: data.data,
                            number: 1,
                            total: 0,
                            user: {
                                id: userInfo.rong_id,
                                name: userInfo.nick_name,
                                portraitUri: userInfo.images,
                            }
                        };
                        // 发送礼物
                        return ChatRoom.sendGift({
                            targetId: targetUrl,
                            content,
                        });
                    },
                    sendText: async function (data) {
                        const result = await that.getRongInfo(res.data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    // 获取融云信息
    getRongInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|ktv_user_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ktv_user_id: id,
            ...publicParams
        };
        return new Promise((resolve, reject) => {
            $API.user.getRongInfo(params).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        });
    },
    // 点赞收藏
    handleStar(e) {
        const token = wx.getStorageSync('sysToken');
        const {
            type,
            version,
            worksid,
        } = e.currentTarget.dataset
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|is_star|${type}|privateKey|${publicParams.privateKey}|token|${token}|version|${version}|works_id|${worksid}`),
            token: token,
            version: version,
            works_id: worksid,
            is_star: type,
            ...publicParams
        };
        const liveInfo = this.data.liveInfo;
        $API.works.updateStar(params).then(res => {
            if (res.data.isok > 0) {
                liveInfo.is_star = type;
                liveInfo.fabulous_count = res.data.star_count;
                this.setData({
                    liveInfo: liveInfo
                });
            }
        });
    },
    onInput(event) {
        this.setData({
            content: event.detail.value
        });
    },
    onFocus(event) {
        let {
            height
        } = event.detail;
        height < 0 && (height = 0);
        let adapterHeight = 0;
        this.setKeyboardPos(height, adapterHeight);
        this.hideSoftKeyboard();
    },
    onBlur() {
        if (preventBlur) { return false; }
        let adapterHeight = 0;
        this.setKeyboardPos(0, adapterHeight);
    },
    showEmojis() {
        preventBlur = true;
        this.showSoftKeyboard({
            emoji: 'block',
        });
    },
    preventTap() {
        return false;
    },
    showSendArea() {
        this.setData({
            isFocus: true,
            show_chatArea: true,
        });
    },
    closeSendArea() {
        preventBlur = false;
        let adapterHeight = 0;
        this.setKeyboardPos(0, adapterHeight);
        this.hideSoftKeyboard();
        this.setData({
            isFocus: false,
            show_chatArea: false,
        });
    },
    showSoftKeyboard(display) {
        setTimeout(() => {
            this.setData({
                isShowEmojiSent: true,
                display: display,
                bottom: 210,
                toView: this.getToView()
            });
        }, 100);
    },
    setKeyboardPos(keyboardHeight, adapterHeight) {
        keyboardHeight = keyboardHeight || 0;
        let data;
        let isScroll = (keyboardHeight > 0);
        console.log(keyboardHeight, adapterHeight);
        if (isScroll) {
            data = {
                bottom: adapterHeight + keyboardHeight,
                toView: this.getToView()
            };
        } else {
            data = {
                bottom: adapterHeight + keyboardHeight,
                isShowKeyboard: false,
            };
        }
        this.setData(data);
    },
    sendText() {
        let {
            content,
            liveMessages,
        } = this.data;
        if (content.length < 1) { return; }
        this.setData({
            content: '',
            isShowEmojiSent: false
        });
        if (content.length == 0) {
            return;
        }
        const new_content = {
            content: content,
            user: {
                id: userInfo.rong_id,
                name: userInfo.nick_name,
                portraitUri: userInfo.images,
            }
        }
        ChatRoom.sendText({
            targetId: targetUrl,
            content: new_content,
        }).then(message => {
            const new_list = liveMessages;
            if (!new_list) { new_list = []; }
            const index = new_list.findIndex(item => item.messageUId == message.messageUId);
            if (index != -1) { return; }
            new_list.push(message);
            this.setData({
                liveMessages: new_list,
                toView: message.messageUId
            });
            this.closeSendArea();
        });
    },
    selectEmoji(e) {
        let { content } = this.data, { emoji } = e.target.dataset;
        content = content + emoji;
        this.setData({
            content: content,
            isShowEmojiSent: true
        });
    },
    getToView() {
        let {
            liveMessages,
            playerLiveId,
        } = this.data;
        let index = liveMessages.length - 1;
        let message = liveMessages[index] || {};
        return message.messageUId || '';
    },
    hideSoftKeyboard() {
        this.setData({
            display: {
                emoji: 'none',
            }
        });
    },
    statechange(e) {
        console.log('live-player code:', e.detail.code)
    },
    error(e) {
        console.error('live-player error:', e.detail.errMsg)
    },
})