// index.js
// 获取应用实例
const app = getApp();
const { Status, Conversation, CONNECTION_STATUS } = require('../services');
import md5 from 'md5';;
import {
	publicParams,
	imgHosts
} from '../../config';
import {
	formatDate
} from '../../utils/util.js';
import {
	API as $API
} from '../../utils/request';

Page({
	data: {
		searchSrc: imgHosts + 'search.png',
		noDataSrc: imgHosts + 'home/no_data.png',
		conversationList: false,
	},
	// 事件处理函数
	onLoad() {
		// const { rong_token } = wx.getStorageSync('user_info');
		// Status.connect(rong_token).then((user) => {
		// 	return Conversation.getList();
		// }).then(list => {
		// 	this.formatLists(list);
		// });
		// Message.sendMessage('15732167970', 'message');
	},
	onHide() {
		Conversation.c_watch((list) => {
			return false;
		});
	},
	onShow() {
		if (typeof this.getTabBar === 'function' && this.getTabBar()) {
			this.getTabBar().setData({
				selected: 3,
				show: true
			});
		}
		const that = this;
		Conversation.c_watch((list) => {
			that.formatLists(list);
			that.getTotalUnreadCount();
		});
		Status.s_watch((status) => {
			switch (status) {
				case CONNECTION_STATUS.CONNECTED:
					wx.hideLoading();
					break;
				case CONNECTION_STATUS.NETWORK_UNAVAILABLE:
					wx.showLoading({
						title: '重连中 ...',
					});
					break;
				case CONNECTION_STATUS.KICKED_OFFLINE_BY_OTHER_CLIENT:
					wx.showModal({
						title: '提示',
						content: '当前用户已在其他端登录',
						success(res) {
							// 点击确定回调
							if (res.confirm) {
								that.logout();
							}
						}
					});
					break;
			}
		});
		Conversation.getList().then(list => {
			this.formatLists(list);
			this.getTotalUnreadCount();
		});
		// Conversation.getList().then(list => {
		// 	this.formatLists(list);
		// });
	},
	showHandle(e) {
		const that = this, { id, istop } = e.currentTarget.dataset;
		let items = ['置顶该会话', '从会话列表中移除'];
		if (istop) { items[0] = '取消置顶' }
		// if (e.detail.messageType == '') { }
		wx.showActionSheet({
			itemList: items,
			success(res) {
				console.log(res.tapIndex);
				if (res.tapIndex == 0) {
					that.topThis(id, istop);
				}
				else if (res.tapIndex == 1) {
					that.deleteThis(id);
				}
			},
			fail(res) {
				console.log(res.errMsg)
			}
		})
	},
	topThis(id, istop) {
		Conversation.setTop(id, !istop).then(() => {
			console.log('设置成功');
		});
	},
	deleteThis(id) {
		const that = this, { conversationList } = this.data;
		const message = conversationList.find(item => item.targetId == id);
		// 有未读消息时，先清除未读消息再删除
		if (message && message.unreadMessageCount > 0) {
			Conversation.clearUnreadCount({
				targetId: id,
			});
		}
		Conversation.delete(id).then(() => {
			console.log('删除成功');
		});
		setTimeout(() => {
			Conversation.getList().then(list => {
				if (list.length > 0) {
					that.formatLists(list);
				}
				else {
					that.setData({ conversationList: list });
				}
				that.getTotalUnreadCount();
			});
		}, 300)
	},
	getTotalUnreadCount() {
		Conversation.getTotalUnreadCount().then((count) => {
			if (count > 0) {
				wx.setTabBarBadge({
					index: 3,
					text: count,
				});
			}
			else {
				wx.removeTabBarBadge({ index: 3 });
			}
		});
	},
	formatLists: async function (list) {
		if (list.length == 0) {
			return this.setData({ conversationList: list });
		}
		for (let i = 0, len = list.length; i < len; i++) {
			const newDate = formatDate(new Date(list[i].latestMessage.sentTime));
			list[i].sendTime_Date = newDate;
			if (!list[i].nick_name) {
				// console.log('for i', list[i]);
				const res = await this.getChatInfoById(list[i].targetId);
				list[i] = Object.assign({}, list[i], res.data);
				if (i == list.length - 1) {
					this.setData({ conversationList: list });
				}
			}
			else {
				if (i == list.length - 1) {
					this.setData({ conversationList: list });
				}
			}
		}
	},
	getChatInfoById(id) {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`chat_id|${id}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			chat_id: id,
			...publicParams
		};
		return new Promise((resolve, reject) => {
			$API.user.getChatInfoById(params).then(res => {
				resolve(res);
			}).catch(error => {
				reject(error);
			});
		})
	},
	chatToUser(e) {
		const { id, image, name } = e.currentTarget.dataset;
		Conversation.clearUnreadCount({
			targetId: id,
		});
		wx.navigateTo({
			url: `./chat/index?id=${id}&name=${name}&image=${image}`,
		});
	},
	gotoSearch() {
		wx.navigateTo({
			url: '../userCenter/contactList/index?type=1&search=true',
		});
	},
	logout() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		Status.disconnect();
		$API.user.logout(params).then(res => {
			if (res.data.isok == 1) {
				wx.clearStorageSync();
				wx.reLaunch({
					url: '../login/index',
				});
			}
		});
	},
})