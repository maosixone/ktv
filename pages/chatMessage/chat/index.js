// index.js
// 获取应用实例
const app = getApp();
const { Message, Conversation, File } = require('../../services');
const RongEmoji = require('../../../static/RongIMEmoji-2.2.6');
RongEmoji.init();

import {
	imgHosts
} from '../../../config';
import {
	sliceArray,
	compress,
	getAdapterheight,
	getBase64Image
} from '../../../utils/util';


const formatEmojis = () => {
	let list = RongEmoji.list;
	return sliceArray(list, {
		size: 24
	});
};
const recorderManager = wx.getRecorderManager();

Page({
	data: {
		adapterHeight: getAdapterheight(),
		searchSrc: imgHosts + 'search.png',
		messageList: [],
		content: '',
		hasMore: false,
		display: {
			emoji: 'none',
			more: 'none'
		},
		bottom: 0,
		emojis: formatEmojis(),
		isShowEmojiSent: false,
		isRecording: false,
		isShowKeyboard: false,
		isAllowScroll: false,
		targetId: '',
		targetUserInfo: false,
		localUserInfo: false,
		playingVoice: null,
		toView: '',
	},
	// 事件处理函数
	onLoad(option) {
		const { id, image, name } = option, { adapterHeight } = this.data, userInfo = wx.getStorageSync('user_info');
		wx.setNavigationBarTitle({ title: name });
		const targetUserInfo = {
			id,
			name,
			avatar: image
		}, localUserInfo = {
			id: userInfo.rong_id,
			name: userInfo.nick_name,
			avatar: userInfo.images,
		};
		this.getDraft(id);
		this.setData({ targetUserInfo, localUserInfo, targetId: id });
		this.setKeyboardPos(0, adapterHeight);
		let position = 0;
		this.getMessageList({ position, targetId: id });

		Message.m_watch((message) => {
			if (message.isOffLineMessage) {
				return;
			}
			if (message.type == 1 && message.messageType !== 'RC:TypSts' && message.targetId === id) {
				let {
					messageList
				} = this.data;
				messageList.push(message);
				this.setData({
					messageList,
					toView: message.messageUId
				});
				Conversation.clearUnreadCount({
					targetId: id,
				});
			}
		});
		// Message.getMessageList({
		// 	targetId: id,
		// 	position,
		// }).then((result) => {
		// 	const { hasMore } = result;
		// 	const new_messageList = [...messageList, ...result.messageList];
		// 	if (position == 0) {
		// 		let index = messageList.length - 1;
		// 		let message = messageList[index] || {};
		// 		toView = message.messageUId || '';
		// 	}
		// 	this.setData({ messageList: new_messageList, hasMore: hasMore, });
		// });
	},
	onUnload() {
		let { targetId, content } = this.data;
		Message.m_watch((message) => { return message; })
		if (content.length > 0) {
			// 关闭消息监听
			Conversation.setDraft({ targetId, content }).then(() => {
				console.log('设置指定会话草稿成功');
			});
		}
	},
	getDraft(id) {
		Conversation.getDraft({ targetId: id }).then((draft) => {
			if (draft && draft.length > 0) {
				this.setData({ content: draft });
			}
		});
	},
	getMoreMessages() {
		let {
			targetId,
			hasMore,
			messageList
		} = this.data;
		messageList = messageList || [];
		let firstMessage = messageList[0] || {};
		let position = firstMessage.sentTime || 0;
		let count = 15;
		if (hasMore) {
			this.setData({
				isAllowScroll: true
			});
			this.getMessageList({
				targetId,
				position,
				count
			});
		}
	},
	getMessageList(params) {
		let {
			position
		} = params, { messageList } = this.data;
		return Message.getMessageList(params).then((result) => {
			const { hasMore } = result;
			const new_messageList = messageList.length > 0 ? [...result.messageList, ...messageList] : result.messageList;
			console.log('new_messageList', new_messageList);
			let toView = '';
			if (position == 0) {
				const index = new_messageList.length - 1;
				const message = new_messageList[index] || {};
				toView = message.messageUId || '';
			}

			if (!hasMore && position != 0) {
				// 灰条提示
				toView = 'message-notify-without';
				this.setData({
					hasMore: hasMore
				});
			}
			if (position == 0) {
				this.setData({
					messageList: new_messageList,
					hasMore: hasMore,
					isAllowScroll: false,
					toView: toView
				});
			}
			else {
				this.setData({
					messageList: new_messageList, hasMore: hasMore, isAllowScroll: false,
				});
			}
		});
	},
	onInput(event) {
		this.setData({
			content: event.detail.value
		});
	},
	onFocus(event) {
		let {
			height
		} = event.detail;
		let adapterHeight = 0;
		this.setKeyboardPos(height, adapterHeight);
		this.hideSoftKeyboard();
	},
	onBlur(event) {
		let adapterHeight = 0;
		this.setKeyboardPos(0, adapterHeight);
	},
	hideBottom() {
		const adapterHeight = getAdapterheight();
		this.setKeyboardPos(0, adapterHeight);
		this.hideSoftKeyboard();
	},
	sendText() {
		let {
			content,
			targetId,
			messageList
		} = this.data;

		this.setData({
			content: '',
			isShowEmojiSent: false
		});
		if (content.length == 0) {
			return;
		}
		Message.sendText({
			targetId,
			content
		}).then(message => {
			messageList.push(message);
			this.setData({
				messageList,
				toView: message.messageUId
			});
		});
	},
	selectEmoji(e) {
		let { content } = this.data, { emoji } = e.target.dataset;
		content = content + emoji;
		this.setData({
			content: content,
			isShowEmojiSent: true
		});
	},
	sendImage() {
		wx.chooseImage({
			count: 1,
			sizeType: ['original', 'compressed'],
			sourceType: ['album', 'camera'],
			success: (res) => {
				let {
					tempFilePaths,
					tempFiles
				} = res;
				let tempFilePath = tempFilePaths[0];
				// const base64_res = wx.getFileSystemManager().readFileSync(tempFilePath, 'base64');
				getBase64Image('my-canvas', tempFilePath, (base64) => {
					wx.getImageInfo({
						src: tempFilePath,
						success: (res) => {
							let extra = compress(res);
							let {
								targetId,
								messageList
							} = this.data;

							let name = 'RC:ImgMsg';
							let content = {
								imageUri: tempFilePath,
								extra
							};
							let message = Message.create({
								targetId,
								name,
								content
							});

							messageList.push(message);
							this.setData({
								messageList,
								toView: message.messageUId
							});

							File.upLoad({
								path: tempFilePath,
							}, 1, tempFiles[0]).then(result => {
								let {
									downloadUrl: imageUri,
								} = result;
								console.log(result);
								Message.sendImage({
									targetId,
									imageUri,
									extra,
									content: base64
								}).then(message => { });
							});
						}
					});
				});
			}
		})
	},
	showMore() {
		this.showSoftKeyboard({
			emoji: 'none',
			more: 'block'
		});
	},
	showVoice() {
		this.setData({
			isShowKeyboard: false
		});
		this.hideKeyboard();
	},
	showEmojis() {
		this.showSoftKeyboard({
			emoji: 'block',
			more: 'none'
		});
	},
	onPlayVoice(e) {
		let voiceComponent = e.detail;
		let {
			playingVoice
		} = this.data;
		if (playingVoice) {
			let playingId = playingVoice.__wxExparserNodeId__;
			let voiceId = voiceComponent.__wxExparserNodeId__;
			// 两次播放为同个音频，状态保持不变
			if (playingId == voiceId) {
				return;
			}
			let {
				innerAudiothis
			} = playingVoice.data;
			playingVoice.setData({
				isPlaying: false
			});
			innerAudiothis.stop();
		}
		this.setData({
			playingVoice: voiceComponent
		});
	},
	onLongpress(e) {
		const that = this;
		let items = ['删除消息'];
		// if (e.detail.messageType == '') { }
		wx.showActionSheet({
			itemList: items,
			success(res) {
				console.log(res.tapIndex);
				if (res.tapIndex == 0) {
					that.deleteMsg(e.detail);
				}
			},
			fail(res) {
				console.log(res.errMsg)
			}
		})
	},
	deleteMsg(message) {
		let {
			targetId,
		} = this.data;
		Message.deleteMsg({
			targetId,
			messageUId: message.messageUId,
			sentTime: message.sentTime,
			messageDirection: message.messageDirection
		}).then(() => {
			const index = this.data.messageList.findIndex(item => item.messageUId == message.messageUId);
			this.data.messageList.splice(index, 1);
			const new_messageList = [].concat(this.data.messageList);
			this.setData({ messageList: new_messageList, toView: new_messageList[new_messageList.length - 1].messageUId });
		});
	},
	onPreviewImage(e) {
		let currentImageUrl = e.detail;
		let urls = this.getImageUrls();
		if (urls.length === 0) {
			urls.push(currentImageUrl);
		}
		wx.previewImage({
			current: currentImageUrl,
			urls: urls
		})
	},
	getImageUrls() {
		let {
			messageList
		} = this.data;
		return messageList.filter(message => {
			return (message.messageType == 'RC:ImgMsg' || message.messageType == 'RC:GIFMsg');
		}).map(message => {
			return message.content.imageUri || message.content.remoteUrl;
		});
	},
	startRecording() {
		this.setData({
			isRecording: true
		});
		let record = () => {
			recorderManager.start({
				format: 'aac'
			});
		};
		wx.getSetting({
			success(res) {
				if (!res.authSetting['scope.record']) {
					wx.authorize({
						scope: 'scope.record',
						success: record
					})
				} else {
					record();
				}
			}
		})
	},
	stopRecording() {
		this.setData({
			isRecording: false
		});
		recorderManager.onStop((res) => {
			const {
				tempFilePath,
				duration
			} = res;
			File.upLoad({
				path: tempFilePath,
				name: 'voice.aac'
			}, 2).then(file => {
				let content = {
					remoteUrl: file.downloadUrl,
					duration: Math.ceil(duration / 1000)
				};
				let {
					type,
					targetId,
					messageList
				} = this.data;
				Message.sendVoice({
					type,
					targetId,
					content
				}).then(message => {
					messageList.push(message);
					this.setData({
						messageList,
						toView: message.messageUId
					});
				});
			});
		})
		recorderManager.stop();
	},
	showKeyboard() {
		this.setData({ isShowKeyboard: true });
	},
	hideKeyboard() {
		let keyboardHeight = 0;
		let {
			adapterHeight
		} = this.data;
		this.setKeyboardPos(keyboardHeight, adapterHeight);
		this.hideSoftKeyboard();
	},
	showSoftKeyboard(display) {
		setTimeout(() => {
			this.setData({
				display: display,
				bottom: 210,
				isShowKeyboard: false,
				toView: this.getToView()
			});
		}, 100);
	},
	hideSoftKeyboard() {
		this.setData({
			display: {
				emoji: 'none',
				more: 'none'
			}
		});
	},
	setKeyboardPos(keyboardHeight, adapterHeight) {
		keyboardHeight = keyboardHeight || 0;
		let data;
		let isScroll = (keyboardHeight > 0);
		if (isScroll) {
			data = {
				bottom: adapterHeight + keyboardHeight,
				isShowEmojiSent: false,
				toView: this.getToView()
			};
		} else {
			data = {
				bottom: adapterHeight + keyboardHeight,
				isShowEmojiSent: false
			};
		}
		this.setData(data);
	},
	getToView() {
		let {
			messageList
		} = this.data;
		let index = messageList.length - 1;
		let message = messageList[index] || {};
		return message.messageUId || '';
	},
})