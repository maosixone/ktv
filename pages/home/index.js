// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
const { Status, Message, Conversation, ChatRoom } = require('../services');
const RongEmoji = require('../../static/RongIMEmoji-2.2.6');
RongEmoji.init();
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    API as $API
} from '../../utils/request';
import {
    sliceArray,
    checkOrientation
} from '../../utils/util';

let cur_page = [1, 1, 1],
    updateSlider = false,
    cur_duration = 0,
    cur_videoContext = null,
    userInfo = null,
    targetUrl = null,
    showType_curIndex = 0,
    videoIndex = [0, 0, 0],
    delayTimer = null,
    allowPause = false,		//	等到数据加载后才允许暂停操作
    preventBlur = false;	// 阻止由于input失焦引起输入框下落
const formatEmojis = () => {
    let list = RongEmoji.list;
    return sliceArray(list, {
        size: 24
    });
};

Page({
    data: {
        showType: 1,
        isPause: false,
        recommendLists: false, //  推荐列表
        ownWorkLists: [], //  作品列表
        liveLists: [], //  直播列表
        videoList: [],
        sliderValue: [],
        recommendCurrent: 0,
        playWorksId: '',
        playOwnWorksId: '',
        playerLiveId: '',
        hide_animation: false,
        index_playSrc: imgHosts + 'index_play.png',
        app_iconSrc: imgHosts + 'app_icon.png',
        activeSrc: imgHosts + 'active.png',
        moreIcoSrc: imgHosts + 'home/more_ico.png',
        moreRightSrc: imgHosts + 'home/more_right.png',
        musicSrc: imgHosts + 'home/green_music.png',
        fabulousSrc: imgHosts + 'home/fabulous.png',
        noFabulousSrc: imgHosts + 'home/no_fabulous.png',
        rewardSrc: imgHosts + 'home/reward.png',
        live_rewardSrc: imgHosts + 'home/live_reward.png',
        live_endSrc: imgHosts + 'home/live_end.jpg',
        noDataSrc: imgHosts + 'home/no_data.png',
        liveMessages: {},
        isShowEmojiSent: false,
        isShowKeyboard: false,
        emojis: formatEmojis(),
        show_chatArea: false,
        isFocus: false,
        bottom: 0,
        toView: '',
        content: '',
        display: {
            emoji: 'none',
        },
    },
    onReady() {
        this.ani = wx.createAnimation({
            duration: 2000,
            timingFunction: 'ease'
        });
        this.ani.opacity(0).step();
        this.setData({
            hide_animation: this.ani.export()
        });
        this.ani.opacity(1).translateZ(1).step();
        this.setData({
            show_animation: this.ani.export()
        });
    },
    onLoad() {
        cur_page = [1, 1, 1];
        updateSlider = true;
        checkOrientation('judge-canvas');
        // 连接融云
        const { rong_token } = wx.getStorageSync('user_info');
        Status.connect(rong_token).then((user) => {
            this.getTotalUnreadCount();
        });
    },
    onHide() {
        clearTimeout(delayTimer);
        Conversation.c_watch((list) => {
            return false;
        });
    },
    // 事件处理函数
    onShow() {
        if (typeof this.getTabBar === 'function' && this.getTabBar()) {
            this.getTabBar().setData({
                selected: 0,
                show: true
            });
        }
        userInfo = wx.getStorageSync('user_info');
        // cur_page[0] == 1 ? cur_page[0] = 1 : cur_page[0] = cur_page[0] - 1;
        // cur_page[1] == 1 ? cur_page[1] = 1 : cur_page[1] = cur_page[1] - 1;
        // cur_page[2] == 1 ? cur_page[2] = 1 : cur_page[2] = cur_page[2] - 1;
        this.getRecommendLists(cur_page[0]);
        const that = this;
        delayTimer = setTimeout(function () {
            that.getOwnWorkLists(cur_page[1]);
            that.getLiveLists(1, cur_page[2] * 15);
        }, 100)

        Conversation.c_watch((list) => {
            this.getTotalUnreadCount();
        });
        Message.m_watch((message) => {
            if (message.isOffLineMessage) {
                return;
            }
            const new_list = this.data.liveMessages;
            console.log('chatRoom', message);
            if (!new_list[this.data.playerLiveId] || new_list[this.data.playerLiveId].length == 0) { return; }
            if (userInfo.rong_id == message.senderUserId && message.messageType == 'RC:Chatroom:Welcome') { return; }
            if (message.senderUserId == 'system' && message.content.content == 'live_end') {
                const new_list = [].concat(this.data.liveLists);
                const index = new_list.findIndex(item => item.live_broadcast_id == this.data.playerLiveId);
                new_list[index].cover_image = this.data.live_endSrc;
                if (new_list[index]) {
                    ChatRoom.quit(new_list[index].stream_name).then(function () {
                        console.log('退出聊天室成功');
                        wx.showToast({
                            title: '直播结束',
                            icon: 'none',
                        });
                    });
                    return this.setData({ playerLiveId: '', liveLists: new_list });
                }
            }
            if (message.messageType == 'RC:Chatroom:Welcome' || message.messageType == 'RC:TxtMsg') {
                const index = new_list[this.data.playerLiveId].findIndex(item => item.messageUId == message.messageUId);
                if (index != -1) { return; }
                new_list[this.data.playerLiveId].push(message);
                this.setData({ liveMessages: new_list, toView: message.messageUId });
            }
        });
    },
    getTotalUnreadCount() {
        Conversation.getTotalUnreadCount().then((count) => {
            if (count > 0) {
                wx.setTabBarBadge({
                    index: 3,
                    text: count,
                });
            }
            else {
                wx.removeTabBarBadge({ index: 3 });
            }
        });
    },
    getRecommendLists(page = 1, pageSize = 15) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${page}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            currentPage: page,
            pageSize: pageSize,
            ...publicParams
        };
        $API.works.getWorksListPage(params).then(res => {
            if (page != 1 && (!res.data.worksList || res.data.worksList.length == 0)) { return; }
            if (this.data.playWorksId == '' && res.data.worksList.length > 0) {
                cur_videoContext = wx.createVideoContext(`v-${res.data.worksList[0].works_id}`);
                allowPause = false;
                wx.showLoading({
                    title: '加载中',
                    mask: true,
                });
                this.setData({
                    playWorksId: res.data.worksList[0].works_id,
                });
            }
            const newWorksList = page == 1 ? res.data.worksList : [...this.data.recommendLists, ...res.data.worksList];
            this.setData({
                recommendLists: newWorksList,
            }, () => { cur_page[0]++; });
        })
    },
    getOwnWorkLists(currentPage = 1, pageSize = 15) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${currentPage}|dbkey|${publicParams.dbkey}|is_download||pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            is_download: '',
            currentPage: currentPage,
            pageSize: pageSize,
            ...publicParams
        };
        $API.works.getPersonWorksList(params).then(res => {
            if (!res.data.worksList || res.data.worksList.length == 0) { return; }
            if (this.data.playOwnWorksId == '') {
                this.setData({
                    playOwnWorksId: res.data.worksList[0].works_id,
                });
            }
            const newWorksList = currentPage == 1 ? res.data.worksList : [...this.data.ownWorkLists, ...res.data.worksList];
            this.setData({
                ownWorkLists: newWorksList
            }, () => { cur_page[1]++; });
        });
    },
    getLiveLists(currentPage = 1, pageSize = 15) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`currentPage|${currentPage}|dbkey|${publicParams.dbkey}|pageSize|${pageSize}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            currentPage: currentPage,
            pageSize: pageSize,
            ...publicParams
        };
        $API.works.getLiveListPage(params).then(res => {
            if (currentPage > 1 && (!res.data || res.data.length == 0)) { return; }
            const index = (res.data || []).findIndex(item => item.live_broadcast_id == this.data.playerLiveId);
            if (index == -1 && res.data && res.data.length > 0) {
                this.setData({
                    playerLiveId: res.data[0].live_broadcast_id
                });
                const cur_live = res.data[0];
                if (cur_live) {
                    const content = {
                        id: userInfo.rong_id,
                        user: {
                            id: userInfo.rong_id,
                            name: userInfo.nick_name,
                            portraitUri: userInfo.images,
                        }
                    };
                    // 首次进入直播间无需退出上一个
                    ChatRoom.join(cur_live.stream_name, content).then(() => {
                        console.log('加入聊天室成功');
                        targetUrl = cur_live.stream_name;
                        return ChatRoom.sendJoin({
                            targetId: cur_live.stream_name,
                            content,
                        });
                    }).then((message) => {
                        const new_list = {};
                        // if (!new_list[playerLiveId]) { new_list[playerLiveId] = []; }
                        new_list[cur_live.live_broadcast_id] = []
                        // const index = new_list[playerLiveId].findIndex(item => item.messageUId == message.messageUId);
                        // if (index != -1) { return; }
                        new_list[cur_live.live_broadcast_id].push(message);
                        this.setData({ liveMessages: new_list, toView: message.messageUId });
                    });
                }
            }
            const newLiveLists = currentPage == 1 ? (res.data || []) : [...this.data.liveLists, ...res.data];
            this.getLivesRoomInfo(newLiveLists);
            this.setData({
                liveLists: newLiveLists
            }, () => { cur_page[2]++; });

        });
    },
    // 从直播列表提取room_id和stream_name
    getLivesRoomInfo(list) {
        const roomInfoList = wx.getStorageSync('roomInfoList') || [];
        for (let i = 0; i < list.length; i++) {
            const li = {
                room_id: list[i].room_id,
                stream_name: list[i].stream_name,
            };
            const index = roomInfoList.findIndex(item => item.room_id == list[i].room_id);
            if (index != -1) {
                roomInfoList[index] = li;
            }
            else {
                roomInfoList.push(li);
            }
        }
        wx.setStorageSync('roomInfoList', roomInfoList);
    },
    // 关注/取关
    handleFollow(e) {
        const token = wx.getStorageSync('sysToken');
        const {
            type,
            id,
            worksid,
        } = e.currentTarget.dataset
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|follow_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            follow_id: id,
            ...publicParams
        };
        const newWorksList = this.data.recommendLists;
        const index = this.data.recommendLists.findIndex(item => item.works_id == worksid);
        if (type == 'add') {
            params.remark_name = '';
            params.sign = md5(`dbkey|${publicParams.dbkey}|follow_id|${id}|privateKey|${publicParams.privateKey}|remark_name||token|${token}`);
            $API.works.addFollow(params).then(res => {
                if (res.data.isok > 0) {
                    for (let i = 0, len = newWorksList.length; i < len; i++) {
                        if (newWorksList[i].ktv_user_id == newWorksList[index].ktv_user_id) {
                            newWorksList[i].is_follow = 1;
                        }
                    }
                    newWorksList[index].is_follow = 1;
                    this.data.recommendLists = newWorksList;
                    this.setData({
                        recommendLists: this.data.recommendLists
                    });
                }
            });
        } else {
            $API.works.cancelFollow(params).then(res => {
                if (res.data.isok > 0) {
                    for (let i = 0, len = newWorksList.length; i < len; i++) {
                        if (newWorksList[i].ktv_user_id == newWorksList[index].ktv_user_id) {
                            newWorksList[i].is_follow = 0;
                        }
                    }
                    newWorksList[index].is_follow = 0;
                    this.data.recommendLists[index] = newWorksList[index];
                    this.setData({
                        recommendLists: newWorksList
                    });
                }
            });
        }
    },
    // 打赏
    handleReward(e) {
        const {
            worksid,
            broadcast_id
        } = e.currentTarget.dataset, { showType, liveLists } = this.data, that = this;
        if (showType == 3) {
            const cur_live = liveLists.find(item => item.live_broadcast_id == broadcast_id);
            if (!cur_live) { return; }
            if (cur_live.stream_name == 'hanhall') {
                return this.nowHallProgramInfo(cur_live.room_id);
            }
            else {
                return this.getRoomLiveUserInfo(cur_live.room_id);
            }
        }
        else {
            wx.navigateTo({
                url: `../reward/index?worksId=${worksid}`,
                events: {
                    sendText: async function (data) {
                        const result = await that.getRongInfo(data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        }
    },
    // 大厅直播打赏
    nowHallProgramInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        }, that = this;
        $API.lobby.nowHallProgramInfo(params).then(res => {
            if (res.data.isok != 1) {
                return wx.showToast({
                    title: '暂无播放节目',
                    icon: 'none',
                    duration: 2000
                });
            }
            wx.navigateTo({
                url: `../reward/index?lobby=true&name=${res.data.nick_name}&live_rm_id=${id}&images=${res.data.images}`,
                events: {
                    sendGift: function (data) {
                        const content = {
                            id: data.data,
                            number: 1,
                            total: 0,
                            user: {
                                id: userInfo.rong_id,
                                name: userInfo.nick_name,
                                portraitUri: userInfo.images,
                            }
                        };
                        // 发送礼物
                        return ChatRoom.sendGift({
                            targetId: targetUrl,
                            content,
                        });
                    },
                    sendText: async function (data) {
                        const result = await that.getRongInfo(res.data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    // 包厢打赏
    getRoomLiveUserInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|room_id|${id}|token|${token}`),
            token: token,
            room_id: id,
            ...publicParams
        }, that = this;
        $API.room.getRoomLiveUserInfo(params).then(res => {
            if (res.data.isok != 1) {
                return wx.showToast({
                    title: '暂无播放节目',
                    icon: 'none',
                    duration: 2000
                });
            }
            wx.navigateTo({
                url: `../reward/index?room=true&name=${res.data.nick_name}&live_rm_id=${id}&images=${res.data.images}`,
                events: {
                    sendGift: function (data) {
                        const content = {
                            id: data.data,
                            number: 1,
                            total: 0,
                            user: {
                                id: userInfo.rong_id,
                                name: userInfo.nick_name,
                                portraitUri: userInfo.images,
                            }
                        };
                        // 发送礼物
                        return ChatRoom.sendGift({
                            targetId: targetUrl,
                            content,
                        });
                    },
                    sendText: async function (data) {
                        const result = await that.getRongInfo(res.data.ktv_user_id);
                        const content = {
                            targetId: result.data.rong_id,
                            content: `${userInfo.nick_name}打赏${res.data.nick_name}${data.name},价值${data.value}火鸡币`,
                        };
                        Message.sendText(content);
                    }
                }
            });
        });
    },
    // 获取融云信息
    getRongInfo(id) {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|ktv_user_id|${id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ktv_user_id: id,
            ...publicParams
        };
        return new Promise((resolve, reject) => {
            $API.user.getRongInfo(params).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        });
    },
    // 跳转个人主页
    jumpUserWorks(e) {
        const { id, name } = e.currentTarget.dataset;
        const type = this.data.showType == 1 ? 'rec_works' : this.data.showType == 2 ? 'own_works' : 'lives';
        wx.navigateTo({
            url: `../personalWorks/index?type=${type}&user_id=${id}&user_name=${name}`,
        });
    },
    // 点赞收藏
    handleStar(e) {
        const token = wx.getStorageSync('sysToken');
        const {
            type,
            version,
            worksid,
        } = e.currentTarget.dataset
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|is_star|${type}|privateKey|${publicParams.privateKey}|token|${token}|version|${version}|works_id|${worksid}`),
            token: token,
            version: version,
            works_id: worksid,
            is_star: type,
            ...publicParams
        };
        const newWorksList = this.data.showType == 1 ? this.data.recommendLists : this.data.showType == 2 ? this.data.ownWorkLists : this.data.liveLists;
        const index = newWorksList.findIndex(item => item.works_id == worksid);
        $API.works.updateStar(params).then(res => {
            if (res.data.isok > 0) {
                newWorksList[index].is_star = type;
                newWorksList[index].fabulous_count = res.data.star_count;
                if (this.data.showType == 1) {
                    this.data.recommendLists[index] = newWorksList[index];
                    this.setData({
                        recommendLists: this.data.recommendLists
                    });
                } else if (this.data.showType == 2) {
                    this.data.ownWorkLists[index] = newWorksList[index];
                    this.setData({
                        ownWorkLists: this.data.ownWorkLists
                    });
                }
            }
        });
    },
    changeArea(e) {
        const { type } = e.currentTarget.dataset;
        this.lrSwiperHandle(+type);
    },
    changeSwiper(e) {
        const {
            current,
        } = e.detail;
        this.lrSwiperHandle(+current);
    },
    // 左右滑动均在此处理
    lrSwiperHandle(current) {
        const { playWorksId, playOwnWorksId, playerLiveId, liveLists } = this.data;
        // 切换到推荐或作品，退出聊天室
        if (current < 2 && showType_curIndex == 2) {
            const before_live = liveLists.find(item => item.live_broadcast_id == playerLiveId), that = this;
            if (before_live) {
                ChatRoom.quit(before_live.stream_name).then(function () {
                    console.log('退出聊天室成功');
                    that.setData({
                        liveMessages: {
                            [playerLiveId]: [],
                        }
                    });
                });
            }
        }
        // 推荐
        if (current == 0) {
            if (showType_curIndex === current) { return; }
            cur_videoContext.pause();
            cur_videoContext = wx.createVideoContext(`v-${playWorksId}`);
            cur_videoContext.play();
            this.setData({ isPause: false });
        }
        // 作品
        else if (current == 1) {
            if (showType_curIndex === current) { return; }
            cur_videoContext.pause();
            cur_videoContext = wx.createVideoContext(`o-${playOwnWorksId}`);
            cur_videoContext.play();
            this.setData({ isPause: false });
        }
        // 直播
        else if (current == 2) {
            if (showType_curIndex === current) { return; }
            showType_curIndex < 2 && cur_videoContext.pause();
            const cur_live = this.data.liveLists.find(item => item.live_broadcast_id == playerLiveId);
            if (cur_live) {
                const content = {
                    id: userInfo.rong_id,
                    user: {
                        id: userInfo.rong_id,
                        name: userInfo.nick_name,
                        portraitUri: userInfo.images,
                    }
                };
                // 首次进入直播间无需退出上一个
                ChatRoom.join(cur_live.stream_name, content).then(() => {
                    console.log('加入聊天室成功');
                    targetUrl = cur_live.stream_name;
                    return ChatRoom.sendJoin({
                        targetId: cur_live.stream_name,
                        content,
                    });
                }).then((message) => {
                    const new_list = {};
                    // if (!new_list[playerLiveId]) { new_list[playerLiveId] = []; }
                    new_list[playerLiveId] = []
                    // const index = new_list[playerLiveId].findIndex(item => item.messageUId == message.messageUId);
                    // if (index != -1) { return; }
                    new_list[playerLiveId].push(message);
                    this.setData({ liveMessages: new_list, toView: message.messageUId });
                });
            }
        }
        showType_curIndex = current;
        this.setData({
            showType: current + 1
        });
    },
    // 滑动修改导致swiper加载失败，未用到此api
    checkIsFollow(index) {
        const new_recommendLists = Object.assign({}, this.data.recommendLists),
            token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|follow_id|${new_recommendLists[index].ktv_user_id}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            follow_id: new_recommendLists[index].ktv_user_id,
            ...publicParams
        };
        $API.works.isFollow(params).then(res => {
            new_recommendLists[index].is_follow = res.data.is_follow;
            this.setData({
                recommendLists: new_recommendLists
            });
        });
    },
    changeVideoEnd(e) {
        const {
            current,
            source,
            currentItemId
        } = e.detail, that = this;
        if (this.data.showType == 1) {
            if (videoIndex[0] === current) { return; }
            videoIndex[0] = current;
            // const current_index = this.data.recommendLists.findIndex(item => item.works_id == currentItemId);
            // this.checkIsFollow(current_index);
            if (current == (this.data.recommendLists.length - 1)) {
                this.getRecommendLists(cur_page[0]);
            }
            delayTimer = setTimeout(() => {
                cur_videoContext = wx.createVideoContext(`v-${currentItemId}`);
                allowPause = false;
                that.setData({
                    isPause: false,
                    playWorksId: currentItemId,
                });
            }, 10);
        } else if (this.data.showType == 2) {
            if (videoIndex[1] === current) { return; }
            videoIndex[1] = current;
            if (current == (this.data.ownWorkLists.length - 1)) {
                this.getOwnWorkLists(cur_page[1]);
            }
            delayTimer = setTimeout(function () {
                cur_videoContext = wx.createVideoContext(`o-${currentItemId}`);
                allowPause = false;
                that.setData({
                    isPause: false,
                    playOwnWorksId: currentItemId
                });
            }, 10);
        } else if (this.data.showType == 3) {
            if (videoIndex[2] === current) { return; }
            videoIndex[2] = current;
            if (current == (this.data.liveLists.length - 1)) {
                this.getLiveLists(cur_page[2]);
            }
            delayTimer = setTimeout(function () {
                const before_live = that.data.liveLists.find(item => item.live_broadcast_id == that.data.playerLiveId);
                const cur_live = that.data.liveLists.find(item => item.live_broadcast_id == currentItemId);
                if (cur_live) {
                    targetUrl = cur_live.stream_name;
                    const content = {
                        id: userInfo.rong_id,
                        user: {
                            id: userInfo.rong_id,
                            name: userInfo.nick_name,
                            portraitUri: userInfo.images,
                        }
                    };
                    if (before_live) {
                        ChatRoom.quit(before_live.stream_name).then(function () {
                            console.log('退出聊天室成功');
                            that.setData({
                                liveMessages: {
                                    [before_live.live_broadcast_id]: [],
                                    [currentItemId]: [],
                                }
                            });
                            return ChatRoom.join(cur_live.stream_name);
                        }).then(() => {
                            console.log('加入聊天室成功');
                            return ChatRoom.sendJoin({
                                targetId: cur_live.stream_name,
                                content,
                            });
                        }).then((message) => {
                            const new_list = {};
                            // if (!new_list[currentItemId]) { new_list[currentItemId] = []; }
                            new_list[currentItemId] = []
                            // const index = new_list[currentItemId].findIndex(item => item.messageUId == message.messageUId);
                            // if (index != -1) { return; }
                            new_list[currentItemId].push(message);
                            that.setData({ liveMessages: new_list, toView: message.messageUId });
                        });
                    }
                    else {
                        ChatRoom.join(cur_live.stream_name).then(() => {
                            console.log('加入聊天室成功');
                            return ChatRoom.sendJoin({
                                targetId: cur_live.stream_name,
                                content,
                            });
                        }).then((message) => {
                            const new_list = {};
                            // if (!new_list[currentItemId]) { new_list[currentItemId] = []; }
                            new_list[currentItemId] = []
                            // const index = new_list[currentItemId].findIndex(item => item.messageUId == message.messageUId);
                            // if (index != -1) { return; }
                            new_list[currentItemId].push(message);
                            that.setData({ liveMessages: new_list, toView: message.messageUId });
                        });
                    }
                    that.setData({
                        playerLiveId: currentItemId
                    });
                }
            }, 10);
        }
    },
    onloadedmetadata(e) {
        const { playWorksId, playOwnWorksId } = this.data, { id } = e.currentTarget;
        if (id == `v-${playWorksId}`) {
            wx.hideLoading();
        }
        if (id == `v-${playWorksId}` || id == `o-${playOwnWorksId}`) {
            allowPause = true;
            console.log('onloadedmetadata: ', e);
        }
    },
    onProgress(e) {
        const { playWorksId } = this.data, { buffered } = e.detail, { id } = e.currentTarget;
        if (id == `v-${playWorksId}`) {
            // console.log('progress: ', e, buffered);
        }
    },
    statechange(e) {
        console.log('live-player code:', e.detail.code)
    },
    error(e) {
        console.error('live-player error:', e.detail.errMsg)
    },
    jumpMore() {
        const type = this.data.showType == 1 ? 'rec_works' : this.data.showType == 2 ? 'own_works' : 'lives';
        wx.navigateTo({
            url: `../moreWorks/index?type=${type}`,
        });
    },
    videoTimeUpdate(e) {
        const { currentTime, duration } = e.detail, sliderValue = [{}, {}], { playWorksId, playOwnWorksId } = this.data;
        if (updateSlider) {
            cur_duration = duration;
            if (this.data.showType == 1) {
                sliderValue[0][`${playWorksId}`] = currentTime / duration * 100;
            }
            else if (this.data.showType == 2) {
                sliderValue[1][`${playOwnWorksId}`] = currentTime / duration * 100;
            }
            this.setData({ sliderValue: sliderValue });
        }
    },
    videoTap() {
        const { isPause } = this.data;
        if (!allowPause) { return; }
        if (isPause) {
            cur_videoContext.play();
        }
        else {
            cur_videoContext.pause();
        }
        this.setData({ isPause: !isPause });
    },
    sliderChanging() {
        updateSlider = false;
    },
    sliderChange(e) {
        const sliderValue = [{}, {}], { playWorksId, playOwnWorksId } = this.data;
        if (cur_duration > 0) {
            cur_videoContext.seek(e.detail.value / 100 * cur_duration);
            updateSlider = true;
            if (this.data.showType == 1) {
                sliderValue[0][`${playWorksId}`] = e.detail.value;
            }
            else if (this.data.showType == 2) {
                sliderValue[1][`${playOwnWorksId}`] = e.detail.value;
            }
            this.setData({ sliderValue: sliderValue });
        }
    },
    onInput(event) {
        this.setData({
            content: event.detail.value
        });
    },
    onFocus(event) {
        let {
            height
        } = event.detail;
        height = height - 56;
        height < 0 && (height = 0);
        let adapterHeight = 0;
        this.setKeyboardPos(height, adapterHeight);
        this.hideSoftKeyboard();
    },
    onBlur(event) {
        let adapterHeight = 0;
        if (preventBlur) { return false; }
        this.setKeyboardPos(0, adapterHeight);
    },
    showEmojis() {
        preventBlur = true;
        this.showSoftKeyboard({
            emoji: { [this.data.playerLiveId]: 'block' },
        });
    },
    preventTap() {
        return false;
    },
    showSendArea() {
        const newData = {
            [this.data.playerLiveId]: true
        };
        this.setData({
            isFocus: true,
            show_chatArea: newData,
        });
    },
    closeSendArea() {
        const newData = {
            [this.data.playerLiveId]: false
        };
        preventBlur = false;
        let adapterHeight = 0;
        this.setKeyboardPos(0, adapterHeight);
        this.setData({
            show_chatArea: newData,
        });
        this.hideSoftKeyboard();
    },
    showSoftKeyboard(display) {
        delayTimer = setTimeout(() => {
            this.setData({
                isShowEmojiSent: true,
                display: display,
                bottom: 210,
                toView: this.getToView()
            });
        }, 100);
    },
    setKeyboardPos(keyboardHeight, adapterHeight) {
        keyboardHeight = keyboardHeight || 0;
        let data;
        let isScroll = (keyboardHeight > 0);
        if (isScroll) {
            data = {
                bottom: adapterHeight + keyboardHeight,
                toView: this.getToView()
            };
        } else {
            data = {
                bottom: adapterHeight + keyboardHeight,
                isShowKeyboard: false,
            };
        }
        this.setData(data);
    },
    sendText() {
        let {
            content,
            liveMessages,
            playerLiveId
        } = this.data;
        if (content.length < 1) { return; }
        this.setData({
            content: '',
            isShowEmojiSent: false
        });
        if (content.length == 0) {
            return;
        }
        const new_content = {
            content: content,
            user: {
                id: userInfo.rong_id,
                name: userInfo.nick_name,
                portraitUri: userInfo.images,
            }
        }
        ChatRoom.sendText({
            targetId: targetUrl,
            content: new_content,
        }).then(message => {
            const new_list = liveMessages;
            if (!new_list[playerLiveId]) { new_list[playerLiveId] = []; }
            const index = new_list[playerLiveId].findIndex(item => item.messageUId == message.messageUId);
            if (index != -1) { return; }
            new_list[playerLiveId].push(message);
            this.setData({
                liveMessages: new_list,
                toView: message.messageUId
            });
            this.closeSendArea();
        });
    },
    selectEmoji(e) {
        let { content } = this.data, { emoji } = e.target.dataset;
        content = content + emoji;
        this.setData({
            content: content,
            isShowEmojiSent: true
        });
    },
    getToView() {
        let {
            liveMessages,
            playerLiveId,
        } = this.data;
        let index = liveMessages[playerLiveId].length - 1;
        let message = liveMessages[playerLiveId][index] || {};
        return message.messageUId || '';
    },
    hideSoftKeyboard() {
        this.setData({
            display: {
                emoji: {
                    [this.data.playerLiveId]: 'none'
                },
            }
        });
    },
    getTabBarHeight() {
        const { windowHeight, screenHeight, statusBarHeight, safeArea } = wx.getSystemInfoSync();
        const tabBarHeight = screenHeight - windowHeight - statusBarHeight - safeArea.top;
        return tabBarHeight;
    },
});