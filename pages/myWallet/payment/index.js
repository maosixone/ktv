// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		toRightSrc: imgHosts + 'to_right.png',
		accountDetail: false,
	},
	onLoad() {
		this.getAccountDetail();
	},
	jumpPage(e) {
		const {
			type
		} = e.currentTarget.dataset;
		let link = null;
		if (type == 1) {
			link = '../payPassword/index';
		} else if (type == 2) {
			link = '../accountBind/index?type=wechat';
		} else if (type == 3) {
			link = '../accountBind/index?type=alipay';
		}
		wx.navigateTo({
			url: link,
		});
	},
	getAccountDetail() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getAccountDetail(params).then(res => {
			this.setData({
				accountDetail: res.data
			});
		});
	}
})