// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		rechargeBgSrc: imgHosts + 'reward/recharge_bg.png',
		cash_wechatSrc: imgHosts + 'cash_wechat.png',
		cash_alipaySrc: imgHosts + 'cash_alipay.png',
		warningSrc: imgHosts + 'login_warning.png',
		cash_way: '',
	},
	onLoad(option) {
		this.getThisWalletInfo();
	},
	changeCashWay(e) {
		const {
			way
		} = e.currentTarget.dataset;
		this.setData({
			cash_way: way
		});
	},
	getThisWalletInfo() {
		const token = wx.getStorageSync('sysToken');
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
			token: token,
			...publicParams
		};
		$API.user.getWalletInfo(params).then(res => {
			this.setData({
				walletInfo: res.data
			});
		});
	},
})