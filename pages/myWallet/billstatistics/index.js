// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import * as echarts from '../../../static/ec-canvas/echarts';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';
import {
	formatDate
} from '../../../utils/util';

const date = new Date(), years = [], months = [], echarts_colors = ['#0E3633', '#DDA086', '#DC448C', '#B21660'];
let chartLine = null, pick_year = null, pick_month = null, echarts_data = null;

for (let i = 2020; i <= (date.getFullYear() + 5); i++) {
	years.push(i)
}

for (let i = 1; i <= 12; i++) {
	months.push(i)
}

Page({
	data: {
		rechargeBgSrc: imgHosts + 'reward/recharge_bg.png',
		to_downSrc: imgHosts + 'to_down.png',
		noDataSrc: imgHosts + 'home/no_data.png',
		ecLine: {
			onInit: function (canvas, width, height) {
				//初始化echarts元素，绑定到全局变量，方便更改数据
				chartLine = echarts.init(canvas, null, {
					width: width,
					height: height
				});
				console.log('onInit', chartLine);
				canvas.setChart(chartLine);

				//可以先不setOption，等数据加载好后赋值，
				//不过那样没setOption前，echats元素是一片空白，体验不好，所有我先set。
				const option = getOption();
				chartLine.setOption(option);
			}
		},
		years,
		months,
		time_value: [years.findIndex(item => item == date.getFullYear()), months.findIndex(item => item == date.getMonth() + 1)],
		time_visible: false,
		type_visible: false,
		current_year: date.getFullYear(),
		current_month: date.getMonth() + 1,
		current_type: '',
		billDetail: false,
		billDetailList: false,
	},
	onShow() {
		this.getWalletDetail();
		this.getWalletDetailList();
	},
	preventTap() {
		return false;
	},
	cancelModal() {
		chartLine.setOption({
			series: [{
				color: echarts_colors,
			}],
		});
		this.setData({
			time_visible: false,
			type_visible: false,
		});
	},
	showModal(e) {
		const { type } = e.currentTarget.dataset;
		chartLine.setOption({
			series: [{
				color: ['rgba(14, 54, 51, 0.72)', 'rgba(221, 160, 134, 0.72)', 'rgba(220, 68, 140, 0.72)', 'rgba(178, 22, 96, 0.72)'],
			}],
		});
		if (type == 'type') {
			this.setData({
				type_visible: true,
			});
		}
		else if (type == 'time') {
			const { current_year, current_month } = this.data;
			const year = years.findIndex(item => item == current_year), month = months.findIndex(item => item == current_month);
			this.setData({
				time_value: [year, month],
				time_visible: true,
			});
		}
	},
	chooseTime(e) {
		const { handle } = e.target.dataset;
		if (handle == 2) {
			wx.showLoading({
				title: '加载中',
			});
			this.setData({
				current_year: pick_year,
				current_month: pick_month,
			}, () => {
				wx.hideLoading();
				this.getWalletDetail();
				this.getWalletDetailList();
			});
		}
		this.cancelModal();
	},
	chooseType(e) {
		const { type } = e.target.dataset;
		wx.showLoading({
			title: '加载中',
		});
		this.setData({
			current_type: type,
		}, () => {
			wx.hideLoading();
			this.getWalletDetailList();
		});
		this.cancelModal();
	},
	// 时间选择切换监听
	bind_timeChange(e) {
		const val = e.detail.value;
		pick_year = this.data.years[val[0]];
		pick_month = this.data.months[val[1]];
	},
	getWalletDetail() {
		const token = wx.getStorageSync('sysToken'), { current_year, current_month } = this.data;
		const params = {
			sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|query_time|${current_year}-${current_month}|token|${token}`),
			token: token,
			query_time: `${current_year}-${current_month}`,
			...publicParams
		};
		$API.user.getWalletDetail(params).then(res => {
			this.setData({
				billDetail: res.data
			});

			const data = [
				{
					value: res.data.recharge,
				},
				{
					value: res.data.withdrawal,
				},
				{
					value: res.data.consumption,
				},
				{
					value: res.data.income,
				},
			];
			echarts_data = data;
			if (!chartLine) {
				setTimeout(() => {
					console.log('chartLine', chartLine);
					chartLine.setOption({
						series: [{
							color: echarts_colors,
							data: data
						}],
					});
				}, 300);

			}
			else {
				console.log('chartLine', chartLine);
				chartLine.setOption({
					series: [{
						color: echarts_colors,
						data: data
					}],
				});
			}
		});
	},
	getWalletDetailList() {
		const token = wx.getStorageSync('sysToken'), { current_year, current_month, current_type } = this.data;
		const params = {
			sign: md5(`action_type|${current_type}|dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|query_time|${current_year}-${current_month}|token|${token}`),
			token: token,
			action_type: current_type,
			query_time: `${current_year}-${current_month}`,
			...publicParams
		};
		$API.user.getWalletDetailList(params).then(res => {
			for (let i = 0, len = res.data.length; i < len; i++) {
				switch (res.data[i].action_type) {
					case 1:
						res.data[i].action_name = '充值';
						break;
					case 2:
						res.data[i].action_name = '提现';
						break;
					case 3:
						res.data[i].action_name = '消费';
						break;
					case 4:
						res.data[i].action_name = '被打赏';
						break;
					case 5:
						res.data[i].action_name = '红包';
						break;
					case 6:
						res.data[i].action_name = '直播';
						break;
					case 7:
						res.data[i].action_name = '退歌';
						break;
					case 8:
						res.data[i].action_name = '转账收入';
						break;
					default:
						break;
				}
				const data = new Date(res.data[i].create_time);
				res.data[i].formatDate = formatDate(data);
			}
			this.setData({
				billDetailList: res.data
			});
		});
	},
	calcelChoose() {
		this.setData({
			type_visible: false
		})
	}
})

function getOption() {
	const option = {
		tooltip: {
			trigger: 'item'
		},
		legend: {
			orient: 'vertical',
			left: 'left'
		},
		series: [
			{
				name: 'Access From',
				type: 'pie',
				radius: ['45%', '100%'],
				color: echarts_colors,
				data: echarts_data || [0, 0, 0, 0,],
				label: {
					show: false,
				},
				emphasis: {
					itemStyle: {
						shadowBlur: 10,
						shadowOffsetX: 0,
						shadowColor: 'rgba(0, 0, 0, 0.5)'
					}
				}
			}
		]
	};
	return option;
}