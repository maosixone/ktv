// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../config';
import {
    API as $API
} from '../../utils/request';

Page({
    data: {
        wallet_BgSrc: imgHosts + 'wallet_bg.png',
        closeSrc: imgHosts + 'modal_close.png',
        toRightSrc: imgHosts + 'to_right.png',
        walletInfo: false, //  钱包信息
        incomeDetail: false, 	//	收入详情
    },
    onShow() {
        this.getThisWalletInfo();
        this.getIncomeDetail();
    },
    jumpPage(e) {
        const {
            type
        } = e.currentTarget.dataset;
        if (type === 'payment') { return; }
        if (type == 'reward_code') {
            wx.navigateTo({
                url: `./walletCode/index?qrcode=${this.data.walletInfo.wallet_qr_code}`,
            });
        } else {
            wx.navigateTo({
                url: `./${type}/index`,
            });
        }
    },
    getThisWalletInfo() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getWalletInfo(params).then(res => {
            this.setData({
                walletInfo: res.data
            });
        });
    },
    getIncomeDetail() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getIncomeDetail(params).then(res => {
            this.setData({
                incomeDetail: res.data
            });
        });
    },
})