// index.js
// 获取应用实例
const app = getApp()

Page({
	data: {
		qrcode: '',
	},
	onLoad(option) {
		const {
			qrcode
		} = option
		this.setData({
			qrcode: qrcode
		});
	},
})