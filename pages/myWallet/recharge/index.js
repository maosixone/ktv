// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
    publicParams,
    imgHosts
} from '../../../config';
import {
    API as $API
} from '../../../utils/request';


let userInfo = null;

Page({
    data: {
        rechargeBgSrc: imgHosts + 'reward/recharge_bg.png',
        closeSrc: imgHosts + 'modal_close.png',
        rewardWorkInfo: false, //  打赏的作品详情
        walletInfo: false, //  钱包信息
        waveEnum: [50, 100, 200, 500, 1000, 1500],
        chargeAmount: 0,
        agreeState: false, //  是否同意用户协议
        visible: false,
        Length: 6,
        Value: '',
        isFocus: true,
    },
    onLoad(option) {
        userInfo = wx.getStorageSync('user_info');
        this.getThisWalletInfo();
    },
    changeAgreeState() {
        this.setData({
            agreeState: !this.data.agreeState
        });
    },
    setWaveValue(e) {
        const {
            wave_value
        } = e.currentTarget.dataset;
        this.setData({
            chargeAmount: wave_value
        });
    },
    customWaveValue(e) {
        const {
            value
        } = e.detail;
        this.setData({
            chargeAmount: value
        });
    },
    // 创建充值订单
    createOrder() {
        const {
            chargeAmount,
        } = this.data;
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|product_id|18|token|${token}|transfer_fee|${chargeAmount * 10}`),
            token: token,
            transfer_fee: chargeAmount * 10,
            product_id: '18',
            ...publicParams
        };
        // this.handleRecharge();
        $API.order.transferOrder(params).then(res => {
            this.gotoPay(res.data.order_id);
        });
    },
    // 通过微信支付
    gotoPay(order_id) {
        const {
            chargeAmount,
        } = this.data, token = wx.getStorageSync('sysToken');

        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|openid|${userInfo.openid}|order_id|${order_id}|payFormClient|APPLY|payment_method|3|privateKey|${publicParams.privateKey}|recharge_fee|${chargeAmount}|token|${token}`),
            token: token,
            openid: userInfo.openid,
            order_id: order_id,
            recharge_fee: chargeAmount,
            payment_method: '3',
            payFormClient: 'APPLY',
            ...publicParams
        }
        $API.pay.gotoPay(params).then(res => {
            // 获取prepay_id
            this.getPayment(res.data);
        });
    },
    // 调起微信支付
    getPayment(pay_data) {
        const { nonceStr, appId, sign, timeStamp } = pay_data, that = this;
        // md5计算 paySign时，需要添加商户key
        // console.log(md5(`appId=${appId}&nonceStr=${nonceStr}&package=${pay_data.package}&signType=MD5&timeStamp=${timeStamp}&key=4V0TKMfmjBtLINCnBo2ehnSg7j3xZqQp`));
        wx.requestPayment({
            timeStamp: timeStamp,
            nonceStr: nonceStr,
            package: pay_data.package,
            signType: 'MD5',
            paySign: sign,	// md5(`appId=${appId}&nonceStr=${nonceStr}&package=${pay_data.package}&signType=MD5&timeStamp=${timeStamp}&key=4V0TKMfmjBtLINCnBo2ehnSg7j3xZqQp`),
            success(res) {
                // that.handleRecharge();

                wx.showToast({
                    title: '充值成功',
                    success() {
                        wx.navigateBack();
                    }
                });
            },
            fail(res) {
                console.log(res);
                wx.showToast({
                    title: '充值失败',
                    icon: 'none'
                });
            }
        })
    },
    // 充值
    handleRecharge() {
        const {
            chargeAmount,
        } = this.data;
        // if (!agreeState) {
        // 	wx.showToast({
        // 		icon: 'none',
        // 		title: '请同意《用户充值协议》',
        // 	});
        // 	return;
        // }
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|recharge_fee|${chargeAmount * 10}|token|${token}`),
            token: token,
            recharge_fee: chargeAmount * 10,
            ...publicParams
        };
        $API.pay.recharge(params).then(res => {
            if (res.data.isok) {
                wx.showToast({
                    title: '充值成功',
                    success() {
                        wx.navigateBack();
                    }
                });
            } else {
                wx.showToast({
                    title: '充值失败',
                    icon: 'none'
                });
            }
        });
    },
    // 关闭打赏弹窗
    closeModal() {
        this.setData({
            visible: false,
            Value: ''
        });
    },
    // 输入支付密码
    Focus(e) {
        const inputValue = e.detail.value;
        const ilen = inputValue.length;
        this.setData({
            Value: inputValue
        });
        if (ilen == 6) {

        }
    },
    Tap() {
        this.setData({
            isFocus: true
        });
    },
    getThisWalletInfo() {
        const token = wx.getStorageSync('sysToken');
        const params = {
            sign: md5(`dbkey|${publicParams.dbkey}|privateKey|${publicParams.privateKey}|token|${token}`),
            token: token,
            ...publicParams
        };
        $API.user.getWalletInfo(params).then(res => {
            this.setData({
                walletInfo: res.data
            });
        });
    },
})