// index.js
// 获取应用实例
const app = getApp();
import md5 from 'md5';
import {
	publicParams,
	imgHosts
} from '../../../config';
import {
	API as $API
} from '../../../utils/request';

Page({
	data: {
		warningSrc: imgHosts + 'login_warning.png',
		titleText: '',
	},
	onLoad(option) {
		const {
			type
		} = option;
		let name = null;
		if (type == 'wechat') {
			name = '微信';
		} else {
			name = '支付宝';
		}
		this.setData({
			titleText: name
		});
		wx.setNavigationBarTitle({
			title: `${name}提现账号`
		});
	},
})